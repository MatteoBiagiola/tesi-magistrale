/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package face;

import com.facepp.error.FaceppParseException;
import com.facepp.http.HttpRequests;
import com.facepp.http.PostParameters;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author matteo
 */
public class Face {

    /**
     * @param args the command line arguments
     */
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    private int label;
    private HttpRequests httpRequests;
    private JSONObject result;
    private ArrayList person;
    private long start;
    private PrintWriter writer;
    
    
    public static void main(String[] args) {
        // TODO code application logic here
        Face face = new Face();
        File training = new File("resources/input/training");
        File test = new File("resources/input/P2L_S5_C2");
        try {
            face.setWriter(new PrintWriter("resources/output/results.txt","UTF-8"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Face.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Face.class.getName()).log(Level.SEVERE, null, ex);
        }
        face.setup();
        //face.createPersons(training.listFiles());
        //face.createGroup();
        face.recognizeFaces(test.listFiles());
        face.getWriter().close();
    }
    
    public void setup()
    {
        this.setLabel(0);
        this.setStart(0);
        this.setPerson(new ArrayList());
        this.setHttpRequests(new HttpRequests(
                "b58803548d27eeb63d1292c7e0c054b6","nHj5heTCA2-ohxf_zm0IcU-7JDMwErbb"
                ,false,false));
        this.setResult(new JSONObject());
    }
    
    public void createPersons(File[] files)
    {
        for(File file: files)
        {
            if(file.isDirectory())
            {
                System.out.println("********");
                System.out.println("Creating person_" + this.getLabel());
                try 
                {
                    this.getHttpRequests().personCreate(new PostParameters().setPersonName("person_"
                            + this.getLabel()));
                } catch (FaceppParseException ex) {
                    Logger.getLogger(Face.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.setLabel(this.getLabel() + 1);
                this.createPersons(file.listFiles());
                System.out.println("********");
                System.out.println("person_" + (this.getLabel() - 1) + " created");
            }
            else if(file.getName().contains(".directory") || file.getName().contains("DS_Store"))
            {
                file.delete();
            }
            else
            {
                try 
                {
                    this.setStart(System.currentTimeMillis());
                    this.setResult(this.getHttpRequests().
                            detectionDetect(new PostParameters().setImg(file).setAttribute("none").setMode("oneface")));
                    System.out.println("Detection time: " + (System.currentTimeMillis() - this.getStart()) + " [ms]");
                    System.out.print("Adding face...");
                    this.getHttpRequests().personAddFace(new PostParameters().setPersonName("person_" + (this.getLabel() - 1)).
                            setFaceId(this.getResult().getJSONArray("face").getJSONObject(0).getString("face_id")));
                    System.out.println("done");
                    
                } catch (FaceppParseException ex) {
                    Logger.getLogger(Face.class.getName()).log(Level.SEVERE, null, ex);
                } catch (JSONException ex) {
                    System.out.println(file.getPath());
                    Logger.getLogger(Face.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void createGroup()
    {
        System.out.print("Creating group_0...");
        try 
        {
            this.setResult(this.getHttpRequests().infoGetPersonList());
            for(int i = 0; i < this.getResult().getJSONArray("person").length(); i++)
            {
                this.getPerson().add(this.getResult().
                        getJSONArray("person").getJSONObject(i).getString("person_name"));
            }
            this.getHttpRequests().groupCreate(new PostParameters().setGroupName("group_0"));
            this.getHttpRequests().groupAddPerson(new PostParameters().
                    setGroupName("group_0").setPersonName(this.getPerson()));
            System.out.println("done");
            System.out.print("Training...");
            this.getHttpRequests().trainIdentify(new PostParameters().setGroupName("group_0"));
            System.out.println("done");
        } catch (FaceppParseException ex) {
            Logger.getLogger(Face.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(Face.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void recognizeFaces(File[] files)
    {
        for(File file: files)
        {
            if(file.isDirectory())
            {
                this.recognizeFaces(file.listFiles());
                this.getWriter().println();
            }
            else if(file.getName().contains(".directory") || file.getName().contains("DS_Store"))
            {
                file.delete();
            }
            else
            {
                try 
                {
                    this.setStart(System.currentTimeMillis());
                    this.setResult(this.getHttpRequests().recognitionIdentify(new PostParameters()
                            .setImg(file).setGroupName("group_0")));
                    System.out.println("Recognition time: " + (System.currentTimeMillis() - this.getStart()) + " [ms]");
                    this.getWriter().println("label :" + this.getResult().getJSONArray("face").getJSONObject(0).
                            getJSONArray("candidate").getJSONObject(0).getString("person_name") +
                            " confidence: " + this.getResult().getJSONArray("face").getJSONObject(0).
                            getJSONArray("candidate").getJSONObject(0).getDouble("confidence"));
                    this.getWriter().flush();
                } catch (FaceppParseException ex) {
                    Logger.getLogger(Face.class.getName()).log(Level.SEVERE, null, ex);
                } catch (JSONException ex) {
                    this.getWriter().println("recognition not successful for image " + file.getPath());
                    Logger.getLogger(Face.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public HttpRequests getHttpRequests() {
        return httpRequests;
    }

    public void setHttpRequests(HttpRequests httpRequests) {
        this.httpRequests = httpRequests;
    }

    public ArrayList getPerson() {
        return person;
    }

    public void setPerson(ArrayList person) {
        this.person = person;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public JSONObject getResult() {
        return result;
    }

    public void setResult(JSONObject result) {
        this.result = result;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }
    
}
