/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prefacerecognition;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class PreFaceRecognition {

    /**
     * @param args the command line arguments
     */
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, IOException 
    {
        // TODO code application logic here
        /*create file with image file names*/
        try
        {
            System.gc();
            SystemMonitor sm = new SystemMonitor();
            long RAM_available = sm.getTotalRamSize();
            PreFaceRecognition pfr = new PreFaceRecognition();
            File targetFile = new File("resources/input");
            FileManager fm = new FileManager("resources/images.txt","resources/images.txt");
            fm.fillFile(targetFile.listFiles());
            fm.getWriter().close();
            String line = new String();
            StringBuffer sb = new StringBuffer();
            FaceDetector fd = new FaceDetector(new Mat(),320,new Size(20,20),
                    new Size(),Objdetect.CASCADE_SCALE_IMAGE, (float) 1.2,5);
            FacePreProcesser fp = new FacePreProcesser(new Mat(),256,256,new Size(),new Size(),
                2,(float) 1.1,Objdetect.CASCADE_SCALE_IMAGE);
            while((line = fm.getReader().readLine()) != null)
            {
                if(sm.getRamUsed() > RAM_available - 70)
                {
                    System.gc();
                }
                try
                {
                    fd.setImage(fm.readImage(line, sb));
                    fd.faceDetections(fd.resize(),"resources/faceDetection/haarcascade_frontalface_default.xml");
                    fp.setFace(fd.getDetected().get(0));
                    pfr.noPreProcessing(sb,fp);
                    //pfr.FirstPreProcessing(sb,fp);
                    //pfr.SecondPreProcessing(sb,fp);
                }
                //if eyes are not detected it is saved face detected
                //scaled with the desired face height and the desired face width.
                catch(ArrayIndexOutOfBoundsException ex)
                {
                    Mat equalized = new Mat();
                    Imgproc.cvtColor(pfr.resize(fp),equalized,Imgproc.COLOR_BGR2GRAY);
                    Imgcodecs.imwrite(sb.toString(),equalized);
                }
                catch(IndexOutOfBoundsException ex)
                {
                    System.out.println("Image " + sb.toString() + " not detected");
                }
            }
            fm.getReader().close();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void noPreProcessing(StringBuffer sb,FacePreProcesser fp)
    {
        Imgcodecs.imwrite(sb.toString(),this.resize(fp));
    }
    
    public void FirstPreProcessing(StringBuffer sb,FacePreProcesser fp)
    {
        Mat equalized = new Mat();
        Imgproc.cvtColor(this.resize(fp),equalized,Imgproc.COLOR_BGR2GRAY);
        Imgproc.equalizeHist(equalized,equalized);
        Imgcodecs.imwrite(sb.toString(),equalized);
    }
    
    public void SecondPreProcessing(StringBuffer sb,FacePreProcesser fp)
    {
        //if eyes are closed it can be that they are not detected.
        //For the moment is not tried another eye classifier but this
        //could be a valid alternative.
        //the first reliable classifier
        fp.detectLeftEye("resources/eyeDetection/haarcascade_mcs_lefteye.xml");
        fp.detectRightEye("resources/eyeDetection/haarcascade_mcs_righteye.xml");
        //the second reliable classifier
        //fp.detectLeftEye("resources/eyeDetection/haarcascade_lefteye_2splits.xml");
        //fp.detectRightEye("resources/eyeDetection/haarcascade_righteye_2splits.xml");
        //the third reliable classifier
        //fp.detectLeftEye("resources/eyeDetection/haarcascade_eye.xml");
        //fp.detectRightEye("resources/eyeDetection/haarcascade_eye.xml");
        fp.geometricalTransformation();
        fp.separateHistogramEqualization(fp.getGeoTransformed());
        fp.smoothing(fp.getEqualized());
        fp.ellipticalMask(fp.getFiltered());
        Imgcodecs.imwrite(sb.toString(),fp.getMasked());
    }
    
    public Mat resize(FacePreProcesser fp)
    {
        Size size = new Size();
        size.height = fp.getDesired_face_height();
        size.width = fp.getDesired_face_width();
        Mat resized = new Mat();
        Imgproc.resize(fp.getFace(),resized,size);
        return resized;
    }
}
