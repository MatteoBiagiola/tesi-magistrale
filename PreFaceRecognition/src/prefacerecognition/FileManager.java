/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prefacerecognition;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

/**
 *
 * @author matteo
 */
public class FileManager {
    private int label;
    private String separator;
    private PrintWriter writer;
    private BufferedReader reader;
    
    public FileManager(String fileToWrite, String fileToRead) throws FileNotFoundException, UnsupportedEncodingException
    {
        this.setLabel(0);
        this.setSeparator(";");
        this.setWriter(new PrintWriter(fileToWrite,"UTF-8"));
        this.setReader(new BufferedReader (new FileReader(fileToRead)));
    }

    public void fillFile(File[] files) throws IOException
    {
        for(File file: files)
        {
            if(file.getAbsolutePath().contains("DS_Store"))
            {
                file.delete();
            }
            else if(file.isDirectory())
            {
                String inputDirName = file.getPath();
                String outputDirName = inputDirName.replace("input","output");
                File outputDirFile = new File(outputDirName);
                if(!outputDirFile.exists())
                {
                    outputDirFile.mkdir();
                }
                this.setLabel(this.getLabel()+1);
                this.fillFile(file.listFiles());
            }
            else
            {
                this.getWriter().println(file.getPath().concat(this.getSeparator()).concat(String.format("%s",this.getLabel())));
            }    
        }
    }
    
    public Mat readImage(String lineToRead, StringBuffer outputPath) throws IOException
    {
        String inputPath;
        outputPath.delete(0,outputPath.length());
        int semicolon;
        CharSequence cs;
        semicolon = lineToRead.indexOf(";");
        cs = lineToRead.subSequence(semicolon,lineToRead.length());
        inputPath = lineToRead.replace(cs,"");
        outputPath.append(inputPath.replace("input","output"));
        return Imgcodecs.imread(inputPath);
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public String getSeparator()
    {
        return separator;
    }
    
    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }

    public BufferedReader getReader() {
        return reader;
    }

    public void setReader(BufferedReader reader) {
        this.reader = reader;
    }
    
}
