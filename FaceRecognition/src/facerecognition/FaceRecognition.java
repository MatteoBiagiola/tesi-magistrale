/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facerecognition;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.face.Face;
import org.opencv.face.FaceRecognizer;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class FaceRecognition {

    /**
     * @param args the command line arguments
     */
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here
        try
        {
            long start = System.nanoTime();
            ScheduledThreadPoolExecutor sch = (ScheduledThreadPoolExecutor) 
                Executors.newScheduledThreadPool(5);
            SystemMonitor sm = new SystemMonitor();
            sm.getWriter().println("-------------------");
            sm.getWriter().println("Total RAM size: ".concat(String.format("%d",sm.getTotalRamSize()).concat(" [Mb]")));
            sm.getWriter().println("Total Heap size: ".concat(String.format("%d",sm.getTotalHeapSize()).concat(" [Mb]")));
            sm.getWriter().println("-------------------");
            sch.scheduleAtFixedRate(sm,0,1000,TimeUnit.MILLISECONDS);
            //read training images create with PreFaceRecognition project
            FaceRecognition faceRec = new FaceRecognition();
            File targetTrainingFile = new File("resources/lfwtraining");
            File targetTestFile = new File("resources/lfwtest");
            FileManager fileTrainingManager = new FileManager("resources/training.txt","resources/training.txt");
            FileManager fileTestManager = new FileManager("resources/results.txt","resources/results.txt");
            sm.getWriter().println("---------------->START FILL TRAINING FILE");
            fileTrainingManager.fillFile(targetTrainingFile.listFiles());
            fileTrainingManager.getWriter().close();
            sm.getWriter().println("---------------->END FILL TRAINING FILE");
            //FaceRecognizer fr = Face.createEigenFaceRecognizer();
            //FaceRecognizer fr = Face.createFisherFaceRecognizer();
            FaceRecognizer fr = Face.createLBPHFaceRecognizer();
            String line;
            List<Mat> images = new ArrayList();
            Mat labels = new Mat(fileTrainingManager.getLinesWritten(),1,CvType.CV_32SC1);
            int buff[] = new int[fileTrainingManager.getLinesWritten()];
            labels.get(0,0,buff);
            int i = 0;
            sm.getWriter().println("---------------->START READ TRAINING IMAGES");
            while((line = fileTrainingManager.getReader().readLine()) != null)
            {
                images.add(fileTrainingManager.readImage(line,buff,i));
                i++;
            }
            sm.getWriter().println("---------------->END READ TRAINING IMAGES");
            labels.put(0,0,buff);
            sm.getWriter().println("---------------->START-TRAIN");
            fr.train(images,labels);
            sm.getWriter().println("---------------->END-TRAIN");
            System.out.println();
            //get test image files
            File[] imagesTest = targetTestFile.listFiles();
            FaceDetector fd = new FaceDetector(new Mat(),320,new Size(20,20),
                        new Size(),Objdetect.CASCADE_SCALE_IMAGE, (float) 1.2,3);
            FacePreProcesser fp = new FacePreProcesser(new Mat(),256,256,new Size(),new Size(),
                    2,(float) 1.1,Objdetect.CASCADE_SCALE_IMAGE);
            sm.getWriter().println("---------------->START-RECOGNITION");
            
            faceRec.readTestImages(imagesTest,fd,fp,fr,fileTestManager);
            sm.getWriter().println("---------------->END-RECOGNITION");
            fileTestManager.getWriter().close();
            sm.getWriter().println("---------------->START GARBAGE COLLECTOR");
            System.gc();
            sm.getWriter().println("---------------->END GARBAGE COLLECTOR");
            sm.getWriter().println("---------------->START SLEEP");
            Thread.sleep(5000);
            sm.getWriter().println("---------------->END SLEEP");
            sm.getWriter().close();
            sch.shutdown();
            System.out.println("Elapsed time: ".concat(String.format("%s",(System.nanoTime() - start)/10e8).concat(" [s]")));
        }
        catch(FileNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch(UnsupportedEncodingException ex)
        {
            ex.printStackTrace();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void readTestImages(File[] imagesTest,FaceDetector fd,FacePreProcesser fp,FaceRecognizer fr, FileManager fileTestManager)
    {
        for(File imageTest: imagesTest)
        {
            if(imageTest.getPath().contains(".DS_Store"))
            {
                imageTest.delete();
            }
            else if(imageTest.isDirectory())
            {
                String pathInputName = imageTest.getPath();
                String pathOutputName = pathInputName.replace("lfwtest","lfwtestPreProcessed");
                File pathOutputDir = new File(pathOutputName);
                /*if(!pathOutputDir.exists())
                {
                    pathOutputDir.mkdir();
                }*/
                this.readTestImages(imageTest.listFiles(),fd,fp,fr,fileTestManager);
            }
            else   
            {
                String pathInputName = imageTest.getPath();
                String pathOutputName = pathInputName.replace("lfwtest","lfwtestPreProcessed");
                Mat testImage = Imgcodecs.imread(pathInputName,Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                fd.setImage(testImage);
                fd.faceDetections(fd.resize(),"resources/faceDetection/haarcascade_frontalface_default.xml");
                try
                {
                    fp.setFace(fd.getDetected().get(0));
                    this.noPreProcessing(fp,fr,fileTestManager,pathOutputName);
                    //this.firstPreProcessing(fp,fr,fileTestManager,pathOutputName);
                    //this.secondPreProcessing(fp,fr,fileTestManager,pathOutputName);
                }
                catch(IndexOutOfBoundsException ex)
                {
                    System.out.println("Image: ".concat(pathInputName).concat(", not detected"));
                }
            }
        }
    }
    
    public void noPreProcessing(FacePreProcesser fp,FaceRecognizer fr,FileManager fileTestManager,String pathOutputName)
    {
        Mat resized = new Mat();
        Imgproc.resize(fp.getFace(),resized,new Size(fp.getDesired_face_width(),fp.getDesired_face_height()));
        //Imgcodecs.imwrite(pathOutputName,resized);
        int predictLabel[] = {-1};
        double confidence[] = {0.0};
        fr.predict(resized,predictLabel,confidence);
        fileTestManager.writeResult(predictLabel[0],confidence[0]);
    }
    
    public void firstPreProcessing(FacePreProcesser fp,FaceRecognizer fr,FileManager fileTestManager,String pathOutputName)
    {
        Mat resized = new Mat();
        Imgproc.resize(fp.getFace(),resized,new Size(fp.getDesired_face_width(),fp.getDesired_face_height()));
        Imgproc.equalizeHist(resized,resized);
        Imgcodecs.imwrite(pathOutputName,resized);
        int predictLabel[] = {-1};
        double confidence[] = {0.0};
        fr.predict(resized,predictLabel,confidence);
        fileTestManager.writeResult(predictLabel[0],confidence[0]);
    }
    
    public void secondPreProcessing(FacePreProcesser fp,FaceRecognizer fr,FileManager fileTestManager,String pathOutputName)
    {
        try
        {
            //the first reliable eye classifier
            fp.detectLeftEye("resources/eyeDetection/haarcascade_mcs_lefteye.xml");
            fp.detectRightEye("resources/eyeDetection/haarcascade_mcs_righteye.xml");
            fp.geometricalTransformation();
            fp.separateHistogramEqualization(fp.getGeoTransformed());
            fp.smoothing(fp.getEqualized());
            fp.ellipticalMask(fp.getFiltered());
            Imgcodecs.imwrite(pathOutputName,fp.getMasked());
        }
        //if eyes are not detected it is saved face detected
        //scaled with the desired face height and the desired face width.
        catch(ArrayIndexOutOfBoundsException ex)
        {
            System.out.println("Is not possible to geometrically transform the image: ".concat(pathOutputName));
            Mat equalized = new Mat();
            Imgproc.equalizeHist(this.resize(fp),equalized);
            fp.setMasked(equalized);
            Imgcodecs.imwrite(pathOutputName,fp.getMasked());
        }
        //prediction of the test image
        int predictLabel[] = {-1};
        double confidence[] = {0.0};
        fr.predict(fp.getMasked(),predictLabel,confidence);
        fileTestManager.writeResult(predictLabel[0],confidence[0]);
    }
    
    public Mat resize(FacePreProcesser fp)
    {
        Size size = new Size();
        size.height = fp.getDesired_face_height();
        size.width = fp.getDesired_face_width();
        Mat resized = new Mat();
        Imgproc.resize(fp.getFace(),resized,size);
        return resized;
    }
}
