/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facejni;

import org.opencv.face.FaceRecognizer;

/**
 *
 * @author matteo
 */
public class ExtLBPHFaceRecognizer extends FaceRecognizer{
    
    private static native long createLBPHFaceRecognizer();
    
    public ExtLBPHFaceRecognizer()
    {
        super(createLBPHFaceRecognizer());
        System.out.println("LBPH Face Recognizer created");
    }
}
