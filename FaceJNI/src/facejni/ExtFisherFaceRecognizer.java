/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facejni;

import org.opencv.face.FaceRecognizer;

/**
 *
 * @author matteo
 */
public class ExtFisherFaceRecognizer extends FaceRecognizer{
   
    private static native long createFisherFaceRecognizer();
        
    protected ExtFisherFaceRecognizer()
   {
       super(createFisherFaceRecognizer());
       System.out.println("Fisher Face Recognizer created");
   }
   
}
