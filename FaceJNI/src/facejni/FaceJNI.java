/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facejni;

import org.opencv.core.Core;
/**
 *
 * @author matteo
 */
public class FaceJNI {

    /**
     * @param args the command line arguments
     */
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary("FaceJNI") ; 
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        ExtFisherFaceRecognizer exFF = new ExtFisherFaceRecognizer();
        ExtLBPHFaceRecognizer exLBPH = new ExtLBPHFaceRecognizer();
    }
    
}
