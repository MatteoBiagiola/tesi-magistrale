#include <jni.h>
#include "facejni_ExtFisherFaceRecognizer.h"
#include "facejni_ExtLBPHFaceRecognizer.h"
#include <opencv2/core.hpp>
#include <opencv2/face.hpp>

using namespace cv;
using namespace cv::face;

/// throw java exception
static void throwJavaException(JNIEnv *env, const std::exception *e, const char *method) {
  std::string what = "unknown exception";
  jclass je = 0;

  if(e) {
    std::string exception_type = "std::exception";

    if(dynamic_cast<const cv::Exception*>(e)) {
      exception_type = "cv::Exception";
      je = env->FindClass("org/opencv/core/CvException");
    }

    what = exception_type + ": " + e->what();
  }

  if(!je) je = env->FindClass("java/lang/Exception");
  env->ThrowNew(je, what.c_str());

  (void)method;        // avoid "unused" warning
}

extern "C" 
{
	JNIEXPORT jlong JNICALL Java_facejni_ExtFisherFaceRecognizer_createFisherFaceRecognizer(JNIEnv *env, jclass thisObj)
	{
		static const char method_name[] = "face::createExtFisherFaceRecognizer()";
		try 
		{
			typedef Ptr<cv::face::BasicFaceRecognizer> Ptr_BasicFaceRecognizer;
			Ptr_BasicFaceRecognizer _retval_ = cv::face::createFisherFaceRecognizer();
			return (jlong)(new Ptr_BasicFaceRecognizer(_retval_));
		} catch(const std::exception &e) {
			throwJavaException(env, &e, method_name);
		} catch (...) {
			throwJavaException(env, 0, method_name);
		}
		return 0;
	}
	JNIEXPORT jlong JNICALL Java_facejni_ExtLBPHFaceRecognizer_createLBPHFaceRecognizer(JNIEnv *env, jclass thisObj)
	{
		static const char method_name[] = "face::createExtLBPHFaceRecognizer()";
		try 
		{
			typedef Ptr<cv::face::LBPHFaceRecognizer> Ptr_LBPHFaceRecognizer;
			Ptr_LBPHFaceRecognizer _retval_ = cv::face::createLBPHFaceRecognizer();
			return (jlong)(new Ptr_LBPHFaceRecognizer(_retval_));
		} catch(const std::exception &e) {
			throwJavaException(env, &e, method_name);
		} catch (...) {
			throwJavaException(env, 0, method_name);
		}
		return 0;
	}
}
