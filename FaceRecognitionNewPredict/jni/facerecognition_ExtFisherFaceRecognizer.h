/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class facerecognition_ExtFisherFaceRecognizer */

#ifndef _Included_facerecognition_ExtFisherFaceRecognizer
#define _Included_facerecognition_ExtFisherFaceRecognizer
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     facerecognition_ExtFisherFaceRecognizer
 * Method:    predictVec
 * Signature: (JJ)Ljava/util/Vector;
 */
JNIEXPORT jobject JNICALL Java_facerecognition_ExtFisherFaceRecognizer_predictVec
  (JNIEnv *, jclass, jlong, jlong);

/*
 * Class:     facerecognition_ExtFisherFaceRecognizer
 * Method:    delete
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_facerecognition_ExtFisherFaceRecognizer_delete
  (JNIEnv *, jclass, jlong);

#ifdef __cplusplus
}
#endif
#endif
