/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facerecognition;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.face.FaceRecognizer;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class FaceRecognition {

    /**
     * @param args the command line arguments
     */
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary("FaceRecognition");
    }
    
    private int detected = 0;
    private long startNorm = 0;
    private long startLbph = 0;
    private long startFisher = 0;
    private long normTime = 0;
    private long lbphTime = 0;
    private long fisherTime = 0;
    
    public static void main(String[] args) throws Throwable {
        // TODO code application logic here
        try
        {   
            File trainingFisher = new File("resources/trainingFisher.xml");
            File trainingLbph = new File("resources/trainingLbph.xml");
            String algo = "all";
            if(args.length != 0)
            {
                algo = args[0];
            }
            int size = 96;
            ExtFisherFaceRecognizer ff = FaceExt.createExtFisherFaceRecognizer();
            ExtLBPHFaceRecognizer lbph = FaceExt.createExtLBPHFaceRecognizer();
            FacePreProcesser fp = new FacePreProcesser(size,size,new Size(),new Size(),
                3,(float) 1.1,Objdetect.CASCADE_SCALE_IMAGE);
            long startTrain = System.nanoTime();
            FaceRecognition faceRec = new FaceRecognition();
            int numImagesTest = 0;
            File targetTrainingFile = new File("resources/training");
            File targetTestFile = new File("resources/test");
            numImagesTest = faceRec.TestImages(targetTestFile.listFiles(),0);
            System.out.println("Number images test: " + numImagesTest);
            FileManager fileTrainingManager = new FileManager("resources/training.txt",
                    "resources/training.txt");
            FileManager fileTestManager = new FileManager("resources/results.txt",
                    "resources/results.txt");
            //fileTrainingManager.fillFile(targetTrainingFile.listFiles());
            fileTrainingManager.getWriter().close();
            if(trainingFisher.exists() && trainingLbph.exists())
            {
                System.out.println("Training files exist");
                System.out.print("Loading...");
                if(algo.equals("all"))
                {
                    ff.load(trainingFisher.getPath());
                    lbph.load(trainingLbph.getPath());
                }
                else if(algo.equals("fisher"))
                {
                    ff.load(trainingFisher.getPath());
                }
                else if(algo.equals("lbph"))
                {
                    lbph.load(trainingLbph.getPath());
                }
                System.out.println("done");
            }
            else
            {
                System.out.println("Training files don't exist");
                System.out.print("Creating training files...");
                String line;
                List<Mat> images = new ArrayList();
                Mat labels = new Mat(fileTrainingManager.getLinesWritten(),1,CvType.CV_32SC1);
                int buff[] = new int[fileTrainingManager.getLinesWritten()];
                labels.get(0,0,buff);
                int i = 0;
                while((line = fileTrainingManager.getReader().readLine()) != null)
                {
                    images.add(fileTrainingManager.readImage(line,buff,i,size));
                    i++;
                }
                labels.put(0,0,buff);
                ff.train(images,labels);
                lbph.train(images,labels);
                images.removeAll(images);
                labels.release();
                ff.save(trainingFisher.getPath());
                lbph.save(trainingLbph.getPath());
                System.out.println("done");
            }
            System.out.println("Training time: " + (System.nanoTime() - startTrain)/10e8);
            //get test image files
            File[] imagesTest = targetTestFile.listFiles();
            faceRec.readTestImages(imagesTest,ff,lbph,fileTestManager,size,fp,algo);
            fileTestManager.getWriter().close();
            if(algo.equals("all"))
            {
                System.out.println("Average fisher recognition time: " + 
                    (faceRec.fisherTime/(numImagesTest*10e8)) + " [s]");
                System.out.println("Average lbph recognition time: " + 
                    (faceRec.lbphTime/(numImagesTest*10e8)) + " [s]");
                System.out.println("Average norm min-max time: " + 
                    (faceRec.normTime/(numImagesTest*10e8)) + " [s]");
            }
            else if(algo.equals("fisher"))
            {
                System.out.println("Average fisher recognition time: " + 
                    (faceRec.fisherTime/(numImagesTest*10e8)) + " [s]");
            }
            else if(algo.equals("lbph"))
            {
                System.out.println("Average lbph recognition time: " + 
                    (faceRec.lbphTime/(numImagesTest*10e8)) + " [s]");
            }
        }
        catch(FileNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch(UnsupportedEncodingException ex)
        {
            ex.printStackTrace();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
        catch(CvException ex)
        {
            System.out.println("OutOfMemoryError");
            System.gc();
        }
        catch(Exception ex)
        {
            System.out.println("Exception");
            ex.printStackTrace();
        }
    }
    
    public void readTestImages(File[] imagesTest,ExtFisherFaceRecognizer ff,
            ExtLBPHFaceRecognizer lbph,FileManager fileTestManager, int size, 
            FacePreProcesser fp, String algo)
    {
        for(File imageTest: imagesTest)
        {
            if(imageTest.getPath().contains(".DS_Store") ||
                    imageTest.getPath().contains(".directory"))
            {
                imageTest.delete();
            }
            else if(imageTest.isDirectory())
            {
                this.readTestImages(imageTest.listFiles(),ff,lbph,fileTestManager,size,fp,algo);
            }
            else   
            {
                String pathInputName = imageTest.getPath();
                Mat testImage = Imgcodecs.imread(pathInputName,Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                fp.setFace(testImage);
                this.noPreProcessing(testImage,ff,lbph,fileTestManager,size,algo);
                //this.PreProcessing(fp,fr,fileTestManager,pathInputName);
            }
        }
    }
    
    public void noPreProcessing(Mat image,ExtFisherFaceRecognizer ff,
            ExtLBPHFaceRecognizer lbph,FileManager fileTestManager,int size,String algo)
    {
        this.startFisher = 0;
        this.startLbph = 0;
        this.startNorm = 0;
        Imgproc.resize(image,image,new Size(size,size));
        if(algo.equals("all"))
        {
            this.startFisher = System.nanoTime();
            Vector confidencesFisher = ff.predictVector(image);
            this.fisherTime = this.fisherTime + (System.nanoTime() - this.startFisher);
            this.startLbph = System.nanoTime();
            Vector confidencesLbph = lbph.predictVector(image);
            this.lbphTime = this.lbphTime + (System.nanoTime() - this.startLbph);
            if(confidencesFisher.size() != 0 && confidencesLbph.size() != 0)
            {
                Vector results = new Vector();
                this.startNorm = System.nanoTime();
                this.normMinMax(confidencesFisher);
                this.normMinMax(confidencesLbph);
                for(int i = 0; i < confidencesFisher.size(); i++)
                {
                    results.add((double) confidencesFisher.get(i) + (double) confidencesLbph.get(i));
                }
                int label = this.searchMin(results);
                double dist = (double) results.get(label);
                this.normTime = this.normTime + (System.nanoTime() - this.startNorm);
                fileTestManager.writeResult(label, dist);
            }
            else
            {
                System.out.println("Prediction doesn't work");
            }
        }
        else if(algo.equals("fisher"))
        {
            this.startFisher = System.nanoTime();
            Vector confidencesFisher = ff.predictVector(image);
            this.fisherTime = this.fisherTime + (System.nanoTime() - this.startFisher);
            if(confidencesFisher.size() != 0)
            {
                int label = this.searchMin(confidencesFisher);
                double dist = (double) confidencesFisher.get(label);
                fileTestManager.writeResult(label, dist);
            }
            else
            {
                System.out.println("Prediction doesn't work");
            }
        }
        else if(algo.equals("lbph"))
        {
            this.startLbph = System.nanoTime();
            Vector confidencesLbph = lbph.predictVector(image);
            this.lbphTime = this.lbphTime + (System.nanoTime() - this.startLbph);
            if(confidencesLbph.size() != 0)
            {
                int label = this.searchMin(confidencesLbph);
                double dist = (double) confidencesLbph.get(label);
                fileTestManager.writeResult(label, dist);
            }
            else
            {
                System.out.println("Prediction doesn't work");
            }
        }
    }
    
    public int TestImages(File[] images,int num)
    {
        for(File image: images)
        {
            if(image.isDirectory())
            {
                num = this.TestImages(image.listFiles(),num);
            }
            else if(image.getPath().contains(".DS_Store") ||
                        image.getPath().contains(".directory"))
            {
                image.delete();
            }
            else
            {
                num = num + 1;
            }
        }
        return num;
    }
    
    public void PreProcessing(FacePreProcesser fp,FaceRecognizer fr,
            FileManager fileTestManager,String imageName)
    {
        //if eyes are closed it can be that they are not detected.
        //For the moment is not tried another eye classifier but this
        //could be a valid alternative.
        Imgproc.resize(fp.getFace(),fp.getFace(),new Size(256,256));
        fp.detectLeftEye("resources/eyeDetection/haarcascade_lefteye_2splits.xml");
        fp.detectRightEye("resources/eyeDetection/haarcascade_righteye_2splits.xml");
        fp.geometricalTransformation();
        fp.separateHistogramEqualization(fp.getGeoTransformed());
        fp.smoothing(fp.getEqualized());
        fp.ellipticalMask(fp.getFiltered());
        if(fp.getLeftEyePoint().x > 0 && fp.getRightEyePoint().x > 0)
        {
            this.setDetected(this.getDetected() + 1);
            int predictLabel[] = {-1};
            double confidence[] = {0.0};
            fr.predict(fp.getFiltered(),predictLabel,confidence);
            fileTestManager.writeResult(predictLabel[0],confidence[0]);
        }
        else
        {
            fileTestManager.writeResult(imageName);
        }
    }

    public int getDetected() {
        return detected;
    }

    public void setDetected(int detected) {
        this.detected = detected;
    }
    
    public int searchMin(Vector v)
    {
        double min = Double.MAX_VALUE;
        int index = -1;
        for(int i = 0; i < v.size(); i++)
        {
            if((double) v.get(i) < min)
            {
                min = (double) v.get(i);
                index = i;
            }
        }
        return index;
    }
    
    public int searchMax(Vector v)
    {
        double max = -1;
        int index = -1;
        for(int i = 0; i < v.size(); i++)
        {
            if((double) v.get(i) > max)
            {
                max = (double) v.get(i);
                index = i;
            }
        }
        return index;
    }
    
    public void printVector(Vector v)
    {
        for(int i = 0; i < v.size(); i++)
        {
            if(i == v.size() - 1)
            {
                System.out.println(v.get(i));
            }
            else
            {
                System.out.print(v.get(i) + ":");
            }
        }
    }
    
    public void normMinMax(Vector v)
    {
        int min_index = this.searchMin(v);
        int max_index = this.searchMax(v);
        double min = (double) v.get(min_index);
        double max = (double) v.get(max_index);
        for(int i = 0; i < v.size(); i++)
        {
            double data = (double) v.remove(i);
            double dataNorm = (data - min) / (max - min);
            v.insertElementAt(dataNorm,i);
        }
    }
    
    public void normZScore(Vector v)
    {
        double mean = this.getMean(v);
        double var = this.getVariance(v);
        for(int i = 0; i < v.size(); i++)
        {
            double data = (double) v.remove(i);
            double dataNorm = (data - mean) / (var);
            v.insertElementAt(dataNorm,i);
        }
    }
    
    public double getMean(Vector v)
    {
        double sum = 0.0;
        for(int i = 0;i < v.size(); i++)
        {
            sum = sum + (double) v.get(i);
        }
        return (sum/v.size());
    }
    
    public double getVariance(Vector v)
    {
        double mean = this.getMean(v);
        double tmp = 0.0;
        double sum = 0.0;
        for(int i = 0; i < v.size(); i++)
        {
            sum = (double) v.get(i) - mean;
            tmp = tmp + Math.pow(sum,2);;
        }
        return (tmp/v.size());
    }
    
    public void normMad(Vector v)
    {
        double median = this.getMedian(v);
        double mad = this.getMad(v);
        for(int i = 0; i < v.size(); i++)
        {
            double data = (double) v.remove(i);
            double dataNorm = (data - median) / (mad);
            v.insertElementAt(dataNorm,i);
        }
    }
    
    public double getMedian(Vector v) 
    {
       Vector v1 = (Vector) v.clone();
       v1.sort(null);
       if (v1.size() % 2 == 0) 
       {
          return ((double) v1.get((v1.size()/2)-1) + (double) v1.get(v1.size()/2))/2.0;
       } 
       else 
       {
          return (double) v1.get(v1.size()/2);
       }
    }
    
    public double getMad(Vector v)
    {
        double median = this.getMedian(v);
        Vector sub = new Vector();
        for(int i = 0; i < v.size(); i++)
        {
            sub.add(Math.abs((double) v.get(i) - median));
        }
        return this.getMedian(sub);
    }
    
    public void normTanh(Vector v)
    {
        double mean = this.getMean(v);
        double var = this.getVariance(v);
        for(int i = 0; i < v.size(); i++)
        {
            double data = (double) v.remove(i);
            double dataNorm = 0.5 * Math.tanh((0.01*((data - mean)/var)) + 1);
            v.insertElementAt(dataNorm,i);
        }
    }
}
