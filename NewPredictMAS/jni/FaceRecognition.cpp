#include <jni.h>
#include "utilities_FaceExt.h"
#include "utilities_ExtFisherFaceRecognizer.h"
#include "utilities_ExtLBPHFaceRecognizer.h"
#include <string>
#include <vector>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/face.hpp>

using namespace cv;
using namespace cv::face;
using namespace std;

/// throw java exception
static void throwJavaException(JNIEnv *env, const std::exception *e, const char *method) {
  std::string what = "unknown exception";
  jclass je = 0;

  if(e) {
    std::string exception_type = "std::exception";

    if(dynamic_cast<const cv::Exception*>(e)) {
      exception_type = "cv::Exception";
      je = env->FindClass("org/opencv/core/CvException");
    }

    what = exception_type + ": " + e->what();
  }

  if(!je) je = env->FindClass("java/lang/Exception");
  env->ThrowNew(je, what.c_str());

  (void)method;        // avoid "unused" warning
}
extern "C" 
{
	JNIEXPORT jlong JNICALL Java_utilities_FaceExt_createFisherFaceRecognizer(JNIEnv *env, jclass thisObj)
	{
		static const char method_name[] = "face::createExtFisherFaceRecognizer()";
		try 
		{
			typedef Ptr<cv::face::BasicFaceRecognizer> Ptr_BasicFaceRecognizer;
			Ptr_BasicFaceRecognizer _retval_ = cv::face::createFisherFaceRecognizer();
			return (jlong)(new Ptr_BasicFaceRecognizer(_retval_));
		} catch(const std::exception &e) {
			throwJavaException(env, &e, method_name);
		} catch (...) {
			throwJavaException(env, 0, method_name);
		}
		return 0;
	}
	JNIEXPORT jlong JNICALL Java_utilities_FaceExt_createLBPHFaceRecognizer(JNIEnv *env, jclass thisObj)
	{
		static const char method_name[] = "face::createExtLBPHFaceRecognizer()";
		try 
		{
			typedef Ptr<cv::face::LBPHFaceRecognizer> Ptr_LBPHFaceRecognizer;
			Ptr_LBPHFaceRecognizer _retval_ = cv::face::createLBPHFaceRecognizer();
			return (jlong)(new Ptr_LBPHFaceRecognizer(_retval_));
		} catch(const std::exception &e) {
			throwJavaException(env, &e, method_name);
		} catch (...) {
			throwJavaException(env, 0, method_name);
		}
		return 0;
	}
	JNIEXPORT jobject JNICALL Java_utilities_ExtFisherFaceRecognizer_predictVec(JNIEnv *env, jclass thisObj, jlong model, jlong image)
	{
		static const char method_name[] = "face::predictVecFisherFace()";
		jclass classVector = env->FindClass("java/util/Vector");
		jmethodID cntVector = env->GetMethodID(classVector,"<init>","()V");
		jclass classDouble = env->FindClass("java/lang/Double");
		jmethodID cntDouble = env->GetMethodID(classDouble,"<init>","(D)V");
		jmethodID add = env->GetMethodID(classVector,"add","(Ljava/lang/Object;)Z");
		jobject objVectorDouble = env->NewObject(classVector,cntVector);
		try
		{
			Ptr<cv::face::FaceRecognizer> faceRec = *((Ptr<cv::face::FaceRecognizer>*) model);
			Mat& img = *((Mat*) image);
			vector<double> confidences;
			faceRec->predictVec(img,confidences);
			jsize length = confidences.size();
			for(int i = 0;i < length; i++)
			{
				jobject objDouble = env->NewObject(classDouble,cntDouble,confidences[i]);
				env->CallBooleanMethod(objVectorDouble,add,objDouble);
			}
			return objVectorDouble;
		}
		catch(cv::Exception e)
		{
			throwJavaException(env, &e, method_name);
		}
		catch (...)
		{
			throwJavaException(env, 0, method_name);
		}
		return objVectorDouble;
	}
	JNIEXPORT jobject JNICALL Java_utilities_ExtLBPHFaceRecognizer_predictVec(JNIEnv *env, jclass thisObj, jlong model, jlong image)
	{
		static const char method_name[] = "face::predictVecLBPH()";
		jclass classVector = env->FindClass("java/util/Vector");
		jmethodID cntVector = env->GetMethodID(classVector,"<init>","()V");
		jclass classDouble = env->FindClass("java/lang/Double");
		jmethodID cntDouble = env->GetMethodID(classDouble,"<init>","(D)V");
		jmethodID add = env->GetMethodID(classVector,"add","(Ljava/lang/Object;)Z");
		jobject objVectorDouble = env->NewObject(classVector,cntVector);
		try
		{
			Ptr<cv::face::FaceRecognizer> faceRec = *((Ptr<cv::face::FaceRecognizer>*) model);
			Mat& img = *((Mat*) image);
			vector<double> confidences;
			faceRec->predictVec(img,confidences);
			jsize length = confidences.size();
			for(int i = 0;i < length; i++)
			{
				jobject objDouble = env->NewObject(classDouble,cntDouble,confidences[i]);
				env->CallBooleanMethod(objVectorDouble,add,objDouble);
			}
			return objVectorDouble;
		}
		catch(cv::Exception e)
		{
			throwJavaException(env, &e, method_name);
		}
		catch (...)
		{
			throwJavaException(env, 0, method_name);
		}
		return objVectorDouble;
	}
	JNIEXPORT void JNICALL Java_utilities_ExtFisherFaceRecognizer_delete(JNIEnv *env, jclass thisObj, jlong model)
	{
		delete (Ptr<cv::face::FaceRecognizer>*) model;
		return;
	}
	JNIEXPORT void JNICALL Java_utilities_ExtLBPHFaceRecognizer_delete(JNIEnv *env, jclass thisObj, jlong model)
	{
		delete (Ptr<cv::face::FaceRecognizer>*) model;
		return;
	}
}
