/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.RecognizeFaces;
import jade.core.Agent;
import org.opencv.core.Core;

/**
 *
 * @author matteo
 */
public class Agent1 extends Agent{
    
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary("NewPredictMAS");
    }
    
    protected void setup()
    {
        String arg = "all";
        if(this.getArguments() != null)
        {
            arg = (String) this.getArguments()[0];
        }
        this.addBehaviour(new RecognizeFaces(this,arg));
    }
    
    protected void takeDown()
    {
        
    }
}
