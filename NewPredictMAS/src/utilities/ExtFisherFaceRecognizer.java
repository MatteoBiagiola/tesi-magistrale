/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.util.Vector;
import org.opencv.core.Mat;
import org.opencv.face.BasicFaceRecognizer;
import org.opencv.face.FaceRecognizer;

/**
 *
 * @author matteo
 */
public class ExtFisherFaceRecognizer extends BasicFaceRecognizer{
    
    
    protected ExtFisherFaceRecognizer(long addr)
    {
       super(addr);
    }
    
    public Vector predictVector(Mat image)
    {
        return predictVec(nativeObj,image.nativeObj);
    }
    
    @Override
    protected void finalize() throws Throwable {
        delete(nativeObj);
        return;
    }
   
    private static native Vector predictVec(long nativeObj,long src_nativeObj);
    private static native void delete(long nativeObj);
}
