/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.Objdetect;
import utilities.ExtFisherFaceRecognizer;
import utilities.ExtLBPHFaceRecognizer;
import utilities.FaceExt;
import utilities.FacePreProcesser;
import utilities.FileManager;

/**
 *
 * @author matteo
 */
public class RecognizeFaces extends OneShotBehaviour{

    private String arg = "";
    private int detected = 0;
    private long startNorm = 0;
    private long startLbph = 0;
    private long startFisher = 0;
    private long normTime = 0;
    private long lbphTime = 0;
    private long fisherTime = 0;
    
    public RecognizeFaces(Agent a, String arg)
    {
        super(a);
        this.arg = arg;
    }
    
    @Override
    public void action() {
        try
        {   
            File trainingFisher = new File("resources/trainingFisher.xml");
            File trainingLbph = new File("resources/trainingLbph.xml");
            int size = 96;
            ExtFisherFaceRecognizer ff = FaceExt.createExtFisherFaceRecognizer();
            ExtLBPHFaceRecognizer lbph = FaceExt.createExtLBPHFaceRecognizer();
            FacePreProcesser fp = new FacePreProcesser(size,size,new Size(),new Size(),
                3,(float) 1.1,Objdetect.CASCADE_SCALE_IMAGE);
            long startTrain = System.nanoTime();
            int numImagesTest = 0;
            File targetTrainingFile = new File("resources/training");
            File targetTestFile = new File("resources/test");
            numImagesTest = this.TestImages(targetTestFile.listFiles(),0);
            System.out.println("Number images test: " + numImagesTest);
            FileManager fileTrainingManager = new FileManager("resources/training.txt",
                    "resources/training.txt");
            FileManager fileTestManager = new FileManager("resources/results.txt",
                    "resources/results.txt");
            fileTrainingManager.fillFile(targetTrainingFile.listFiles());
            fileTrainingManager.getWriter().close();
            if(trainingFisher.exists() && trainingLbph.exists())
            {
                System.out.println("Training files exist");
                System.out.print("Loading...");
                if(this.arg.equals("all"))
                {
                    ff.load(trainingFisher.getPath());
                    lbph.load(trainingLbph.getPath());
                }
                else if(this.arg.equals("fisher"))
                {
                    ff.load(trainingFisher.getPath());
                }
                else if(this.arg.equals("lbph"))
                {
                    lbph.load(trainingLbph.getPath());
                }
                System.out.println("done");
            }
            else
            {
                System.out.println("Training files don't exist");
                System.out.print("Creating training files...");
                String line;
                List<Mat> images = new ArrayList();
                Mat labels = new Mat(fileTrainingManager.getLinesWritten(),1,CvType.CV_32SC1);
                int buff[] = new int[fileTrainingManager.getLinesWritten()];
                labels.get(0,0,buff);
                int i = 0;
                while((line = fileTrainingManager.getReader().readLine()) != null)
                {
                    images.add(fileTrainingManager.readImage(line,buff,i,size));
                    i++;
                }
                labels.put(0,0,buff);
                ff.train(images,labels);
                lbph.train(images,labels);
                images.removeAll(images);
                labels.release();
                ff.save(trainingFisher.getPath());
                lbph.save(trainingLbph.getPath());
                System.out.println("done");
            }
            System.out.println("Training time: " + (System.nanoTime() - startTrain)/10e8);
            //get test image files
            File[] imagesTest = targetTestFile.listFiles();
            this.readTestImages(imagesTest,ff,lbph,fileTestManager,size,fp,this.arg);
            fileTestManager.getWriter().close();
            if(this.arg.equals("all"))
            {
                System.out.println("Average fisher recognition time: " + 
                    (this.fisherTime/(numImagesTest*10e8)) + " [s]");
                System.out.println("Average lbph recognition time: " + 
                    (this.lbphTime/(numImagesTest*10e8)) + " [s]");
                System.out.println("Average norm min-max time: " + 
                    (this.normTime/(numImagesTest*10e8)) + " [s]");
            }
            else if(this.arg.equals("fisher"))
            {
                System.out.println("Average fisher recognition time: " + 
                    (this.fisherTime/(numImagesTest*10e8)) + " [s]");
            }
            else if(this.arg.equals("lbph"))
            {
                System.out.println("Average lbph recognition time: " + 
                    (this.lbphTime/(numImagesTest*10e8)) + " [s]");
            }
        }
        catch(FileNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch(UnsupportedEncodingException ex)
        {
            ex.printStackTrace();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
        catch(CvException ex)
        {
            System.out.println("OutOfMemoryError");
            System.gc();
        }
        catch(Exception ex)
        {
            System.out.println("Exception");
            ex.printStackTrace();
        }
    }
    
    public void readTestImages(File[] imagesTest,ExtFisherFaceRecognizer ff,
            ExtLBPHFaceRecognizer lbph,FileManager fileTestManager, int size, 
            FacePreProcesser fp, String algo)
    {
        for(File imageTest: imagesTest)
        {
            if(imageTest.getPath().contains(".DS_Store") ||
                    imageTest.getPath().contains(".directory"))
            {
                imageTest.delete();
            }
            else if(imageTest.isDirectory())
            {
                this.readTestImages(imageTest.listFiles(),ff,lbph,fileTestManager,size,fp,algo);
            }
            else   
            {
                String pathInputName = imageTest.getPath();
                Mat testImage = Imgcodecs.imread(pathInputName,Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                fp.setFace(testImage);
                this.noPreProcessing(testImage,ff,lbph,fileTestManager,size,algo);
            }
        }
    }
    
    public void noPreProcessing(Mat image,ExtFisherFaceRecognizer ff,
            ExtLBPHFaceRecognizer lbph,FileManager fileTestManager,int size,String algo)
    {
        this.startFisher = 0;
        this.startLbph = 0;
        this.startNorm = 0;
        Imgproc.resize(image,image,new Size(size,size));
        if(algo.equals("all"))
        {
            this.startFisher = System.nanoTime();
            Vector confidencesFisher = ff.predictVector(image);
            this.fisherTime = this.fisherTime + (System.nanoTime() - this.startFisher);
            this.startLbph = System.nanoTime();
            Vector confidencesLbph = lbph.predictVector(image);
            this.lbphTime = this.lbphTime + (System.nanoTime() - this.startLbph);
            if(confidencesFisher.size() != 0 && confidencesLbph.size() != 0)
            {
                Vector results = new Vector();
                this.startNorm = System.nanoTime();
                this.normMinMax(confidencesFisher);
                this.normMinMax(confidencesLbph);
                for(int i = 0; i < confidencesFisher.size(); i++)
                {
                    results.add((double) confidencesFisher.get(i) + (double) confidencesLbph.get(i));
                }
                int label = this.searchMin(results);
                double dist = (double) results.get(label);
                this.normTime = this.normTime + (System.nanoTime() - this.startNorm);
                fileTestManager.writeResult(label, dist);
            }
            else
            {
                System.out.println("Prediction doesn't work");
            }
        }
        else if(algo.equals("fisher"))
        {
            this.startFisher = System.nanoTime();
            Vector confidencesFisher = ff.predictVector(image);
            this.fisherTime = this.fisherTime + (System.nanoTime() - this.startFisher);
            if(confidencesFisher.size() != 0)
            {
                int label = this.searchMin(confidencesFisher);
                double dist = (double) confidencesFisher.get(label);
                fileTestManager.writeResult(label, dist);
            }
            else
            {
                System.out.println("Prediction doesn't work");
            }
        }
        else if(algo.equals("lbph"))
        {
            this.startLbph = System.nanoTime();
            Vector confidencesLbph = lbph.predictVector(image);
            this.lbphTime = this.lbphTime + (System.nanoTime() - this.startLbph);
            if(confidencesLbph.size() != 0)
            {
                int label = this.searchMin(confidencesLbph);
                double dist = (double) confidencesLbph.get(label);
                fileTestManager.writeResult(label, dist);
            }
            else
            {
                System.out.println("Prediction doesn't work");
            }
        }
    }
    
    public int TestImages(File[] images,int num)
    {
        for(File image: images)
        {
            if(image.isDirectory())
            {
                num = this.TestImages(image.listFiles(),num);
            }
            else if(image.getPath().contains(".DS_Store") ||
                        image.getPath().contains(".directory"))
            {
                image.delete();
            }
            else
            {
                num = num + 1;
            }
        }
        return num;
    }
    
    public int searchMin(Vector v)
    {
        double min = Double.MAX_VALUE;
        int index = -1;
        for(int i = 0; i < v.size(); i++)
        {
            if((double) v.get(i) < min)
            {
                min = (double) v.get(i);
                index = i;
            }
        }
        return index;
    }
    
    public int searchMax(Vector v)
    {
        double max = -1;
        int index = -1;
        for(int i = 0; i < v.size(); i++)
        {
            if((double) v.get(i) > max)
            {
                max = (double) v.get(i);
                index = i;
            }
        }
        return index;
    }
    
    public void normMinMax(Vector v)
    {
        int min_index = this.searchMin(v);
        int max_index = this.searchMax(v);
        double min = (double) v.get(min_index);
        double max = (double) v.get(max_index);
        for(int i = 0; i < v.size(); i++)
        {
            double data = (double) v.remove(i);
            double dataNorm = (data - min) / (max - min);
            v.insertElementAt(dataNorm,i);
        }
    }
}
