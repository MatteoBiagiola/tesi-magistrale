/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;

/**
 *
 * @author matteo
 */
public class ReceiveMessages extends CyclicBehaviour{

    public ReceiveMessages(Agent a)
    {
        super(a);
    }
    
    @Override
    public void action() {
        this.block();
    }
    
}
