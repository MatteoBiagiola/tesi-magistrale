/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.df.SearchService;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import utilities.CreateMessageForDF;

/**
 *
 * @author matteo
 */
public class Agent0 extends Agent{
    
    public int num = 0;
    
    protected void setup()
    {
        ACLMessage toDF = CreateMessageForDF.createSearchMessage(this,"receive-message",-1);
        
        this.addBehaviour(new SearchService(this,5000,toDF));
    }
    
    protected void takeDown()
    {
        System.out.println(this.getLocalName() + ": dead");
    }
}
