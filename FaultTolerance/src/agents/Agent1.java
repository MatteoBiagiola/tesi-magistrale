/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.ams.ManageDFFault;
import behaviours.ReceiveMessage;
import behaviours.df.DeRegistrationService;
import behaviours.df.RegistrationService;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import utilities.CreateMessageForDF;

/**
 *
 * @author matteo
 */
public class Agent1 extends Agent{
    
    protected void setup()
    {
        this.addBehaviour(new ReceiveMessage(this));
        
        ACLMessage toDF = CreateMessageForDF.createRegistrationMessage(this, 
                this.getLocalName() + "-service", "receive-message");
        
        this.addBehaviour(new RegistrationService(this,toDF));
        
        this.addBehaviour(new ManageDFFault());
    }
    
    protected void takeDown()
    {
        ACLMessage toDF = CreateMessageForDF.createDeRegistrationMessage(this, 
                this.getLocalName() + "-service", "receive-message");
        
        this.addBehaviour(new DeRegistrationService(this,toDF));
        
        System.out.println(this.getLocalName() + ": dead");
    }
    
}
