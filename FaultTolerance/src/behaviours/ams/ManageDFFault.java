/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.ams;

import behaviours.df.RegistrationService;
import jade.domain.introspection.AMSSubscriber;
import jade.domain.introspection.BornAgent;
import jade.domain.introspection.Event;
import jade.domain.introspection.IntrospectionVocabulary;
import jade.domain.introspection.RemovedContainer;
import jade.lang.acl.ACLMessage;
import java.util.Map;
import utilities.CreateMessageForDF;

/**
 *
 * @author matteo
 */
public class ManageDFFault extends AMSSubscriber{

    protected static final String BORN_AGENT = "Born_agent";
    protected static final String REMOVED_CONTAINER = "Removed_container";
    protected static final String REMOVED_MAIN_INSTANT = "Removed_main_instant";
    
    public ManageDFFault()
    {
        super();
    }
    
    @Override
    protected void installHandlers(Map handlers) {
        
        ManageDFFault.EventHandler creationsHandler;
        cleanDataStoreVariables();
        
        creationsHandler = (Event event) -> {
            RemovedContainer rc = (RemovedContainer) event;
            System.out.println(myAgent.getLocalName() + ": RemovedContainer "+ rc.getName()
                    + " container: " + rc.getContainer());
            if(rc.getContainer().getName().contains("Main"))
            {
                System.out.println(myAgent.getLocalName() + ": " + rc.getContainer().getName() + " removed container");
                getDataStore().put(REMOVED_CONTAINER, true);
                getDataStore().put(REMOVED_MAIN_INSTANT, System.currentTimeMillis());
            }
            else
            {
                cleanDataStoreVariables();
            }
        };
        handlers.put(IntrospectionVocabulary.REMOVEDCONTAINER,creationsHandler);
        
        creationsHandler = (Event event) -> {
            BornAgent ba = (BornAgent) event;
            boolean removedMain = (boolean) getDataStore().get(REMOVED_CONTAINER);
            if(ba.getClassName().contains("ams") && removedMain)
            {
                System.out.println(myAgent.getLocalName() + ": BornAgent "+ ba.getClassName());
                long removedMainInstant = (long) getDataStore().get(REMOVED_MAIN_INSTANT);
                checkService(removedMainInstant);
            }
            else
            {
                cleanDataStoreVariables();
            }
        };
        handlers.put(IntrospectionVocabulary.BORNAGENT,creationsHandler);
        
    }
    
    protected void checkService(long removedMainInstant)
    {
        System.out.println(this.myAgent.getLocalName() + ": CheckService");
        cleanDataStoreVariables();
        System.out.println(this.myAgent.getLocalName() + ": re-register service after fault");
        ACLMessage toDF = CreateMessageForDF.createRegistrationMessage(this.myAgent, 
        this.myAgent.getLocalName() + "-service", "receive-message");
        this.myAgent.addBehaviour(new RegistrationService(this.myAgent,toDF,removedMainInstant));
    }
    
    protected void cleanDataStoreVariables()
    {
        getDataStore().put(BORN_AGENT, false);
        getDataStore().put(REMOVED_CONTAINER, false);
        getDataStore().put(REMOVED_MAIN_INSTANT, 0);
    }
}
