/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import behaviours.df.SearchService;
import agents.Agent0;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import utilities.CreateMessageForDF;

/**
 *
 * @author matteo
 */
public class SendMessage extends OneShotBehaviour{

    private Agent0 ag;
    private AID receiver;
    
    public SendMessage(Agent a,AID receiver) {
        super(a);
        ag = (Agent0) a;
        this.receiver = receiver;
    }

    @Override
    public void action() {
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.setConversationId("pippo");
        msg.addReceiver(receiver);
        ag.num++;
        System.out.println(this.myAgent.getLocalName() + ": message sent #" + ag.num);
        this.myAgent.send(msg);
        
        ACLMessage toDF = CreateMessageForDF.createSearchMessage(this.myAgent,"receive-message",-1);
        this.myAgent.addBehaviour(new SearchService(this.myAgent,5000,toDF));
    }
    
}
