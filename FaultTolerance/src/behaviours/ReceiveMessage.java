/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 *
 * @author matteo
 */
public class ReceiveMessage extends CyclicBehaviour{

    private int num = 0;
    
    public ReceiveMessage(Agent a)
    {
        super(a);
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchConversationId("pippo"));
        if(msg != null)
        {
            num = num + 1;
            System.out.println(this.myAgent.getLocalName() + ": message received #" + num);
        }
        else
        {
            this.block();
        }
    }
    
    
}
