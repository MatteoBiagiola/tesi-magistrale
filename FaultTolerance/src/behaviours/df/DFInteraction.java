/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.df;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FIPAManagementVocabulary;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matteo
 */
public class DFInteraction extends AchieveREInitiator{
    
    private String action;
    private long removedMainInstant = 0;
    private long waitBeforeRequery = 0;
    private ACLMessage msg;
    
    public DFInteraction(Agent a, ACLMessage msg) {
        super(a, msg);
        this.setAction(msg);
    }
    
    public DFInteraction(Agent a, long waitBeforeRequery, ACLMessage msg) {
        super(a, msg);
        this.setAction(msg);
        this.waitBeforeRequery = waitBeforeRequery;
        this.msg = msg;
    }
    
    public DFInteraction(Agent a, ACLMessage msg, long removedMainInstant) {
        super(a, msg);
        this.removedMainInstant = removedMainInstant;
        this.setAction(msg);
    }
    
    protected void handleAgree(ACLMessage agree)
    {
        System.out.println(this.myAgent.getLocalName() + ": agree message received for the action "
                + this.action + " content " + agree.getContent());
    }
    
    protected void handleRefuse(ACLMessage refuse)
    {
        System.out.println(this.myAgent.getLocalName() + ": refuse message received for the action "
                + this.action + " refuse " + refuse.getContent());
    }
    
    protected void handleInform(ACLMessage inform)
    {
        System.out.println(this.myAgent.getLocalName() + ": inform message received for the action "
                + this.action);
        if(this.action.equals(FIPAManagementVocabulary.REGISTER) || 
                this.action.equals(FIPAManagementVocabulary.DEREGISTER) ||
                this.action.equals(FIPAManagementVocabulary.MODIFY))
        {
            try 
            {
                DFAgentDescription agent = DFService.decodeDone(inform.getContent());
                if(this.removedMainInstant > 0)
                {
                    System.out.println("Re-configuration time: " 
                            + (System.currentTimeMillis() - this.removedMainInstant) + " [ms]");
                }
                System.out.println(this.myAgent.getLocalName() + ": agent requesting " + this.action + " action " + agent.getName().getLocalName());
                handleDFModifies(agent.getName());
            } catch (FIPAException ex) {
                handleDFFailure(inform);
                Logger.getLogger(DFInteraction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(this.action.equals(FIPAManagementVocabulary.SEARCH))
        {
            try 
            {
                DFAgentDescription[] agents = DFService.decodeResult(inform.getContent());
                AID[] agentsAID = new AID[agents.length];
                System.out.println(this.myAgent.getLocalName() + ": search results ");
                for(int i = 0; i < agents.length; i++)
                {
                    System.out.println(agents[i].getName());
                    agentsAID[i] = agents[i].getName();
                }
                System.out.println();
                handleSearchResults(agentsAID);
                if(this.waitBeforeRequery > 0 && agents.length == 0)
                {
                    this.myAgent.addBehaviour(new WakerBehaviour(this.myAgent,this.waitBeforeRequery) 
                    {
                        @Override
                        public void onWake()
                        {
                            handlePeriodElapsed();
                        }
                    });
                }
            } catch (FIPAException ex) {
                Logger.getLogger(DFInteraction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    protected void handleDFFailure(ACLMessage inform)
    {
        
    }
    
    protected void handleDFModifies(AID agent)
    {
        
    }
    
    protected void handleSearchResults(AID[] agentsAID)
    {
        
    }
    
    protected void handlePeriodElapsed()
    {
        
    }
    
    private void setAction(ACLMessage msg)
    {
        if(msg.getContent().contains(FIPAManagementVocabulary.REGISTER))
            this.action = FIPAManagementVocabulary.REGISTER;
        else if(msg.getContent().contains(FIPAManagementVocabulary.DEREGISTER))
            this.action = FIPAManagementVocabulary.DEREGISTER;
        else if(msg.getContent().contains(FIPAManagementVocabulary.MODIFY))
            this.action = FIPAManagementVocabulary.MODIFY;
        else if(msg.getContent().contains(FIPAManagementVocabulary.SEARCH))
            this.action = FIPAManagementVocabulary.SEARCH;
        else this.action = "error";
    }
    
}