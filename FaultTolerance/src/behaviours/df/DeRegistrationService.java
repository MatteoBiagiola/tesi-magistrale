/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.df;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author matteo
 */
public class DeRegistrationService extends DFInteraction{
    
    private ACLMessage msg;
    
    public DeRegistrationService(Agent a, ACLMessage msg) {
        super(a, msg);
        this.msg = msg;
    }

    @Override
    protected void handleDFFailure(ACLMessage inform)
    {
        System.out.println("pippo deregister");
    }
}
