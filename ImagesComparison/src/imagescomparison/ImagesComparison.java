/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imagescomparison;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author matteo
 */
public class ImagesComparison {

    static{
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        List<Mat> faces = new ArrayList();
        File test = new File("resources/input");
        for(File imageFile: test.listFiles())
        {
            Mat image = Imgcodecs.imread(imageFile.getPath());
            Imgproc.resize(image,image,new Size(60,60));
            faces.add(image);
        }
        for(Mat image: faces)
        {
            for(int i = 0;i < faces.size();i++)
            {
                System.out.println(Core.norm(faces.get(i),image,Core.NORM_INF));
            }
            System.out.println();
        }
    }

}
