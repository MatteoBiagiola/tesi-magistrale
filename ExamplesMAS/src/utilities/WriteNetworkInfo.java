/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import agents.Recognizer;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.LEAPACLCodec;
import jade.util.leap.Iterator;
import jade.util.leap.Map;
import jade.util.leap.Set;
import ontology.Faces;

/**
 *
 * @author matteo
 */
public class WriteNetworkInfo {
    
    public static void writeBytesExchanged(Recognizer recognizer)
    {
        int recognized = Faces.getRecognized() + Faces.getRemote_recognized();
        int totalBytes = 0;
        recognizer.getWriter().println("Detected: " + Faces.getDetected()
                + " Recognized: " + recognized
                + "(local:" + Faces.getRecognized() 
                + "/remote:" + Faces.getRemote_recognized()
                + ") Lost: " + (Faces.getDetected() - recognized));
        recognizer.getWriter().close();
        Map bytesExchanged = LEAPACLCodec.getBytesExchanged();
        Set keySet = bytesExchanged.keySet();
        if(!keySet.isEmpty())
        {
            for(Iterator it = keySet.iterator(); it.hasNext();)
            {
                int currentKey = (int) it.next();
                recognizer.getWriterNetworkInfo().println(ACLMessage.getPerformative(currentKey) 
                        + ": bytes " + (int) bytesExchanged.get(currentKey));
                totalBytes = totalBytes + (int) bytesExchanged.get(currentKey);
                recognizer.getWriterNetworkInfo().flush();
            }
            recognizer.getWriterNetworkInfo().println("totalBytes: " + totalBytes);
            recognizer.getWriterNetworkInfo().flush();
            recognizer.getWriterNetworkInfo().println("totalBytes: " + (int) (totalBytes/1024) 
                    + " [KB]");
            recognizer.getWriterNetworkInfo().flush();
        }
        else
        {
            recognizer.getWriterNetworkInfo().println("No message exchanged between containers");
            recognizer.getWriterNetworkInfo().flush();
        }
        
        recognizer.getWriterNetworkInfo().println();
        recognizer.getWriterNetworkInfo().flush();
            
        double frameTotalBytes = (double) Faces.getFrameTotalBytes()/(1024*1024);
        recognizer.getWriterNetworkInfo().print("totalBytes regard frames: " + 
                 frameTotalBytes + " [MB]");
        recognizer.getWriterNetworkInfo().flush();
        
        recognizer.getWriterNetworkInfo().println();
        recognizer.getWriterNetworkInfo().flush();
        
        Map bytesReceivedFromAMS = LEAPACLCodec.getBytesReceivedFromAMS();
        Set keySetAMS = bytesReceivedFromAMS.keySet();
        totalBytes = 0;
        if(!keySetAMS.isEmpty())
        {
            for(Iterator it = keySetAMS.iterator(); it.hasNext();)
            {
                int currentKey = (int) it.next();
                recognizer.getWriterNetworkInfo().println(ACLMessage.getPerformative(currentKey) 
                        + ": bytes " + (int) bytesReceivedFromAMS.get(currentKey));
                totalBytes = totalBytes + (int) bytesReceivedFromAMS.get(currentKey);
                recognizer.getWriterNetworkInfo().flush();
            }
            recognizer.getWriterNetworkInfo().println("totalBytes: " + totalBytes);
            recognizer.getWriterNetworkInfo().flush();
            recognizer.getWriterNetworkInfo().println("totalBytes: " + (int) (totalBytes/1024) 
                    + " [KB]");
            recognizer.getWriterNetworkInfo().flush();
        }
        else
        {
            recognizer.getWriterNetworkInfo().println("No message received from AMS");
            recognizer.getWriterNetworkInfo().flush();
        }
        
        recognizer.getWriterNetworkInfo().close();
    }
}
