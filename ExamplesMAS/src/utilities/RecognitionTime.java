/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author matteo
 */
public class RecognitionTime {
    
    private static final double tNorm = 2;
    
    public static void computeContractDetails(int detected, int timeForRecognition, 
            int classNum, int imgNumPerClass, int[] initiatorDetails, int[] responderDetails)
    {
        double recTimePerFace = RecognitionTime.computeRecognitionTime(classNum,imgNumPerClass);
        double wcet = 0.0;
        if(detected > 0)
        {
            wcet = detected * recTimePerFace;
        }
        if(wcet > timeForRecognition)
        {
            //initiator
            initiatorDetails[0] = RecognitionTime.computeFaces(wcet,timeForRecognition,recTimePerFace,true);
        }
        else if(wcet == 0.0)
        {
            responderDetails[0] = RecognitionTime.computeFaces(timeForRecognition,recTimePerFace);
        }
        else
        {
            responderDetails[0] = RecognitionTime.computeFaces(wcet,timeForRecognition,recTimePerFace,false);
        }
    }
    
    public static double computeRecognitionTime(int classNum, int imgNumPerClass)
    {
        double tFisher = RecognitionTime.computeFisherTime(classNum,imgNumPerClass);
        double tLbph = RecognitionTime.computeLbphTime(classNum,imgNumPerClass);
        double tNorm = 2;
        return Math.ceil(tFisher + tLbph + tNorm);//rounding up
    }
    
    private static double computeFisherTime(int classNum, int imgNumPerClass)
    {
        double m = (0.012 * classNum) - 0.06;
        double q = (1.37 * classNum) + 2.32;
        return (m * imgNumPerClass) + q;
    }
    
    private static double computeLbphTime(int classNum, int imgNumPerClass)
    {
        double m = (3.8 * classNum) - 0.034;
        double q = 75;
        return (m * imgNumPerClass) + q;
    }
    
    private static int computeFaces(double wcet,int timeForRecognition,double recTimePerFace,boolean toSend)
    {
        int result = -1;
        if(toSend)
        {
            result = (int) Math.ceil((wcet - timeForRecognition)/recTimePerFace);
        }
        else
        {
            result = (int) Math.floor((timeForRecognition - wcet)/recTimePerFace);
        }
        return result;
    }
    
    private static int computeFaces(int timeForRecognition,double recTimePerFace)
    {
        return (int) Math.floor(timeForRecognition/recTimePerFace);
    }
}
