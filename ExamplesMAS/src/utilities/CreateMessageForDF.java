/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FIPAManagementVocabulary;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;

/**
 *
 * @author matteo
 */
public class CreateMessageForDF {
    
    public CreateMessageForDF()
    {
        
    }
    
    public static ACLMessage createRegistrationMessage(Agent a, String serviceName, String serviceType)
    {
        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        dfd.setName(a.getAID());
        sd.setName(serviceName);
        sd.setType(serviceType);
        dfd.addServices(sd);
        return DFService.createRequestMessage(a,
                a.getDefaultDF(),FIPAManagementVocabulary.REGISTER,dfd,null);
    }
    
    public static ACLMessage createMultipleRegistrationMessage(Agent a, ArrayList serviceNames, 
            ArrayList serviceTypes)
    {
        if(serviceNames.size() != serviceTypes.size())
        {
            System.out.println("Arrays must be of the same size!!");
            return null;
        }
        int size = serviceNames.size();
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(a.getAID());
        for(int i = 0; i < size; i++)
        {
            ServiceDescription sd = new ServiceDescription();
            sd.setName((String) serviceNames.get(i));
            sd.setType((String) serviceTypes.get(i));
            dfd.addServices(sd);
        }
        return DFService.createRequestMessage(a,
                a.getDefaultDF(),FIPAManagementVocabulary.REGISTER,dfd,null);
    }
    
    public static ACLMessage createDeRegistrationMessage(Agent a, String serviceName, String serviceType)
    {
        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        dfd.setName(a.getAID());
        sd.setName(serviceName);
        sd.setType(serviceType);
        dfd.addServices(sd);
        return DFService.createRequestMessage(a,
                a.getDefaultDF(),FIPAManagementVocabulary.DEREGISTER,dfd,null);
    }
    
    public static ACLMessage createSearchMessage(Agent a, 
            String serviceType, long maxResults)
    {
        DFAgentDescription dfd = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        SearchConstraints sc = new SearchConstraints();
        sd.setType(serviceType);
        sc.setMaxResults(maxResults);
        dfd.addServices(sd);
        return DFService.createRequestMessage(a,
                a.getDefaultDF(),FIPAManagementVocabulary.SEARCH,dfd,sc);
    }
    
    public static ACLMessage createSearchMessage(Agent a, 
            String serviceType)
    {
        return CreateMessageForDF.createSearchMessage(a, serviceType, -1);
    }
}
