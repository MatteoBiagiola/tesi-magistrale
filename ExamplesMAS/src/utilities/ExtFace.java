/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author matteo
 */
public class ExtFace {
        
    public static ExtFisherFaceRecognizer createExtFisherFaceRecognizer()
    {
        ExtFisherFaceRecognizer recognizer = new ExtFisherFaceRecognizer(createFisherFaceRecognizer());
        return recognizer;
    }
    
    public static ExtLBPHFaceRecognizer createExtLBPHFaceRecognizer()
    {
        ExtLBPHFaceRecognizer recognizer = new ExtLBPHFaceRecognizer(createLBPHFaceRecognizer());
        return recognizer;
    }
    
    private static native long createFisherFaceRecognizer();
    private static native long createLBPHFaceRecognizer();
}
