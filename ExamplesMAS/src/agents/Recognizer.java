/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.df.DeRegistrationService;
import behaviours.df.RegistrationService;
import behaviours.faultRecovery.ManageDFFault;
import behaviours.recognizer.SearchDetectorContractor;
import jade.content.lang.leap.LEAPCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import jade.util.leap.HashMap;
import jade.util.leap.Map;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Faces;
import org.opencv.core.Core;
import utilities.CreateMessageForDF;
import utilities.ExtFace;
import utilities.ExtFisherFaceRecognizer;
import utilities.ExtLBPHFaceRecognizer;
import utilities.WriteNetworkInfo;

/**
 *
 * @author matteo
 */
public class Recognizer extends Agent{
    
    private PrintWriter writer = null;
    private PrintWriter writerNetworkInfo = null;
    private ExtFisherFaceRecognizer ff;
    private ExtLBPHFaceRecognizer lbph;
    
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary("ExamplesMAS");
    }

    private String containerName = "";
    
    protected void setup()
    {   
        String containerNumber = "";
        try 
        {
            containerNumber = this.getLocalName().substring(this.getLocalName().length()-1);
            try
            {
                Integer.parseInt(containerNumber);
            }catch(NumberFormatException ex){
                containerNumber = "0";
            }
            this.writer = new PrintWriter("resources/output/rec_results/" + containerNumber + "_local_recognition.txt","UTF-8");
            this.writerNetworkInfo = new PrintWriter("resources/output/rec_results/" + containerNumber + "_network_info.txt","UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try 
        {
            this.containerName = this.getContainerController().getContainerName();
            System.out.println(this.getLocalName() + ": container name " + this.containerName);
        } catch (ControllerException ex) {
            Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
        }
        String args[] = {this.containerName};

        this.ff = ExtFace.createExtFisherFaceRecognizer();
        this.lbph = ExtFace.createExtLBPHFaceRecognizer();
        File trainingFisher = new File("resources/input/FaceRecognition/trainingFisher.xml");
        File trainingLbph = new File("resources/input/FaceRecognition/trainingLbph.xml");
        if(trainingFisher.exists() && trainingLbph.exists())
        {
            System.out.println("Training files exist");
            System.out.print("Loading...");
            this.ff.load(trainingFisher.getPath());
            this.lbph.load(trainingLbph.getPath());
            System.out.println("done");
        }
        else System.out.println(this.getLocalName() + ": at first create training files");

        System.out.print(this.getLocalName() + ": creating agents Extractor, Detector and Contractor...");
        ContainerController cc = this.getContainerController();
        AgentController extractor,detector,contractor;
        try
        {
            String suffix = "";
            try
            {
                Integer.parseInt(this.containerName.substring(this.containerName.length() - 1));
                suffix = this.containerName.substring(this.containerName.length() - 1);
            } catch (NumberFormatException ex){
            }
            extractor = cc.createNewAgent("Extractor" + suffix,"agents.Extractor",args);
            detector = cc.createNewAgent("Detector" + suffix,"agents.Detector",args);
            contractor = cc.createNewAgent("Contractor" + suffix,"agents.Contractor",args);
            int sleepTime = this.setSleepTime(containerNumber);
            System.out.println(this.getLocalName() + " !!!!!!!!!!!!!!!!!!!!!!!! sleepTime: " + sleepTime + " [ms]");
            Thread.sleep(sleepTime);
            extractor.start();
            detector.start();
            contractor.start();
        } catch (StaleProxyException ex) {
            Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NumberFormatException ex){
        } catch (InterruptedException ex) {
            Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("done");
        System.out.println();
        
        ACLMessage toDF = CreateMessageForDF.createRegistrationMessage(this, 
               this.getLocalName() + "-local-service", "Local-recognition " + this.containerName);
        
        this.addBehaviour(new RegistrationService(this,toDF));
        
        LEAPCodec codec = new LEAPCodec();
        Ontology ont = Faces.getInstance();
        this.getContentManager().registerLanguage(codec);
        this.getContentManager().registerOntology(ont);
        
        this.addBehaviour(new WakerBehaviour(this,10000) {
            
            public void onWake()
            {
                ArrayList serviceTypes = new ArrayList();
                serviceTypes.add("Detection " + containerName);
                serviceTypes.add("Local-negotiation " + containerName);
                this.myAgent.addBehaviour(new SearchDetectorContractor(this.myAgent,ParallelBehaviour.WHEN_ALL,
                        1000,serviceTypes));
            }
        });
        
        this.addBehaviour(new ManageDFFault(this.containerName));
        
        /*if(containerNumber.equals("2"))
        {
            long waitingTime = 30000;
            System.out.println(this.getLocalName() + ": ################################### waiting time " + waitingTime);
            this.addBehaviour(new WakerBehaviour(this,waitingTime) {

                @Override
                public void onWake()
                {
                    System.out.println(this.myAgent.getLocalName() + ": ################################### terminating container...");
                    try 
                    {
                        WriteNetworkInfo.writeBytesExchanged((Recognizer) this.myAgent);
                        this.myAgent.getContainerController().kill();
                    } catch (StaleProxyException ex) {
                        Logger.getLogger(Contractor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        }*/
    }
    
    protected void takeDown()
    {
        ACLMessage toDF = CreateMessageForDF.createDeRegistrationMessage(this, 
                       this.getLocalName() + "-local-service", "Local-recognition " + this.containerName);
        
        this.addBehaviour(new DeRegistrationService(this,toDF));
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public ExtFisherFaceRecognizer getFf() {
        return ff;
    }

    public ExtLBPHFaceRecognizer getLbph() {
        return lbph;
    }

    public PrintWriter getWriterNetworkInfo() {
        return writerNetworkInfo;
    }
    
    public int setSleepTime(String containerNumber)
    {
        Map sleeps = new HashMap();
        sleeps.put("0", 50000);
        sleeps.put("1", 50000);
        sleeps.put("2", 40000);
        sleeps.put("3", 30000);
        sleeps.put("4", 20000);
        sleeps.put("5", 10000);
        sleeps.put("6", 0);
        return (int) sleeps.get(containerNumber);
    }
}
