/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.contractor.SearchDetectorRecognizer;
import behaviours.df.DeRegistrationService;
import behaviours.df.RegistrationService;
import behaviours.faultRecovery.ManageDFFault;
import jade.content.lang.leap.LEAPCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.core.behaviours.ParallelBehaviour;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Faces;
import org.opencv.core.Core;
import utilities.CreateMessageForDF;

/**
 *
 * @author matteo
 */
public class Contractor extends Agent{
    
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary("ExamplesMAS");
    }
    
    private String containerName;
    private Integer numFacesToSend = new Integer(0);
    private Integer tempNumFacesToSend = new Integer(0);
    private ArrayList facesToSend = new ArrayList();
    private Integer numFacesToPropose = new Integer(0);
    private ArrayList facesReceived = new ArrayList();
    private boolean iAmResponder = false;
    private PrintWriter writer;
    
    protected void setup()
    {   
        this.containerName = (String) this.getArguments()[0];
        
        ArrayList serviceNames = new ArrayList();
        ArrayList serviceTypes = new ArrayList();
        serviceNames.add(this.getLocalName() + "-local-service");
        serviceNames.add(this.getLocalName() + "-remote-service");
        serviceTypes.add("Local-negotiation " + this.containerName);
        serviceTypes.add("Remote-recognition");
        
        ACLMessage toDF = CreateMessageForDF.createMultipleRegistrationMessage(this, serviceNames, serviceTypes);
        
        this.addBehaviour(new RegistrationService(this,toDF));
        
        String containerNumber = "";
        try 
        {
            containerNumber = this.getLocalName().substring(this.getLocalName().length()-1);
            try
            {
                Integer.parseInt(containerNumber);
            }catch(NumberFormatException ex){
                containerNumber = "0";
            }
            this.writer = new PrintWriter("resources/output/rec_results/" + containerNumber + "_remote_recognition.txt","UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Contractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Contractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        LEAPCodec codec = new LEAPCodec();
        Ontology ont = Faces.getInstance();
        this.getContentManager().registerLanguage(codec);
        this.getContentManager().registerOntology(ont);
        
        serviceTypes = new ArrayList();
        serviceTypes.add("Detection " + this.containerName);
        serviceTypes.add("Local-recognition " + this.containerName);
        this.addBehaviour(new SearchDetectorRecognizer(this,ParallelBehaviour.WHEN_ALL,1000,serviceTypes));
        
        this.addBehaviour(new ManageDFFault(this.containerName));
    }
    
    protected void takeDown()
    {
        ACLMessage toDF = CreateMessageForDF.createDeRegistrationMessage(this, 
                       this.getLocalName() + "-local-service", "Local-negotiation " + this.containerName);
        
        this.addBehaviour(new DeRegistrationService(this,toDF));
    }
    
    public synchronized Integer getNumFacesToSend() {
        return numFacesToSend;
    }

    public synchronized void setNumFacesToSend(Integer numFacesToSend) {
        this.numFacesToSend = numFacesToSend;
    }

    public synchronized Integer getTempNumFacesToSend() {
        return tempNumFacesToSend;
    }

    public synchronized void setTempNumFacesToSend(Integer tempNumFacesToSend) {
        this.tempNumFacesToSend = tempNumFacesToSend;
    }

    public synchronized ArrayList getFacesToSend() {
        return facesToSend;
    }
    
    public synchronized Integer getNumFacesToPropose() {
        return numFacesToPropose;
    }

    public synchronized void setNumFacesToPropose(Integer numFacesToPropose) {
        this.numFacesToPropose = numFacesToPropose;
    }

    public synchronized ArrayList getFacesReceived() {
        return facesReceived;
    }

    public PrintWriter getWriter() {
        return writer;
    }
    
    public boolean isiAmResponder() {
        return iAmResponder;
    }

    public void setiAmResponder(boolean iAmResponder) {
        this.iAmResponder = iAmResponder;
    }
    
    public void resetAgentParameters()
    {
        this.getFacesReceived().clear();
        this.getFacesToSend().clear();
        this.setNumFacesToPropose(0);
        this.setNumFacesToSend(0);
        this.setTempNumFacesToSend(0);
        this.setiAmResponder(false);
    }
}
