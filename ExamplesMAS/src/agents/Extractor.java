/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.df.DeRegistrationService;
import behaviours.df.RegistrationService;
import behaviours.extractor.ExtractFrames;
import behaviours.faultRecovery.ManageDFFault;
import jade.content.lang.leap.LEAPCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import java.io.File;
import ontology.Faces;
import org.opencv.core.Core;
import org.opencv.videoio.VideoCapture;
import utilities.CreateMessageForDF;

/**
 *
 * @author matteo
 */

public class Extractor extends Agent{
    
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary("ExamplesMAS");
    }
    
    private String containerName; 
    
    protected void setup()
    {   
        this.containerName = (String) this.getArguments()[0];
        
        ACLMessage toDF = CreateMessageForDF.createRegistrationMessage(this, 
               this.getLocalName() + "-local-service", "Extraction " + this.containerName);
                
        this.addBehaviour(new RegistrationService(this,toDF));
        
        LEAPCodec codec = new LEAPCodec();
        Ontology ont = Faces.getInstance();
        this.getContentManager().registerLanguage(codec);
        this.getContentManager().registerOntology(ont);
        
        File videoFiles = new File("resources/input/Video/");
        String videoName = null;
        String containerNumber = this.getLocalName().substring(this.getLocalName().length()-1);
        for(File videoFile: videoFiles.listFiles())
        {
            if(videoFile.getPath().contains(".DS_Store")) videoFile.delete();
            else if(videoFile.getName().substring(0,1).contentEquals(containerNumber))
            {
                videoName = videoFile.getPath();
            }
        }
        if(videoName != null)
        {
            System.out.println(this.getLocalName() + ": video name " + videoName);
            this.addBehaviour(new ExtractFrames(this,new VideoCapture(videoName)));
        }
        else
        {
            System.out.println(this.getLocalName() + ": video whose name is related with the name of the container doesn't exist");
            System.out.println(this.getLocalName() + ": trying to take the first file in the directory...");
            containerNumber = "0";
            if(videoFiles.listFiles().length != 0)
            {
                videoName = videoFiles.listFiles()[0].getPath();
                System.out.println(this.getLocalName() + ": video file " + videoName);
                this.addBehaviour(new ExtractFrames(this,new VideoCapture(videoName)));
            }
            else
            {
                System.out.println(this.getLocalName() + ": any video exists");
            } 
        }
        
        this.addBehaviour(new ManageDFFault(this.containerName));
    }
    
    protected void takeDown()
    {
        ACLMessage toDF = CreateMessageForDF.createDeRegistrationMessage(this, 
                       this.getLocalName() + "-local-service", "Extraction " + this.containerName);
        
        this.addBehaviour(new DeRegistrationService(this,toDF));
        
    }
}
