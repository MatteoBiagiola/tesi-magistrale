/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.detector.SearchLocalAgents;
import behaviours.df.DeRegistrationService;
import behaviours.df.RegistrationService;
import behaviours.faultRecovery.ManageDFFault;
import jade.content.lang.leap.LEAPCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import ontology.Faces;
import org.opencv.core.Core;
import utilities.CreateMessageForDF;

/**
 *
 * @author matteo
 */
public class Detector extends Agent{
    
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary("ExamplesMAS");
    }
    
    private String containerName;
    
    protected void setup()
    {   
        this.containerName = (String) this.getArguments()[0];
        
        ACLMessage toDF = CreateMessageForDF.createRegistrationMessage(this, 
               this.getLocalName() + "-local-service", "Detection " + this.containerName);
        
        this.addBehaviour(new RegistrationService(this,toDF));
        
        LEAPCodec codec = new LEAPCodec();
        Ontology ont = Faces.getInstance();
        this.getContentManager().registerLanguage(codec);
        this.getContentManager().registerOntology(ont);
        this.addBehaviour(new WakerBehaviour(this,12000){
            @Override
            public void onWake()
            {
                ArrayList serviceTypes = new ArrayList();
                serviceTypes.add("Extraction " + containerName);
                serviceTypes.add("Local-recognition " + containerName);
                serviceTypes.add("Local-negotiation " + containerName);
                this.myAgent.addBehaviour(new SearchLocalAgents(this.myAgent,ParallelBehaviour.WHEN_ALL,
                        1000,serviceTypes));
            }
        });
        
        String containerNumber = this.getLocalName().substring(this.getLocalName().length()-1);
        try
        {
            Integer.parseInt(containerNumber);
        }catch(NumberFormatException ex){
            containerNumber = "0";
        }
        
        this.addBehaviour(new ManageDFFault(this.containerName));
        
    }
    
    protected void takeDown()
    {
        ACLMessage toDF = CreateMessageForDF.createDeRegistrationMessage(this, 
                       this.getLocalName() + "-local-service", "Detection " + this.containerName);
        
        this.addBehaviour(new DeRegistrationService(this,toDF));
    }
    
}
