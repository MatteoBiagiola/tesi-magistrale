/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.AgentActionSchema;
import jade.content.schema.ConceptSchema;
import jade.content.schema.ObjectSchema;
import jade.content.schema.PredicateSchema;
import jade.content.schema.PrimitiveSchema;
import jade.content.schema.TermSchema;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matteo
 */
public class Faces extends Ontology{
    
    public static final int timeForRecognition = 2500; //[ms]
    public static final int classNum = 20;
    public static final int imgNumPerClass = 18;
    
    public static final String ONT_NAME = "Faces";
    
    public static final String DETECT = "Detect";
    public static final String RECOGNIZE = "Recognize";
    public static final String CONTRACT = "Contract";
    
    public static final String SLOT = "Slot";
    public static final String START = "start";
    public static final String PERIOD = "period";
    
    public static final String FRAME = "Frame";
    public static final String FRAME_IMAGE = "frameImage";
    public static final String FRAME_NUMBER = "frameNumber";
    
    public static final String CUTTED_FACE = "CuttedFace";
    public static final String CUT_FACE = "cutface";
    public static final String FRAME_NUM = "frameNum";
    public static final String FACE_NUM = "faceNum";
    public static final String SIZE = "size";
    public static final String PREDICT_LABEL = "predictlabel";
    public static final String CONFIDENCE = "confidence";
    
    public static final String NUM_FACES_TO_SEND = "numFacesToSend";
    public static final String NUM_FACES_TO_PROPOSE = "numFacesToPropose";
    public static final String SENDER = "sender";
    public static final String DEF_ACCEPT_CONTENT = "defAcceptContent";
    
    public static final String CUTTED_FACES = "cuttedFaces";
    
    private static int recognized =  0;
    private static int remote_recognized = 0;
    private static int detected = 0;
    private static int frameTotalBytes = 0;
    
    private static Ontology instance = new Faces();
    
    private Faces()
    {
        super(ONT_NAME, BasicOntology.getInstance());
        try 
        {
            this.add(new PredicateSchema(SLOT),Slot.class);
            this.add(new ConceptSchema(FRAME),Frame.class);
            this.add(new ConceptSchema(CUTTED_FACE),CuttedFace.class);
            this.add(new AgentActionSchema(DETECT),Detect.class);
            this.add(new AgentActionSchema(RECOGNIZE),Recognize.class);
            this.add(new AgentActionSchema(CONTRACT),Contract.class);
            
            PredicateSchema slot = (PredicateSchema) this.getSchema(SLOT);
            slot.add(START, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER));
            slot.add(PERIOD, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER), ObjectSchema.OPTIONAL);
            
            ConceptSchema frame = (ConceptSchema) this.getSchema(FRAME);
            frame.add(FRAME_IMAGE, (PrimitiveSchema) this.getSchema(BasicOntology.BYTE_SEQUENCE));
            frame.add(FRAME_NUMBER, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER));
            
            ConceptSchema cutface = (ConceptSchema) this.getSchema(CUTTED_FACE);
            cutface.add(CUT_FACE, (PrimitiveSchema) this.getSchema(BasicOntology.BYTE_SEQUENCE), ObjectSchema.OPTIONAL);
            cutface.add(FRAME_NUM, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER));
            cutface.add(FACE_NUM, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER));
            cutface.add(SIZE, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER), ObjectSchema.OPTIONAL);
            cutface.add(PREDICT_LABEL, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER), ObjectSchema.OPTIONAL);
            cutface.add(CONFIDENCE, (PrimitiveSchema) this.getSchema(BasicOntology.FLOAT), ObjectSchema.OPTIONAL);
            cutface.add(SENDER, (TermSchema) this.getSchema(BasicOntology.AID), ObjectSchema.OPTIONAL);
            
            
            AgentActionSchema detection = (AgentActionSchema) this.getSchema(DETECT);
            detection.add(FRAME,frame);
            
            AgentActionSchema recognition = (AgentActionSchema) this.getSchema(RECOGNIZE);
            recognition.add(CUTTED_FACES,(ConceptSchema) this.getSchema(CUTTED_FACE),ObjectSchema.MANDATORY,ObjectSchema.UNLIMITED);
            
            AgentActionSchema contracting = (AgentActionSchema) this.getSchema(CONTRACT);
            contracting.add(CUTTED_FACES,(ConceptSchema) this.getSchema(CUTTED_FACE),ObjectSchema.MANDATORY,ObjectSchema.UNLIMITED);
            contracting.add(NUM_FACES_TO_SEND, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER), ObjectSchema.OPTIONAL);
            contracting.add(NUM_FACES_TO_PROPOSE, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER), ObjectSchema.OPTIONAL);
            contracting.add(DEF_ACCEPT_CONTENT, (TermSchema) this.getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            
            
        } catch (OntologyException ex) {
            Logger.getLogger(Faces.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Ontology getInstance(){
	return instance;
    }
    
    public static int getRecognized() {
        return recognized;
    }

    public static void setRecognized(int aRecognized) {
        recognized = aRecognized;
    }

    public static int getRemote_recognized() {
        return remote_recognized;
    }

    public static void setRemote_recognized(int aRemote_recognized) {
        remote_recognized = aRemote_recognized;
    }

    public static int getDetected() {
        return detected;
    }

    public static void setDetected(int aDetected) {
        detected = aDetected;
    }

    public static int getFrameTotalBytes() {
        return frameTotalBytes;
    }

    public static void setFrameTotalBytes(int aFrameTotalBytes) {
        frameTotalBytes = aFrameTotalBytes;
    }
}
