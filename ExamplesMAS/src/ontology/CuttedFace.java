/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

import jade.content.Concept;
import jade.core.AID;

/**
 *
 * @author matteo
 */
public class CuttedFace implements Concept{
    private byte[] cutface;
    private int frameNum;
    private int faceNum;
    private int size;
    private int predictlabel;
    private double confidence;
    private AID sender;

    public byte[] getCutface() {
        return cutface;
    }

    public void setCutface(byte[] cutface) {
        this.cutface = cutface;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public int getPredictlabel() {
        return predictlabel;
    }

    public void setPredictlabel(int predictlabel) {
        this.predictlabel = predictlabel;
    }

    public int getFrameNum() {
        return frameNum;
    }

    public void setFrameNum(int frameNum) {
        this.frameNum = frameNum;
    }

    public int getFaceNum() {
        return faceNum;
    }

    public void setFaceNum(int faceNum) {
        this.faceNum = faceNum;
    }

    public AID getSender() {
        return sender;
    }

    public void setSender(AID sender) {
        this.sender = sender;
    }
    
}
