/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

import jade.content.AgentAction;
import jade.util.leap.ArrayList;

/**
 *
 * @author matteo
 */
public class Contract implements AgentAction{
    private ArrayList cuttedFaces = new ArrayList();
    private Integer numFacesToSend;
    private Integer numFacesToPropose;
    private String defAcceptContent;

    public Integer getNumFacesToSend() {
        return numFacesToSend;
    }

    public void setNumFacesToSend(Integer numFacesToSend) {
        this.numFacesToSend = numFacesToSend;
    }

    public Integer getNumFacesToPropose() {
        return numFacesToPropose;
    }

    public void setNumFacesToPropose(Integer numFacesToPropose) {
        this.numFacesToPropose = numFacesToPropose;
    }

    public String getDefAcceptContent() {
        return defAcceptContent;
    }

    public void setDefAcceptContent(String defAcceptContent) {
        this.defAcceptContent = defAcceptContent;
    }
    
    public ArrayList getCuttedFaces() {
        return cuttedFaces;
    }

    public void setCuttedFaces(ArrayList cuttedFaces) {
        this.cuttedFaces = cuttedFaces;
    }
}
