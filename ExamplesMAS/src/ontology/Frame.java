/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

/**
 *
 * @author matteo
 */
import jade.content.Concept;

public class Frame implements Concept{
	
    private int frameNumber;
    private byte[] frameImage;

    public int getFrameNumber() {
        return frameNumber;
    }

    public void setFrameNumber(int frameNumber) {
        this.frameNumber = frameNumber;
    }

    public byte[] getFrameImage() {
        return frameImage;
    }

    public void setFrameImage(byte[] frameImage) {
        this.frameImage = frameImage;
    }


}
