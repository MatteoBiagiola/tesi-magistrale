/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.faultRecovery;

import behaviours.df.RegistrationService;
import jade.domain.introspection.AMSSubscriber;
import jade.domain.introspection.BornAgent;
import jade.domain.introspection.Event;
import jade.domain.introspection.IntrospectionVocabulary;
import jade.domain.introspection.RemovedContainer;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import java.util.Map;
import utilities.CreateMessageForDF;

/**
 *
 * @author matteo
 */
public class ManageDFFault extends AMSSubscriber{

    protected static final String BORN_AGENT = "Born_agent";
    protected static final String REMOVED_CONTAINER = "Removed_container";
    protected static final String REMOVED_MAIN_INSTANT = "Removed_main_instant";
    
    private String containerName;
    
    public ManageDFFault(String containerName)
    {
        super();
        this.containerName = containerName;
    }
    
    @Override
    protected void installHandlers(Map handlers) {
        
        ManageDFFault.EventHandler creationsHandler;
        cleanDataStoreVariables();
        
        creationsHandler = (Event event) -> {
            RemovedContainer rc = (RemovedContainer) event;
            System.out.println(myAgent.getLocalName() + ": RemovedContainer "+ rc.getName()
                    + " container: " + rc.getContainer());
            if(rc.getContainer().getName().contains("Main"))
            {
                System.out.println(myAgent.getLocalName() + ": " + rc.getContainer().getName() + " removed container");
                getDataStore().put(REMOVED_CONTAINER, true);
                getDataStore().put(REMOVED_MAIN_INSTANT, System.currentTimeMillis());
                System.out.println("???????????????????????????????????????? " + this.myAgent.getLocalName() + ": removed main instant " 
                        + getDataStore().get(REMOVED_MAIN_INSTANT));
            }
            else
            {
                cleanDataStoreVariables();
            }
        };
        handlers.put(IntrospectionVocabulary.REMOVEDCONTAINER,creationsHandler);
        
        creationsHandler = (Event event) -> {
            BornAgent ba = (BornAgent) event;
            boolean removedMain = (boolean) getDataStore().get(REMOVED_CONTAINER);
            if(ba.getClassName().contains("ams") && removedMain)
            {
                System.out.println(myAgent.getLocalName() + ": BornAgent "+ ba.getClassName());
                long removedMainInstant = (long) getDataStore().get(REMOVED_MAIN_INSTANT);
                checkService(removedMainInstant);
            }
            else
            {
                cleanDataStoreVariables();
            }
        };
        handlers.put(IntrospectionVocabulary.BORNAGENT,creationsHandler);
        
    }
    
    protected void checkService(long removedMainInstant)
    {
        System.out.println(this.myAgent.getLocalName() + ": CheckService");
        cleanDataStoreVariables();
        System.out.println(this.myAgent.getLocalName() + ": re-register services after fault");
        if(this.myAgent.getLocalName().contains("Contractor"))
        {
            ArrayList serviceNames = new ArrayList();
            ArrayList serviceTypes = new ArrayList();
            serviceNames.add(this.myAgent.getLocalName() + "-local-service");
            serviceNames.add(this.myAgent.getLocalName() + "-remote-service");
            serviceTypes.add("Local-negotiation " + this.containerName);
            serviceTypes.add("Remote-recognition");
            ACLMessage toDF = CreateMessageForDF.createMultipleRegistrationMessage(this.myAgent,
                    serviceNames,serviceTypes);
            this.myAgent.addBehaviour(new RegistrationService(this.myAgent,toDF,removedMainInstant));
        }
        else if(this.myAgent.getLocalName().contains("Recognizer"))
        {
            ACLMessage toDF = CreateMessageForDF.createRegistrationMessage(this.myAgent, 
               this.myAgent.getLocalName() + "-local-service", "Local-recognition " + this.containerName);
            this.myAgent.addBehaviour(new RegistrationService(this.myAgent,toDF,removedMainInstant));
        }
        else if(this.myAgent.getLocalName().contains("Detector"))
        {
            ACLMessage toDF = CreateMessageForDF.createRegistrationMessage(this.myAgent, 
               this.myAgent.getLocalName() + "-local-service", "Detection " + this.containerName);
            this.myAgent.addBehaviour(new RegistrationService(this.myAgent,toDF,removedMainInstant));
        }
        else if(this.myAgent.getLocalName().contains("Extractor"))
        {
            ACLMessage toDF = CreateMessageForDF.createRegistrationMessage(this.myAgent, 
               this.myAgent.getLocalName() + "-local-service", "Extraction " + this.containerName);
            this.myAgent.addBehaviour(new RegistrationService(this.myAgent,toDF,removedMainInstant));
        }
    }
    
    protected void cleanDataStoreVariables()
    {
        getDataStore().put(BORN_AGENT, false);
        getDataStore().put(REMOVED_CONTAINER, false);
        getDataStore().put(REMOVED_MAIN_INSTANT, 0);
    }
}
