/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor;

import agents.Contractor;
import behaviours.contractor.initiator.ProtocolInitiator;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Contract;
import ontology.CuttedFace;
import ontology.Faces;

/**
 *
 * @author matteo
 */
public class ReceiveCancelMessage extends CyclicBehaviour{

    MessageTemplate myTemplate;
    private Contractor contractor;
    private AID detectorAID;
    private AID recognizerAID;
    
    public ReceiveCancelMessage(Agent a, AID detectorAID, AID recognizerAID)
    {
        super(a);
        MessageTemplate tpl = MessageTemplate.or(MessageTemplate.MatchPerformative(ACLMessage.CANCEL)
                ,MessageTemplate.MatchPerformative(ACLMessage.INFORM_REF));
        this.myTemplate = MessageTemplate.and(tpl, 
                MessageTemplate.MatchOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName()));
        this.contractor = (Contractor) a;
        this.detectorAID = detectorAID;
        this.recognizerAID = recognizerAID;
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(this.myTemplate);
        if(msg != null)
        {
            if(msg.getPerformative() == ACLMessage.CANCEL)
            {
                System.out.println(this.myAgent.getLocalName() + ": cancel message received");
                Behaviour b = null;
                Iterator it = this.myAgent.getBehaviours().iterator();
                while(it.hasNext())
                {
                    b = (Behaviour) it.next();
                    if(!b.equals(this) && !b.getBehaviourName().contains("Cancel") 
                            && !b.getBehaviourName().contains("Manage"))
                    {
                        if(b.getBehaviourName().contains("Initiator"))
                        {
                            ProtocolInitiator ib = (ProtocolInitiator) b;
                            ib.deleteAllInitialReceivers();
                        }
                        this.myAgent.removeBehaviour(b);
                        System.out.println(this.myAgent.getLocalName() + ": behaviour " + b.getBehaviourName() +
                                " deleted");
                    }
                }
                b = new ReceiveMessages(this.myAgent,this.detectorAID,this.recognizerAID);
                this.myAgent.addBehaviour(b);
                System.out.println(this.myAgent.getLocalName() + ": behaviour " + b.getBehaviourName() +
                                " added");
                System.out.println(this.myAgent.getLocalName() + ": behaviour " + this.getBehaviourName() +
                                " deleted");

                //send faces to local recognizer if myAgent is a Responder
                if(this.contractor.isiAmResponder())
                {
                    System.out.println(this.myAgent.getLocalName() + ": send faces to " + msg.getSender().getLocalName());
                    Contract contract = new Contract();
                    ArrayList facesReceived = new ArrayList();
                    ACLMessage reply = msg.createReply();
                    reply.setConversationId("fd");//at least one face detected
                    reply.setPerformative(ACLMessage.INFORM);
                    for(int i = 0; i < this.contractor.getFacesReceived().size(); i++)
                    {
                        facesReceived.add(this.contractor.getFacesReceived().get(i));
                    }
                    contract.setCuttedFaces(facesReceived);
                    this.contractor.getFacesReceived().clear();
                    reply.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
                    reply.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                    try 
                    {
                        this.myAgent.getContentManager().fillContent(reply,contract);
                        this.myAgent.send(reply);
                    } catch (Codec.CodecException ex) {
                        Logger.getLogger(ReceiveCancelMessage.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (OntologyException ex) {
                        Logger.getLogger(ReceiveCancelMessage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                //reset agent parameters for the next time slot
                this.contractor.resetAgentParameters();
                
                this.myAgent.removeBehaviour(this);
            }
            else if(msg.getPerformative() == ACLMessage.INFORM_REF)
            {
                System.out.println("+++++++++++++++++ : recognition results received from " + msg.getSender().getLocalName());
                System.out.println(this.myAgent.getLocalName() + ": recognition results received");
                //recognition results coming from remote Recognizer agents; write results in a file.
                try 
                {
                    Contract contract = (Contract) this.myAgent.getContentManager().extractContent(msg);
                    ArrayList cuttedFaces = contract.getCuttedFaces();
                    Faces.setRemote_recognized(Faces.getRemote_recognized() + cuttedFaces.size());
                    for(int i = 0; i < cuttedFaces.size(); i++)
                    {
                        CuttedFace cutFace = (CuttedFace) cuttedFaces.get(i);
                        this.contractor.getWriter().println(
                                  "Frame number: " + cutFace.getFrameNum()
                                + " Face number: " + cutFace.getFaceNum()
                                + " Label: " + cutFace.getPredictlabel()
                                + " Confidence: " + cutFace.getConfidence());
                        this.contractor.getWriter().flush();
                    }
                } catch (Codec.CodecException ex) {
                    Logger.getLogger(ReceiveCancelMessage.class.getName()).log(Level.SEVERE, null, ex);
                } catch (OntologyException ex) {
                    Logger.getLogger(ReceiveCancelMessage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }    
        }
        else
        {
            this.block();
        }
    }
    
}
