/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor.responder;

import agents.Contractor;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;
/**
 *
 * @author matteo
 */
public class CreateResponders extends OneShotBehaviour{

    private Contractor contractor = null;
    private ArrayList announces = new ArrayList();
    private Integer initFacesToPropose = new Integer(0);
    
    public CreateResponders(Agent a, ArrayList announces)
    {
        super(a);
        this.contractor = (Contractor) a;
        this.initFacesToPropose = this.contractor.getNumFacesToPropose();
        
        for(int i = 0; i < announces.size(); i++)
        {
            this.announces.add(announces.get(i));
        }

    }
    
    @Override
    public void action() {
        for(Object o: announces.toArray())
        {
            System.out.println(this.myAgent.getLocalName() + ": responder created");
            ACLMessage msg = (ACLMessage) o;
            this.myAgent.addBehaviour(new ResponderBehaviour(this.myAgent,msg,this.initFacesToPropose));  
        }
        this.myAgent.addBehaviour(new MSResponder(this.myAgent,
                MessageTemplate.MatchPerformative(ACLMessage.ANNOUNCE),this.initFacesToPropose));
    }
}
