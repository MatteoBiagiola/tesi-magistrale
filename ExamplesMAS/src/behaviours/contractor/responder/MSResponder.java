/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor.responder;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SSResponderDispatcher;

/**
 *
 * @author matteo
 */
public class MSResponder extends SSResponderDispatcher{
    
    private Integer initFacesToPropose = new Integer(0);
    
    public MSResponder(Agent a, MessageTemplate tpl, Integer initFacesToPropose) {
        super(a, tpl);
        this.initFacesToPropose = initFacesToPropose;
    }

    @Override
    protected Behaviour createResponder(ACLMessage aclm) {
        System.out.println(this.myAgent.getLocalName() + ": new session");
        System.out.println(this.myAgent.getLocalName() + ": message received from " 
                            + aclm.getSender().getLocalName() + " with convId = " + aclm.getConversationId());
        return new ResponderBehaviour(this.myAgent,aclm,this.initFacesToPropose);
    }
    
}
