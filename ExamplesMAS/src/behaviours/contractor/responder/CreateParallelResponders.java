/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor.responder;

import agents.Contractor;
import behaviours.contractor.ReceiveMessages;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Contract;
import ontology.Faces;

/**
 *
 * @author matteo
 */
public class CreateParallelResponders extends OneShotBehaviour{
    
    private Contractor contractor;
    private AID recognizerAID;
    private AID detectorAID;
    private ArrayList announces = new ArrayList();
    private int policy = 0;
    private Integer initFacesToPropose = 0;
    private static ArrayList facesReceivedClone = new ArrayList();
    
    public CreateParallelResponders(Agent a, int policy, 
            ArrayList announces, AID detectorAID, AID recognizerAID, Integer initFacesToPropose)
    {
        super(a);
        this.contractor = (Contractor) a;
        this.policy = policy;
        this.detectorAID = detectorAID;
        this.recognizerAID = recognizerAID;
        this.initFacesToPropose = initFacesToPropose;
        for(int i = 0; i < announces.size(); i++)
        {
            this.announces.add(announces.get(i));
        }
    }

    @Override
    public void action() {
        ParallelBehaviour par = new ParallelBehaviour(this.myAgent,this.policy){
            @Override
            public int onEnd()
            {
                System.out.println(this.myAgent.getLocalName() + ": onEnd, send propagation message to recognizer");
                ACLMessage toRecognizer = new ACLMessage(ACLMessage.PROPAGATE);
                toRecognizer.addReceiver(recognizerAID);
                toRecognizer.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                this.myAgent.send(toRecognizer);
                contractor.getFacesReceived().clear();
                this.myAgent.addBehaviour(new ReceiveMessages(this.myAgent,detectorAID,recognizerAID));
                contractor.resetAgentParameters();
                return 0;
            }
        };
        Behaviour b = null;
        for(int i = 0; i < announces.size(); i++)
        {
            ACLMessage announce = (ACLMessage) announces.get(i);
            b = new ResponderBehaviour(this.myAgent,announce,this.initFacesToPropose){
                @Override
                public int onEnd()
                {
                    boolean defAcceptReceived = (boolean) this.getDataStore().get("DEF_ACCEPT_RECEIVED");
                    System.out.println(this.myAgent.getLocalName() + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>: responder ended defAcceptReceived = " + defAcceptReceived);
                    if(defAcceptReceived)
                    {
                        System.out.println(this.myAgent.getLocalName() + ": send faces to " + recognizerAID.getLocalName());
                        ACLMessage toRecognizer = new ACLMessage(ACLMessage.INFORM);
                        //set faces for Recognizer with placeholder
                        Contract contract = new Contract();
                        ArrayList facesToRecognizer = new ArrayList();
                        setFacesToRecognizer(facesToRecognizer);
                        if(facesToRecognizer.isEmpty()){
                            facesToRecognizer = CreateParallelResponders.getFacesReceivedClone();
                        }
                        contract.setCuttedFaces(facesToRecognizer);
                        toRecognizer.setConversationId("afd");//any face detected
                        toRecognizer.addReceiver(recognizerAID);
                        
                        toRecognizer.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
                        toRecognizer.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                        
                        
                        try {
                            this.myAgent.getContentManager().fillContent(toRecognizer,contract);
                            this.myAgent.send(toRecognizer);
                        } catch (Codec.CodecException | OntologyException ex) {
                            Logger.getLogger(CreateParallelResponders.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    return 0;
                }
            };
            par.addSubBehaviour(b);
        }
        this.myAgent.addBehaviour(par);
    }
    
    private void setFacesToRecognizer(ArrayList facesToRecognizer){
        if(CreateParallelResponders.getFacesReceivedClone().isEmpty()){
            CreateParallelResponders.setFacesReceivedClone(
                    (ArrayList) contractor.getFacesReceived().clone());
        }
        else
        {
            for(int i = 0; i< contractor.getFacesReceived().size(); i++)
            {
                if(!CreateParallelResponders.getFacesReceivedClone().
                        contains(contractor.getFacesReceived().get(i)))
                {
                    CreateParallelResponders.getFacesReceivedClone().
                            add(contractor.getFacesReceived().get(i));
                    facesToRecognizer.add(contractor.getFacesReceived().get(i));
                }
            }
        }
        
    }
    
    public static ArrayList getFacesReceivedClone() {
        return facesReceivedClone;
    }

    public static void setFacesReceivedClone(ArrayList aFacesReceivedClone) {
        facesReceivedClone = aFacesReceivedClone;
    }
}
