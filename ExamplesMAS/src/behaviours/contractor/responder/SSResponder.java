/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor.responder;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.states.MsgReceiver;
import jade.util.leap.HashMap;
import jade.util.leap.Map;

/**
 *
 * @author matteo
 */
public class SSResponder extends FSMBehaviour{
    
    private MessageTemplate myTemplate;
    private static int noMessageSent = -1;
    private Map preBidSent = new HashMap();
    private boolean isDefAcceptReceived = false;
    
    //FSM states
    protected static final String HANDLE_ANNOUNCE = "Handle_announce";
    protected static final String SEND_REPLY = "Send_reply";
    protected static final String RECEIVE_MESSAGE = "Receive_message";
    protected static final String CHECK_IN_SEQUENCE = "Check_in_sequence";
    protected static final String HANDLE_PRE_ACCEPT = "Handle_pre_accept";
    protected static final String HANDLE_PRE_REJECT = "Handle_pre_reject";
    protected static final String HANDLE_DEFINITIVE_ACCEPT = "Handle_definitive_accept";
    protected static final String HANDLE_DEFINITIVE_REJECT = "Handle_definitive_reject";
    protected static final String HANDLE_OUT_OF_SEQUENCE = "Handle_out_of_sequence";
    protected static final String DUMMY_FINAL = "Dummy Final";
    
    public SSResponder(Agent a,ACLMessage announce)
    {
        super(a);
        
        DataStore ds = this.getDataStore();
        ds.put("ANNOUNCE",announce);
        ds.put("DEF_ACCEPT_RECEIVED",isDefAcceptReceived);
        this.myTemplate = MessageTemplate.MatchConversationId(announce.getConversationId());
        
        this.registerDefaultTransition(HANDLE_ANNOUNCE,SEND_REPLY);
        this.registerTransition(SEND_REPLY,RECEIVE_MESSAGE,ACLMessage.PRE_BID);
        this.registerTransition(SEND_REPLY,RECEIVE_MESSAGE,ACLMessage.DEFINITIVE_BID);
        this.registerDefaultTransition(SEND_REPLY,DUMMY_FINAL);
        this.registerDefaultTransition(RECEIVE_MESSAGE,CHECK_IN_SEQUENCE);
        this.registerTransition(CHECK_IN_SEQUENCE,HANDLE_PRE_ACCEPT,ACLMessage.PRE_ACCEPT);
        this.registerTransition(CHECK_IN_SEQUENCE,HANDLE_PRE_REJECT,ACLMessage.PRE_REJECT);
        this.registerTransition(CHECK_IN_SEQUENCE,HANDLE_DEFINITIVE_ACCEPT,ACLMessage.DEFINITIVE_ACCEPT);
        this.registerTransition(CHECK_IN_SEQUENCE,HANDLE_DEFINITIVE_REJECT,ACLMessage.DEFINITIVE_REJECT);
        this.registerDefaultTransition(CHECK_IN_SEQUENCE,HANDLE_OUT_OF_SEQUENCE);
        this.registerDefaultTransition(HANDLE_OUT_OF_SEQUENCE,SEND_REPLY);
        this.registerDefaultTransition(HANDLE_PRE_ACCEPT,SEND_REPLY);
        this.registerDefaultTransition(HANDLE_PRE_REJECT,SEND_REPLY);
        this.registerDefaultTransition(HANDLE_DEFINITIVE_ACCEPT,SEND_REPLY);
        this.registerDefaultTransition(HANDLE_DEFINITIVE_REJECT,DUMMY_FINAL);
        
        Behaviour b = null;
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + HANDLE_ANNOUNCE);
                System.out.println(this.myAgent.getLocalName() + ": current session is " + announce.getReplyWith());
                ACLMessage announce = (ACLMessage) this.getDataStore().get("ANNOUNCE");
                ACLMessage toSend = handleAnnounce(announce);
                this.getDataStore().put("PRE_BID",toSend);
                preBidSent.put(toSend.getConversationId(),toSend);
                this.getDataStore().put("TO_SEND",toSend);
            }
        };
        b.setDataStore(ds);
        this.registerFirstState(b,HANDLE_ANNOUNCE);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + SEND_REPLY);
                ACLMessage toSend = (ACLMessage) this.getDataStore().get("TO_SEND");
                ACLMessage received = (ACLMessage) this.getDataStore().get("RECEIVED");
                if(toSend != null)
                {
                    if(received == null)
                    {
                        received = (ACLMessage) this.getDataStore().get("ANNOUNCE");
                    }
                    String replyWith = received.getReplyWith();
                    toSend.setInReplyTo(replyWith);
                    this.myAgent.send(toSend);
                }
            }
            
            @Override
            public int onEnd()
            {
                ACLMessage toSend = (ACLMessage) this.getDataStore().get("TO_SEND");
                if(toSend == null)
                {
                    return noMessageSent;
                }
                else return toSend.getPerformative();
            }
        };
        b.setDataStore(ds);
        this.registerState(b,SEND_REPLY);
        
        b = new MsgReceiver(this.myAgent,this.myTemplate,MsgReceiver.INFINITE,ds,"RECEIVED"){
            @Override
            protected void handleMessage(ACLMessage msg)
            {
                System.out.println(this.myAgent.getLocalName() + ": state " + RECEIVE_MESSAGE);
                System.out.println(this.myAgent.getLocalName() + ": current session is " 
                        + msg.getReplyWith());
            }
        };
        b.setDataStore(ds);
        this.registerState(b,RECEIVE_MESSAGE);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + CHECK_IN_SEQUENCE);
            }
            
            @Override
            public int onEnd()
            {
                ACLMessage received = (ACLMessage) this.getDataStore().get("RECEIVED");
                return received.getPerformative();
            }
        };
        b.setDataStore(ds);
        this.registerState(b,CHECK_IN_SEQUENCE);
       
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + HANDLE_PRE_ACCEPT);
                ACLMessage preAccept = (ACLMessage) this.getDataStore().get("RECEIVED");
                ACLMessage preBid = (ACLMessage) this.getDataStore().get("PRE_BID");
                ACLMessage toSend = handlePreAccept(preAccept,preBid);
                this.getDataStore().put("DEF_BID",toSend);
                this.getDataStore().put("TO_SEND",toSend);
            }
        };
        b.setDataStore(ds);
        this.registerState(b,HANDLE_PRE_ACCEPT);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + HANDLE_PRE_REJECT);
                ACLMessage preReject = (ACLMessage) this.getDataStore().get("RECEIVED");
                ACLMessage toSend = handlePreReject(preReject);
                this.getDataStore().put("PRE_BID",toSend);
                preBidSent.put(toSend.getConversationId(),toSend);
                this.getDataStore().put("TO_SEND",toSend);
            }
        };
        b.setDataStore(ds);
        this.registerState(b,HANDLE_PRE_REJECT);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + HANDLE_DEFINITIVE_ACCEPT);
                ACLMessage defAccept = (ACLMessage) this.getDataStore().get("RECEIVED");
                ACLMessage defBid = (ACLMessage) this.getDataStore().get("DEF_BID");
                handleDefAccept(defAccept,defBid);
                isDefAcceptReceived = true;
                this.getDataStore().put("DEF_ACCEPT_RECEIVED",isDefAcceptReceived);
                this.getDataStore().remove("TO_SEND");
                //add to faces queue of Responder faces received
            }
        };
        b.setDataStore(ds);
        this.registerState(b,HANDLE_DEFINITIVE_ACCEPT);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + HANDLE_DEFINITIVE_REJECT);
                ACLMessage defReject = (ACLMessage) this.getDataStore().get("RECEIVED");
                ACLMessage lastPreBid = (ACLMessage) preBidSent.get(defReject.getConversationId());
                handleDefReject(defReject,lastPreBid);
                this.getDataStore().remove("TO_SEND");
            }
        };
        b.setDataStore(ds);
        this.registerState(b,HANDLE_DEFINITIVE_REJECT);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + HANDLE_OUT_OF_SEQUENCE);
            }
        };
        b.setDataStore(ds);
        this.registerState(b,HANDLE_OUT_OF_SEQUENCE);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + DUMMY_FINAL);
            }
        };
        b.setDataStore(ds);
        this.registerLastState(b,DUMMY_FINAL);
        
    }
    
    protected ACLMessage handleAnnounce(ACLMessage announce)
    {
        return null;
    }
    
    protected ACLMessage handlePreAccept(ACLMessage preAccept, ACLMessage preBid)
    {
        return null;
    }
    
    protected ACLMessage handlePreReject(ACLMessage preReject)
    {
        return null;
    }
    
    protected void handleDefAccept(ACLMessage defAccept,ACLMessage defBid)
    {
        
    }
    
    protected void handleDefReject(ACLMessage defReject, ACLMessage lastPreBid)
    {
        
    }
    
    protected void handleOutOfSequence(ACLMessage outOfSequence)
    {
        
    }
}
