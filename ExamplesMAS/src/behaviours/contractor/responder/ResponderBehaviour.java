/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor.responder;

import agents.Contractor;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Contract;

/**
 *
 * @author matteo
 */
public class ResponderBehaviour extends SSResponder{
    
    private Contractor contractor = null;
    private Integer initFacesToPropose = new Integer(0);
    
    public ResponderBehaviour(Agent a, ACLMessage announce, Integer initFacesToPropose) {
        super(a, announce);
        this.initFacesToPropose = initFacesToPropose;
        this.contractor = (Contractor) a;
        System.out.println(this.myAgent.getLocalName() + ": initFacesToPropose " + this.initFacesToPropose);
    }
    
    protected ACLMessage handleAnnounce(ACLMessage announce)
    {
        System.out.println(this.myAgent.getLocalName() + ": handle announce with content " +
                announce.getContent());
        ACLMessage toSend = announce.createReply();
        //if the init proposal is 0 then send a refuse message to initiator
        if((this.checkExhaustedProposal(announce)))
        {
            toSend.setPerformative(ACLMessage.REFUSE);
            return toSend;
        }
        toSend.setPerformative(ACLMessage.PRE_BID);
        Integer supply = Integer.parseInt(announce.getContent());
        if(this.contractor.getNumFacesToPropose() - supply > 0)
        {
            toSend.setContent(Integer.toString(supply));
            this.contractor.setNumFacesToPropose(this.contractor.getNumFacesToPropose() - supply);
            System.out.println(this.myAgent.getLocalName() + ": proposal = " + supply);
            System.out.println(this.myAgent.getLocalName() + ": faces to propose = " + this.contractor.getNumFacesToPropose());
        }
        else
        {
            toSend.setContent(Integer.toString(this.contractor.getNumFacesToPropose()));
            System.out.println(this.myAgent.getLocalName() + ": proposal = " + this.contractor.getNumFacesToPropose());
            this.contractor.setNumFacesToPropose(0);
            System.out.println(this.myAgent.getLocalName() + ": faces to propose = " + this.contractor.getNumFacesToPropose());
        }
        return toSend;
    }
    
    protected ACLMessage handlePreAccept(ACLMessage preAccept, ACLMessage preBid)
    {
        System.out.println(this.myAgent.getLocalName() + ": handle pre accept with content " +
                preAccept.getContent());
        ACLMessage toSend = preAccept.createReply();
        toSend.setPerformative(ACLMessage.DEFINITIVE_BID);
        Integer supply = Integer.parseInt(preAccept.getContent());
        Integer previousBid = Integer.parseInt(preBid.getContent());
        if(previousBid - supply > 0)
        {
            toSend.setContent(Integer.toString(supply));
            this.contractor.setNumFacesToPropose(this.contractor.getNumFacesToPropose() + 
                    (previousBid - supply));
            System.out.println(this.myAgent.getLocalName() + ": proposal = " + supply);
            System.out.println(this.myAgent.getLocalName() + ": faces to propose = " + this.contractor.getNumFacesToPropose());
        }
        else
        {
            toSend.setContent(Integer.toString(supply));
            System.out.println(this.myAgent.getLocalName() + ": proposal = " + supply);
            System.out.println(this.myAgent.getLocalName() + ": faces to propose = " + this.contractor.getNumFacesToPropose());
        }
        return toSend;
    }
    
    protected ACLMessage handlePreReject(ACLMessage preReject)
    {
        System.out.println(this.myAgent.getLocalName() + ": handle pre reject with content " +
                preReject.getContent());
        ACLMessage toSend = preReject.createReply();
        if(this.checkExhaustedProposal(preReject))
        {
            toSend.setPerformative(ACLMessage.REFUSE);
            return toSend;
        }
        Integer supply = Integer.parseInt(preReject.getContent());
        toSend.setPerformative(ACLMessage.PRE_BID);
        /*modify the code: supply can't never be > 0 in a PreReject message modifying the code of the method handlePreBid
        as written in ProtocolInitiator behaviour*/
        toSend.setContent(Integer.toString(this.contractor.getNumFacesToPropose()));
        System.out.println(this.myAgent.getLocalName() + ": proposal = " + this.contractor.getNumFacesToPropose());
        this.contractor.setNumFacesToPropose(0);
        /*if(supply > 0)
        {
            toSend.setContent(Integer.toString(supply));
            System.out.println(this.myAgent.getLocalName() + ": proposal = " + supply);
        }
        else
        {
            toSend.setContent(Integer.toString(this.contractor.getNumFacesToPropose()));
            System.out.println(this.myAgent.getLocalName() + ": proposal = " + this.contractor.getNumFacesToPropose());
            this.contractor.setNumFacesToPropose(0);
        }*/
        System.out.println(this.myAgent.getLocalName() + ": faces to propose = " + this.contractor.getNumFacesToPropose());
        return toSend;
    }
    
    protected void handleDefAccept(ACLMessage defAccept, ACLMessage defBid)
    {
        try 
        {
            Contract contract = (Contract) this.myAgent.getContentManager().extractContent(defAccept);
            System.out.println(this.myAgent.getLocalName() + ": handle definitive accept with content " +
                    contract.getDefAcceptContent());
            Integer previousBid = Integer.parseInt(defBid.getContent());
            Integer supply = Integer.parseInt(contract.getDefAcceptContent());
            if(previousBid - supply > 0)
            {
                this.contractor.setNumFacesToPropose(this.contractor.getNumFacesToPropose() + (previousBid - supply));
            }
            for(int i = 0; i < supply; i++)
            {
                this.contractor.getFacesReceived().add(contract.getCuttedFaces().get(i));
            }
            System.out.println(this.myAgent.getLocalName() + " negotiation ended with " + defAccept.getSender().getLocalName()
                    + " with contract " + contract.getDefAcceptContent() +": faces to propose = " + this.contractor.getNumFacesToPropose());
        } catch (Codec.CodecException ex) {
            Logger.getLogger(ResponderBehaviour.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(ResponderBehaviour.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected void handleDefReject(ACLMessage defReject, ACLMessage lastPreBid)
    {
        System.out.println(this.myAgent.getLocalName() + ": handle definitive reject with content " +
                defReject.getContent());
        Integer bid = Integer.parseInt(defReject.getContent());
        if(bid > 0)
        {
            this.contractor.setNumFacesToPropose(this.contractor.getNumFacesToPropose() + bid);
        }
        else if(bid == -1)
        {
            System.out.println("@@@@@@@@@@@@@@@@@@DefReject with content " + bid + " received");
            System.out.println("@@@@@@@@@@@@@@@@@@LastPreBid with content " + lastPreBid.getContent() + " received");
            this.contractor.setNumFacesToPropose(this.contractor.getNumFacesToPropose() 
                    + Integer.parseInt(lastPreBid.getContent()));
            System.out.println("@@@@@@@@@@@@@@@@@@NumFacesToPropose " + this.contractor.getNumFacesToPropose());
        }
        System.out.println(this.myAgent.getLocalName() + " negotiation ended with no contract with " 
                    + defReject.getSender().getLocalName() + ": faces to propose = " + this.contractor.getNumFacesToPropose());
    }
    
    protected void handleOutOfSequence(ACLMessage outOfSequence)
    {
        System.out.println(this.myAgent.getLocalName() + ": handle out of sequence with content " +
                outOfSequence.getContent());
    }
    
    private boolean checkExhaustedProposal(ACLMessage msg)
    {
        System.out.println("************************: sender "
                    + msg.getSender().getLocalName() + " size: " + this.contractor.getFacesReceived().size() 
                    + " initFacesToPropose: " + this.initFacesToPropose);
        if(this.contractor.getFacesReceived().size() == this.initFacesToPropose)
        {
            System.out.println(this.myAgent.getLocalName() + ": proposal exhausted send refuse message to "
                    + msg.getSender().getLocalName() + " size: " + this.contractor.getFacesReceived().size() 
                    + " initFacesToPropose: " + this.initFacesToPropose);
            return true;
        }
        return false;
    }
}
