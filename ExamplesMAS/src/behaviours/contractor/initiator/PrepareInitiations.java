/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor.initiator;

import agents.Contractor;
import behaviours.df.DFInteraction;
import behaviours.df.SearchService;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author matteo
 */
public class PrepareInitiations extends DFInteraction{

    private Contractor contractor = null;
    private ACLMessage msg;
    private long waitBeforeRequery;
    
    public PrepareInitiations(Agent a, long waitBeforeRequery, ACLMessage msg)
    {
        super(a,waitBeforeRequery,msg);
        this.msg = msg;
        this.waitBeforeRequery = waitBeforeRequery;
        this.contractor = (Contractor) a;
    }
    
    @Override
    protected void handleSearchResults(AID[] agents)
    {
        if(agents.length > 0)
        {
            ACLMessage announce = new ACLMessage(ACLMessage.ANNOUNCE);
            for(int i = 0; i < agents.length; i++)
            {
                //exclude myAgent from the receivers
                if(!agents[i].getLocalName().equals(this.myAgent.getLocalName()))
                {
                    announce.addReceiver(agents[i]);
                    announce.setContent(Integer.toString(this.contractor.getTempNumFacesToSend()));
                }  
            }
            this.myAgent.addBehaviour(new ProtocolInitiator(this.myAgent,announce));
        }
    }
    
    @Override
    protected void handlePeriodElapsed()
    {
        System.out.println(this.myAgent.getLocalName() + ": period elapsed");
        this.myAgent.addBehaviour(new SearchService(this.myAgent,
                this.waitBeforeRequery,this.msg));
    }
}
