/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor.initiator;

import agents.Contractor;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import jade.util.leap.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Contract;
import ontology.CuttedFace;
import ontology.Faces;

/**
 *
 * @author matteo
 */
public class ProtocolInitiator extends InitiatorBehaviour{
    
    private Contractor contractor = null;
    
    public ProtocolInitiator(Agent a, ACLMessage announce) {
        super(a, announce);
        this.contractor = (Contractor) a;
    }
    
    protected ACLMessage handlePreBid(ACLMessage preBid, ACLMessage msgSent)
    {
        System.out.println(this.myAgent.getLocalName() + ": handle preBid with content " +
                preBid.getContent());
        Integer bid = Integer.parseInt(preBid.getContent());
        ACLMessage toSend = preBid.createReply();
        if(bid == 0)
        {
            if(this.contractor.getTempNumFacesToSend() == 0)
            {
                toSend.setPerformative(ACLMessage.DEFINITIVE_REJECT);
                toSend.setContent(Integer.toString(bid));
                System.out.println(this.myAgent.getLocalName() + " negotiation ended with no contract with " 
                    + preBid.getSender().getLocalName() + ": faces = " + this.contractor.getNumFacesToSend());
            }
            else
            {
                toSend.setPerformative(ACLMessage.PRE_REJECT);
                toSend.setContent(Integer.toString(bid));
            }
            /*if(msgSent.getPerformative() != ACLMessage.ANNOUNCE)
            {
                toSend.setPerformative(ACLMessage.DEFINITIVE_REJECT);
                toSend.setContent(Integer.toString(bid));
                if(this.initiator1 != null)
                {
                    System.out.println(this.myAgent.getLocalName() + " negotiation ended with no contract with " 
                        + preBid.getSender().getLocalName() + ": faces = " + this.initiator1.getFacesToSent());
                }
                else
                {
                    System.out.println(this.myAgent.getLocalName() + " negotiation ended with no contract with " 
                        + preBid.getSender().getLocalName() + ": faces = " + this.initiator2.getFacesToSent());
                }
            }*/
            /*else
            {
                toSend.setPerformative(ACLMessage.PRE_REJECT);
                toSend.setContent(Integer.toString(bid));
            }*/
        }
        else
        {
            // supply exhausted
            /*modify the code message DEFINITIVE_REJECT in both cases since tempNumFacesToSend counter when reach the zero value
            remains zero until the protocol doesn't finished; this because once a responder sends to the initiator a bid > 0
            and the initiator accepts it, then the protocol ended with a contract, since receiving a PreAccept a participant sends its
            DefinitiveBid because doesn't exist an announce that is better the the other. One could think that the best announce for a
            participant is the greater one: if, for example, a participant has 5 faces to propose and he receives two announces 
            of 4 and 5. He could accepts the first becuase could be received earlier than the other but he accepts also the other and
            the recognized faced would be always 5 and not 4.u*/
            if(this.contractor.getTempNumFacesToSend() == 0)
            {
                toSend.setPerformative(ACLMessage.DEFINITIVE_REJECT);
                toSend.setContent(Integer.toString(bid));
                System.out.println(this.myAgent.getLocalName() + " negotiation ended with no contract with " 
                            + preBid.getSender().getLocalName() + ": faces = " + this.contractor.getNumFacesToSend());
                /*if(this.contractor.getNumFacesToSend() == 0)
                {
                    toSend.setPerformative(ACLMessage.DEFINITIVE_REJECT);
                    toSend.setContent(Integer.toString(bid));
                    System.out.println(this.myAgent.getLocalName() + " negotiation ended with no contract with " 
                                + preBid.getSender().getLocalName() + ": faces = " + this.contractor.getNumFacesToSend());
                }
                else
                {
                    toSend.setPerformative(ACLMessage.PRE_REJECT);
                    toSend.setContent(Integer.toString(bid));
                    System.out.println(this.myAgent.getLocalName() + ": temp faces = " + this.contractor.getTempNumFacesToSend());
                }*/

            }
            // tempBid is greater than supply
            else if(this.contractor.getTempNumFacesToSend() - bid < 0)
            {
                toSend.setPerformative(ACLMessage.PRE_ACCEPT);
                toSend.setContent(Integer.toString(this.contractor.getTempNumFacesToSend()));
                this.contractor.setTempNumFacesToSend(0);
                System.out.println(this.myAgent.getLocalName() + ": temp faces = " + this.contractor.getTempNumFacesToSend());
            }
            // tempBid is less than supply
            else
            {
                toSend.setPerformative(ACLMessage.PRE_ACCEPT);
                toSend.setContent(Integer.toString(bid));
                this.contractor.setTempNumFacesToSend(this.contractor.getTempNumFacesToSend() - bid);
                System.out.println(this.myAgent.getLocalName() + ": temp faces = " + this.contractor.getTempNumFacesToSend());
            }
        }
        return toSend;
    }
    
    protected ACLMessage handleDefinitiveBid(ACLMessage defBid)
    {
        System.out.println(this.myAgent.getLocalName() + ": handle defBid with content " +
                defBid.getContent());
        Integer bid = Integer.parseInt(defBid.getContent());
        ACLMessage toSend = defBid.createReply();
        // supply exhausted
        if((this.contractor.getNumFacesToSend() == 0) || (bid == 0))
        {
            toSend.setPerformative(ACLMessage.DEFINITIVE_REJECT);
            toSend.setContent(Integer.toString(bid));
            System.out.println(this.myAgent.getLocalName() + " negotiation ended with no contract with " 
                    + defBid.getSender().getLocalName() + ": faces = " + this.contractor.getNumFacesToSend());
        }
        // defBid is greater than supply
        else if(this.contractor.getNumFacesToSend() - bid < 0)
        {
            toSend.setPerformative(ACLMessage.DEFINITIVE_ACCEPT);
            toSend.setContent(Integer.toString(this.contractor.getNumFacesToSend()));
            this.contractor.setNumFacesToSend(0);
            System.out.println(this.myAgent.getLocalName() + " negotiation ended with " 
                    + defBid.getSender().getLocalName() + " with contract " + defBid.getContent() 
                    + ": faces = " + this.contractor.getNumFacesToSend());
            //Fill ACLMessage with faces in order to send them to Responder
            this.setMessageToSend(toSend,this.contractor.getNumFacesToSend());
        }
        // defBid is less than supply
        else
        {
            toSend.setPerformative(ACLMessage.DEFINITIVE_ACCEPT);
            toSend.setContent(Integer.toString(bid));
            this.contractor.setNumFacesToSend(this.contractor.getNumFacesToSend() - bid);
            System.out.println(this.myAgent.getLocalName() + " negotiation ended with " 
                    + defBid.getSender().getLocalName() + " with contract " + defBid.getContent() 
                    + ": faces = " + this.contractor.getNumFacesToSend());
            //Fill ACLMessage with faces in order to send them to Responder
            this.setMessageToSend(toSend,bid);
        }
        return toSend;
    }
    
    protected ACLMessage handleOutOfSequence(ACLMessage outOfSequence)
    {
        System.out.println(this.myAgent.getLocalName() + ": handle outOfSequence with content " +
                outOfSequence.getContent());
        ACLMessage toSend = outOfSequence.createReply();
        toSend.setPerformative(ACLMessage.DEFINITIVE_REJECT);
        toSend.setContent("0");
        System.out.println(this.myAgent.getLocalName() + " negotiation ended with no contract with " 
            + outOfSequence.getSender().getLocalName() + ": faces = " + this.contractor.getNumFacesToSend());
        return toSend;
    }
    
    private void setMessageToSend(ACLMessage toSend, int index)
    {
        Contract contract = new Contract();
        int j = 0;
        ArrayList cuttedFacesToSend = new ArrayList();
        for(int i = 0; i < index; i++)
        {
            CuttedFace cutFace = (CuttedFace) this.contractor.getFacesToSend().get(i);
            cutFace.setSender(this.myAgent.getAID());
            cuttedFacesToSend.add(cutFace);
        }
        contract.setCuttedFaces(cuttedFacesToSend);
        contract.setDefAcceptContent(toSend.getContent());
        Iterator it = this.contractor.getFacesToSend().iterator();
        while(it.hasNext() && j < index)
        {
            it.next();
            it.remove();
            j++;
        }
        try 
        {
            toSend.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
            toSend.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
            this.myAgent.getContentManager().fillContent(toSend,contract);
        } catch (Codec.CodecException ex) {
            Logger.getLogger(ProtocolInitiator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(ProtocolInitiator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
