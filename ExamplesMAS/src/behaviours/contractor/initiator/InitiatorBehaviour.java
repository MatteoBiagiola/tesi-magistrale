/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor.initiator;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.states.MsgReceiver;
import jade.util.leap.HashMap;
import jade.util.leap.Iterator;
import jade.util.leap.Map;
import jade.util.leap.Set;
import ontology.Faces;

/**
 *
 * @author matteo
 */
public class InitiatorBehaviour extends FSMBehaviour{
    
    protected Map sessions = new HashMap();
    protected Map receiversStored = new HashMap();
    private static int cnt = 0;
    private MessageTemplate myTemplate;
    private static final int terminate = 1;
    private static final int notNull = 1;
    private String convId;
    
    //FSM states
    protected static final String SEND_INITIATIONS = "Send_initiatons";
    protected static final String SEND_MESSAGE = "Send_message";
    protected static final String RECEIVE_REPLY = "Receive_reply";
    protected static final String CHECK_SESSIONS = "Check_sessions";
    protected static final String DUMMY_FINAL = "Dummy_final";
    protected static final String HANDLE_PRE_BID = "Handle_pre_bid";
    protected static final String HANDLE_DEFINITIVE_BID = "Handle_definitive_bid";
    protected static final String HANDLE_OUT_OF_SEQUENCE = "Handle_out_of_sequence";
    
    public InitiatorBehaviour(Agent a,ACLMessage initiation)
    {
        super(a);
        
        DataStore ds = this.getDataStore();
        ds.put("INITIATION",initiation);
        
        this.convId = this.createConvId(initiation);
        this.myTemplate = MessageTemplate.MatchConversationId(convId);
        
        this.registerTransition(SEND_INITIATIONS,DUMMY_FINAL,terminate);
        this.registerDefaultTransition(SEND_INITIATIONS,RECEIVE_REPLY);
        this.registerDefaultTransition(RECEIVE_REPLY,CHECK_SESSIONS);
        this.registerTransition(CHECK_SESSIONS,HANDLE_PRE_BID,ACLMessage.PRE_BID);
        this.registerTransition(CHECK_SESSIONS,HANDLE_DEFINITIVE_BID,ACLMessage.DEFINITIVE_BID);
        this.registerDefaultTransition(CHECK_SESSIONS,HANDLE_OUT_OF_SEQUENCE);
        this.registerTransition(HANDLE_OUT_OF_SEQUENCE,SEND_MESSAGE,notNull);
        this.registerDefaultTransition(HANDLE_OUT_OF_SEQUENCE,RECEIVE_REPLY);
        this.registerDefaultTransition(HANDLE_PRE_BID,SEND_MESSAGE);
        this.registerDefaultTransition(HANDLE_DEFINITIVE_BID,SEND_MESSAGE);
        this.registerTransition(SEND_MESSAGE,DUMMY_FINAL,terminate);
        this.registerDefaultTransition(SEND_MESSAGE,RECEIVE_REPLY);
        
        Behaviour b = null;
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + SEND_INITIATIONS);
                ACLMessage initiation = (ACLMessage) this.getDataStore().get("INITIATION");
                sendInitiations(initiation);
            }
            
            @Override
            public int onEnd(){
                if(checkTermination())
                {
                    return terminate;
                }
                else return 0;
            }
        };
        b.setDataStore(ds);
        this.registerFirstState(b,SEND_INITIATIONS);
        
        b = new MsgReceiver(this.myAgent,this.myTemplate,MsgReceiver.INFINITE,ds,"RECEIVED"){
            @Override
            protected void handleMessage(ACLMessage msg)
            {
                System.out.println(this.myAgent.getLocalName() + ": state " + RECEIVE_REPLY + 
                        " from " + msg.getSender().getLocalName());
                System.out.println(this.myAgent.getLocalName() + ": current session is " 
                        + msg.getInReplyTo());
            }
        };
        b.setDataStore(ds);
        this.registerState(b,RECEIVE_REPLY);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + CHECK_SESSIONS);
                ACLMessage received = (ACLMessage) this.getDataStore().get("RECEIVED");
                String inReplyTo = received.getInReplyTo();
                if(!inReplyTo.isEmpty())
                {
                    Integer numSessionRemoved = Integer.parseInt(inReplyTo.substring(inReplyTo.length() - 1));
                    this.getDataStore().put("SESS_REMOVED",numSessionRemoved);
                    this.getDataStore().put("MSG_SENT",sessions.get(inReplyTo));
                    sessions.remove(inReplyTo);
                    printSession(inReplyTo,false);
                    printSessions();
                }
            }
            
            @Override
            public int onEnd()
            {
                ACLMessage received = (ACLMessage) this.getDataStore().get("RECEIVED");
                int perfToReturn = -1;
                switch(received.getPerformative()){
                    case ACLMessage.PRE_BID:
                        perfToReturn = ACLMessage.PRE_BID;
                        break;
                    case ACLMessage.DEFINITIVE_BID:
                        perfToReturn = ACLMessage.DEFINITIVE_BID;
                        break;
                    default:
                        break;
                }
                return perfToReturn;
            }
        };
        b.setDataStore(ds);
        this.registerState(b,CHECK_SESSIONS);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + HANDLE_PRE_BID);
                ACLMessage preBid = (ACLMessage) this.getDataStore().get("RECEIVED");
                ACLMessage msgSent = (ACLMessage) this.getDataStore().get("MSG_SENT");
                ACLMessage toSend = handlePreBid(preBid,msgSent);
                this.getDataStore().put("TO_SEND",toSend);
            }
            
        };
        b.setDataStore(ds);
        this.registerState(b,HANDLE_PRE_BID);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + HANDLE_DEFINITIVE_BID);
                ACLMessage definitiveBid = (ACLMessage) this.getDataStore().get("RECEIVED");
                ACLMessage toSend = handleDefinitiveBid(definitiveBid);
                this.getDataStore().put("TO_SEND",toSend);
            }
            
        };
        b.setDataStore(ds);
        this.registerState(b,HANDLE_DEFINITIVE_BID);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + HANDLE_OUT_OF_SEQUENCE);
                ACLMessage outOfSequence = (ACLMessage) this.getDataStore().get("RECEIVED");
                ACLMessage toSend = handleOutOfSequence(outOfSequence);
                this.getDataStore().put("TO_SEND",toSend);
            }
            
            @Override
            public int onEnd()
            {
                ACLMessage toSend = (ACLMessage) this.getDataStore().get("TO_SEND");
                if(toSend != null)
                {
                    return notNull;
                }
                return 0;
            }
            
        };
        b.setDataStore(ds);
        this.registerState(b,HANDLE_OUT_OF_SEQUENCE);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + SEND_MESSAGE);
                ACLMessage toSend = (ACLMessage) this.getDataStore().get("TO_SEND");
                Integer numSessRemoved = (Integer) this.getDataStore().get("SESS_REMOVED");
                if(numSessRemoved != null)
                {
                    String sessionKey = "R" + System.currentTimeMillis() + "_" + Integer.toString(numSessRemoved.intValue());
                    toSend.setReplyWith(sessionKey);
                    sessions.put(sessionKey,toSend);
                    printSession(sessionKey,true);
                    printSessions();
                    this.myAgent.send(toSend);
                    if(toSend.getPerformative() == ACLMessage.DEFINITIVE_ACCEPT ||
                            toSend.getPerformative() == ACLMessage.DEFINITIVE_REJECT)
                    {
                        deleteAgreedReceiver(toSend);
                        printReceivers();
                    }
                }
            }
            
            @Override
            public int onEnd(){
                if(checkTermination())
                {
                    return terminate;
                }
                else return 0;
            }
        };
        b.setDataStore(ds);
        this.registerState(b,SEND_MESSAGE);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + DUMMY_FINAL);
            }
        };
        b.setDataStore(ds);
        this.registerLastState(b,DUMMY_FINAL);
    }
    
    protected void sendInitiations(ACLMessage initiation)
    {
        int cnt = 0;
        if(initiation != null)
        {
            for(Iterator receivers = initiation.getAllReceiver(); receivers.hasNext();)
            {
                AID receiver = (AID) receivers.next();
                ACLMessage toSend = (ACLMessage) initiation.clone();
                toSend.clearAllReceiver();
                toSend.setConversationId(this.convId);
                System.out.println(this.myAgent.getLocalName() + ": convId " + toSend.getConversationId());
                toSend.addReceiver(receiver);
                String sessionKey = "R" + System.currentTimeMillis() + "_" + Integer.toString(cnt);
                this.printSession(sessionKey,true);
                toSend.setReplyWith(sessionKey);
                sessions.put(sessionKey,toSend);
                receiversStored.put("RECEIVER_" + Integer.toString(cnt),toSend);
                cnt++;
                toSend.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                this.myAgent.send(toSend);
            }
            this.printSessions();
        }
    }
    
    protected String createConvId(ACLMessage msg)
    {
        String convId = null;
        if ((msg == null) || (msg.getConversationId() == null)) 
        {
            convId = "C" + this.hashCode() + "_" + 
                    this.myAgent.getLocalName() + "_" + 
                    System.currentTimeMillis() + "_" + getCnt();
	}
	else 
        {
            convId = msg.getConversationId();
	}
        return convId;
    }
    
    private synchronized static int getCnt() {
	int k = cnt;
	cnt++;
	return k;
    }
    
    protected ACLMessage handlePreBid(ACLMessage preBid, ACLMessage msgSent)
    {
        return null;
    }
    
    protected ACLMessage handleDefinitiveBid(ACLMessage definitiveBid)
    {
        return null;
    }
    
    protected ACLMessage handleOutOfSequence(ACLMessage outOfSequence)
    {
        return null;
    }
    
    private void printSession(String sessionKey, boolean open)
    {
        if(open)
        {
            System.out.println(this.myAgent.getLocalName() + ": new session opened with key -> " 
                    + sessionKey);
        }
        else
        {
            System.out.println(this.myAgent.getLocalName() + ": session closed with key -> " 
                    + sessionKey);
        }
    }
    
    private void printSessions()
    {
        Set keySet = sessions.keySet();
        System.out.println();
        System.out.println("Current sessions opened are ");
        for(Iterator it = keySet.iterator(); it.hasNext();)
        {
            String currentKey = (String) it.next();
            System.out.print("key: " + currentKey);
            ACLMessage msg = (ACLMessage) sessions.get(currentKey);
            System.out.println(" value: " + ACLMessage.getPerformative(msg.getPerformative()));
        }
        System.out.println();
    }
    
    private void printReceivers()
    {
        Set keySet = receiversStored.keySet();
        System.out.println();
        System.out.println("Receivers remained are ");
        for(Iterator it = keySet.iterator(); it.hasNext();)
        {
            String currentKey = (String) it.next();
            System.out.print("key: " + currentKey);
            ACLMessage storedMessage = (ACLMessage) receiversStored.get(currentKey);
            AID receiver = (AID) storedMessage.getAllReceiver().next();
            System.out.println(" value: " + receiver.getLocalName());
        }
        System.out.println();
    }
    
    boolean checkTermination()
    {
        Set keySet = sessions.keySet();
        for(Iterator it = keySet.iterator(); it.hasNext();)
        {
            String currentKey = (String) it.next();
            ACLMessage msg = (ACLMessage) sessions.get(currentKey);
            if(!ACLMessage.getPerformative(msg.getPerformative()).contains("DEF"))
            {
                return false;
            }
        }
        return true;
    }
    
    protected void deleteAgreedReceiver(ACLMessage toSend)
    {
        AID targetReceiver = (AID) toSend.getAllReceiver().next();
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%targetReceiver " + targetReceiver.getLocalName());
        Set keySet = receiversStored.keySet();
        for(Iterator it = keySet.iterator(); it.hasNext();)
        {
            String currentKey = (String) it.next();
            ACLMessage storedMessage = (ACLMessage) receiversStored.get(currentKey);
            AID receiver = (AID) storedMessage.getAllReceiver().next();
            if(receiver.getLocalName().equals(targetReceiver.getLocalName()))
            {
                System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Removed " + receiver.getLocalName() + " receiver");
                receiversStored.remove(currentKey);
                return;
            }
        }
    }
    
    public void deleteAllInitialReceivers()
    {
        Set keySet = receiversStored.keySet();
        ACLMessage toSend = new ACLMessage(ACLMessage.DEFINITIVE_REJECT);
        toSend.setContent(Integer.toString(-1));
        for(Iterator it = keySet.iterator(); it.hasNext();)
        {
            String currentKey = (String) it.next();
            ACLMessage storedMessage = (ACLMessage) receiversStored.get(currentKey);
            AID receiver = (AID) storedMessage.getAllReceiver().next();
            System.out.println("@@@@@@@@@@@@@@@@@@Removed " + receiver.getLocalName() + " receiver");
            toSend.setConversationId(storedMessage.getConversationId());
            System.out.println("@@@@@@@@@@@@@@@@@@ConvId " + toSend.getConversationId());
            toSend.addReceiver(receiver);
            System.out.println("@@@@@@@@@@@@@@@@@@DefReject sent to " + receiver.getLocalName() + " receiver");
            toSend.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
            this.myAgent.send(toSend);
        }
        receiversStored.clear();
    }
}
