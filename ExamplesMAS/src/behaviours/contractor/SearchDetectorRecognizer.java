/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor;

import behaviours.df.CreateMultipleSearchService;
import jade.core.AID;
import jade.core.Agent;
import jade.util.leap.ArrayList;
import jade.util.leap.Map;

/**
 *
 * @author matteo
 */
public class SearchDetectorRecognizer extends CreateMultipleSearchService{

    private long waitBeforeRequery = 0;
    private int policy;
    
    public SearchDetectorRecognizer(Agent a, int policy, long waitBeforeRequery, 
            ArrayList serviceTypes) {
        super(a,policy,waitBeforeRequery,serviceTypes);
        this.policy = policy;
        this.waitBeforeRequery = waitBeforeRequery;
    }
    
    public SearchDetectorRecognizer(Agent a, int policy, long waitBeforeRequery, 
            ArrayList serviceTypes, Map servicesMap) {
        super(a,policy,waitBeforeRequery,serviceTypes,servicesMap);
        this.policy = policy;
        this.waitBeforeRequery = waitBeforeRequery;
    }

    @Override
    public int handleOnEnd(Map servicesMap,ArrayList serviceTypes)
    {
        ArrayList agentsAID = new ArrayList();
        for(int i = 0; i < serviceTypes.size(); i++)
        {
            if(servicesMap.containsKey(serviceTypes.get(i)))
            {
                agentsAID.add(servicesMap.get(serviceTypes.get(i)));
            }
        }
        if(servicesMap.size() == serviceTypes.size() && serviceTypes.size() != 0)
        {
            System.out.println(this.myAgent.getLocalName() + ": all services found");
            AID[] detectors = (AID[]) agentsAID.get(0);
            AID[] recognizers = (AID[]) agentsAID.get(1);
            System.out.println(this.myAgent.getLocalName() + ": AID 1 " + detectors[0].getLocalName()
                    + " AID 2 " + recognizers[0].getLocalName());
            this.myAgent.addBehaviour(new ReceiveMessages(this.myAgent,detectors[0],
                        recognizers[0]));
        }
        else
        {
            System.out.println(this.myAgent.getLocalName() + ": requery services not found " 
                    + " wait before requery " + this.waitBeforeRequery);
            return -1;
        }
        return 0;
    }
    
    @Override
    protected void handleRequery(Map servicesMap,ArrayList serviceTypes)
    {
        this.myAgent.addBehaviour(new SearchDetectorRecognizer(this.myAgent,policy,
                                    waitBeforeRequery,serviceTypes,servicesMap));
    }
}
