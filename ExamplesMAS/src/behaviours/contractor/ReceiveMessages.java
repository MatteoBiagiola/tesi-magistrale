/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.contractor;

import agents.Contractor;
import behaviours.others.SendPropagateMessage;
import behaviours.contractor.initiator.PrepareInitiations;
import behaviours.contractor.responder.CreateParallelResponders;
import behaviours.contractor.responder.CreateResponders;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;
import jade.util.leap.HashMap;
import jade.util.leap.Iterator;
import jade.util.leap.Map;
import jade.util.leap.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Contract;
import ontology.CuttedFace;
import ontology.Faces;
import utilities.CreateMessageForDF;
import utilities.RecognitionTime;

/**
 *
 * @author matteo
 */
public class ReceiveMessages extends CyclicBehaviour{

    private Map announcesMap = new HashMap();
    private ArrayList announcesArray = new ArrayList();
    private MessageTemplate myTemplate;
    private Contractor contractor = null;
    private AID detectorAID;
    private AID recognizerAID;

    public ReceiveMessages(Agent a, AID detectorAID, AID recognizerAID)
    {
        super(a);
        this.contractor = (Contractor) a;
        this.myTemplate = this.createMessageTemplate();
        this.detectorAID = detectorAID;
        this.recognizerAID = recognizerAID;
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(this.myTemplate);
        if(msg != null)
        {
            if(msg.getPerformative() == ACLMessage.INFORM)
            {
                /*message coming from local Recognizer to say that local extraction-detection
                phase is ended; negotiation can starts.*/
                try 
                {
                    Contract contract = (Contract) this.myAgent.getContentManager().extractContent(msg);
                    if(contract.getNumFacesToSend() > 0)
                    {
                        //INITIATOR
                        this.contractor.setiAmResponder(false);
                        this.announcesMap.clear();
                        this.contractor.setTempNumFacesToSend(contract.getNumFacesToSend());
                        this.contractor.setNumFacesToSend(contract.getNumFacesToSend());
                        System.out.println(this.myAgent.getLocalName() + ": num faces to send " 
                                + this.contractor.getNumFacesToSend());
                        for(int i = 0; i < contract.getCuttedFaces().size(); i++)
                        {
                            this.contractor.getFacesToSend().add(contract.getCuttedFaces().get(i));
                        }
                        ACLMessage toDF = CreateMessageForDF.createSearchMessage(this.myAgent,"Remote-recognition");
                        this.myAgent.addBehaviour(new PrepareInitiations(this.myAgent,1000,toDF));
                        this.myAgent.addBehaviour(new ReceiveCancelMessage(this.myAgent,
                                this.detectorAID,this.recognizerAID));
                        this.myAgent.removeBehaviour(this);
                    }
                    else
                    {
                        //RESPONDER
                        this.contractor.setiAmResponder(true);
                        this.contractor.setNumFacesToPropose(contract.getNumFacesToPropose());
                        this.MapToArray();
                        System.out.println(this.myAgent.getLocalName() + ": num faces to propose " 
                                + this.contractor.getNumFacesToPropose());
                        System.out.println(this.myAgent.getLocalName() + ": adding create responders behaviour");
                        this.myAgent.addBehaviour(new CreateResponders(this.myAgent,this.announcesArray));
                        this.announcesArray.clear();
                        System.out.println(this.myAgent.getLocalName() + ": adding receive cancel message behaviour");
                        this.myAgent.addBehaviour(new ReceiveCancelMessage(this.myAgent,this.detectorAID,
                                this.recognizerAID));
                        this.myAgent.removeBehaviour(this);
                    }
                } catch (Codec.CodecException ex) {
                    Logger.getLogger(ReceiveMessages.class.getName()).log(Level.SEVERE, null, ex);
                } catch (OntologyException ex) {
                    Logger.getLogger(ReceiveMessages.class.getName()).log(Level.SEVERE, null, ex);
                }  
            }
            else if(msg.getPerformative() == ACLMessage.PROXY)
            {
                System.out.println(this.myAgent.getLocalName() + ": proxy message received");
                //message coming from Detector; no detected faces in the current frame
                if(this.announcesMap.isEmpty())
                {
                    System.out.println(this.myAgent.getLocalName() + ": trigger a new extraction-detection phase");
                    //no announces -> a new extraction-detection phase has to be triggered
                    this.myAgent.addBehaviour(new SendPropagateMessage(this.myAgent,msg.getSender()));
                }
                else
                {
                    System.out.println(this.myAgent.getLocalName() + ": handle current ANNOUNCES");
                    this.MapToArray();
                    int initiatorDetails[] = {-1};
                    int responderDetails[] = {-1};
                    RecognitionTime.computeContractDetails(0,Faces.timeForRecognition,
                            Faces.classNum,Faces.imgNumPerClass,initiatorDetails,responderDetails);
                    this.contractor.setNumFacesToPropose(responderDetails[0]);
                    this.myAgent.addBehaviour(new CreateParallelResponders(this.myAgent,ParallelBehaviour.WHEN_ALL,
                            this.announcesArray,this.detectorAID,this.recognizerAID,responderDetails[0]));
                    this.myAgent.removeBehaviour(this);
                }
            }
            else if(msg.getPerformative() == ACLMessage.ANNOUNCE)
            {
                //announces coming from remote Contractor agents
                System.out.println(this.myAgent.getLocalName() + ": announce message received");
                this.announcesMap.put(msg.getConversationId(),msg);
            }
            else if(msg.getPerformative() == ACLMessage.DEFINITIVE_REJECT)
            {
                //def-reject coming from remote Contractor agents
                System.out.println(this.myAgent.getLocalName() + ": def-reject message received");
                this.announcesMap.remove(msg.getConversationId());
            }
            else if(msg.getPerformative() == ACLMessage.INFORM_REF)
            {
                //recognition results coming from remote Recognizer agents; write results in a file.
                System.out.println("+++++++++++++++++ : recognition results received from " + msg.getSender().getLocalName());
                System.out.println(this.myAgent.getLocalName() + ": recognition results received");
                try 
                {
                    Contract contract = (Contract) this.myAgent.getContentManager().extractContent(msg);
                    ArrayList cuttedFaces = contract.getCuttedFaces();
                    Faces.setRemote_recognized(Faces.getRemote_recognized() + cuttedFaces.size());
                    for(int i = 0; i < cuttedFaces.size(); i++)
                    {
                        CuttedFace cutFace = (CuttedFace) cuttedFaces.get(i);
                        this.contractor.getWriter().println("Frame number: " + cutFace.getFrameNum()
                                + " Face number: " + cutFace.getFaceNum()
                                + " Label: " + cutFace.getPredictlabel()
                                + " Confidence: " + cutFace.getConfidence());
                        this.contractor.getWriter().flush();
                    }
                } catch (Codec.CodecException ex) {
                    Logger.getLogger(ReceiveCancelMessage.class.getName()).log(Level.SEVERE, null, ex);
                } catch (OntologyException ex) {
                    Logger.getLogger(ReceiveCancelMessage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else
        {
            this.block();
        }
    }
    
    private MessageTemplate createMessageTemplate()
    {
        MessageTemplate tpl1 = MessageTemplate.or(MessageTemplate.MatchPerformative(ACLMessage.INFORM_REF)
                ,MessageTemplate.MatchPerformative(ACLMessage.ANNOUNCE));
        MessageTemplate tpl2 = MessageTemplate.or(MessageTemplate.MatchPerformative(ACLMessage.REQUEST),tpl1);
        MessageTemplate tpl3 = MessageTemplate.or(MessageTemplate.MatchPerformative(ACLMessage.PROXY),tpl2);
        MessageTemplate tpl4 = MessageTemplate.or(MessageTemplate.MatchPerformative(ACLMessage.DEFINITIVE_REJECT),tpl3);
        MessageTemplate tpl5 = MessageTemplate.or(MessageTemplate.MatchPerformative(ACLMessage.INFORM),tpl4);
        return MessageTemplate.and(MessageTemplate.MatchOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName())
                ,tpl5);
    }
    
    private void MapToArray()
    {
        Set keySet = this.announcesMap.keySet();
        for(Iterator it = keySet.iterator(); it.hasNext();)
        {
            String currentKey = (String) it.next();
            this.announcesArray.add(this.announcesMap.get(currentKey));
        }
        this.announcesMap.clear();
    }
}
