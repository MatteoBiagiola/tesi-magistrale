/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.others;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import ontology.Faces;

/**
 *
 * @author matteo
 */
public class SendPropagateMessage extends OneShotBehaviour{

    private AID agentAID;
    
    public SendPropagateMessage(Agent a, AID agentAID)
    {
        super(a);
        this.agentAID = agentAID;
    }
    
    @Override
    public void action() {
        ACLMessage msg = new ACLMessage(ACLMessage.PROPAGATE);
        msg.addReceiver(this.agentAID);
        msg.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
        this.myAgent.send(msg);
    }
    
}
