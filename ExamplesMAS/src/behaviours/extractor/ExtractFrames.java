/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.extractor;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Detect;
import ontology.Faces;
import ontology.Frame;
import ontology.Slot;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

/**
 *
 * @author matteo
 */
public class ExtractFrames extends CyclicBehaviour{

    private VideoCapture vd;
    private double timestamp = 0.0;
    private long previousTime = 0;
    private long currentTime = 0;
    private boolean extract = false;
    private long time = 0;
    private String containerNumber = "";
    private MessageTemplate myTemplate;
    
    public ExtractFrames(Agent a, VideoCapture vd)
    {
        super(a);
        this.vd = vd;
        this.containerNumber = this.myAgent.getLocalName().substring(this.myAgent.getLocalName().length()-1);
        try
        {
            Integer.parseInt(this.containerNumber);
        }catch(NumberFormatException ex){
            this.containerNumber = "0";
        }
        this.myTemplate = MessageTemplate.MatchOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(this.myTemplate);
        if(msg != null)
        {
            if(msg.getPerformative() == ACLMessage.REQUEST)
            {
                try 
                {
                    Slot slot = (Slot) this.myAgent.getContentManager().extractContent(msg);
                    this.currentTime = slot.getStart();
                    
                    this.extract = false;
                    Mat frame =  new Mat();
                    if(this.previousTime != 0) 
                    {
                        this.timestamp = this.vd.get(Videoio.CAP_PROP_POS_MSEC);
                        this.vd.set(Videoio.CAP_PROP_POS_MSEC,this.timestamp +
                                (this.currentTime - this.previousTime));
                    }
                    this.time = System.currentTimeMillis();
                    this.extract = this.vd.read(frame);
                    System.out.println(this.myAgent.getLocalName() + ": ^^^^^^^^^^Extraction time: " + (System.currentTimeMillis() - this.time) 
                            + " [ms]");
                    if(this.extract)
                    {
                        this.previousTime = this.currentTime;
                        Mat gray = new Mat();
                        Imgproc.cvtColor(this.shrinkFrame(frame,320),gray,Imgproc.COLOR_BGR2GRAY);
                        MatOfByte byteimage = new MatOfByte();
                        Imgcodecs.imencode(".pgm",gray,byteimage);
                        Imgcodecs.imwrite(String.format("resources/output/frames/" + this.containerNumber + "_frame%05d.pgm",
                                (int) this.vd.get(Videoio.CAP_PROP_POS_FRAMES)),gray);
                        
                        Faces.setFrameTotalBytes(Faces.getFrameTotalBytes() + byteimage.toArray().length);
                        
                        Frame frameClass = new Frame();
                        frameClass.setFrameNumber((int) this.vd.get(Videoio.CAP_PROP_POS_FRAMES));
                        frameClass.setFrameImage(byteimage.toArray());
                        
                        ACLMessage toDetector = msg.createReply();
                        toDetector.setPerformative(ACLMessage.INFORM);
                        toDetector.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
                        toDetector.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                        
                        Detect detect = new Detect();
                        detect.setFrame(frameClass);
                        
                        this.myAgent.getContentManager().fillContent(toDetector, detect);
                        this.myAgent.send(toDetector);
                    }
                    else
                    {
                        this.vd.release();
                        System.out.println(this.myAgent.getLocalName() + ": video ended, send DISCONFIRM message to " + msg.getSender().getLocalName());
                        System.out.println(this.myAgent.getLocalName() + ": waiting for any messages coming from remote...(1 minute)");
                        this.myAgent.addBehaviour(new WakerBehaviour(this.myAgent,60000) 
                        {
                            @Override
                            public void onWake()
                            {
                                System.out.println(this.myAgent.getLocalName() + ": writing results");
                                ACLMessage finalMessage = msg.createReply();
                                finalMessage.setPerformative(ACLMessage.DISCONFIRM);
                                finalMessage.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                                this.myAgent.send(finalMessage);
                            }
                        });
                    }
                } catch (Codec.CodecException ex) {
                    Logger.getLogger(ExtractFrames.class.getName()).log(Level.SEVERE, null, ex);
                } catch (OntologyException ex) {
                    Logger.getLogger(ExtractFrames.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        else
        {
            this.block();
        }
    }
    
    private Mat shrinkFrame(Mat image, int detection_width)
    {
        Mat smallImage = new Mat();
        int scaledHeight = 0;
        Size rescale;
        float scale = image.cols()/(float) detection_width;
        System.out.println("Frame cols: " + image.cols() + " Frame rows: " + image.rows());
        if(image.cols() > detection_width)
        {
            scaledHeight = Math.round(image.rows()/scale);
            rescale = new Size(detection_width,scaledHeight);
            Imgproc.resize(image,smallImage,rescale);
        }
        else
        {
            smallImage = image;
        }
        System.out.println("Shrinked-frame cols: " + smallImage.cols() + " Shrinked-frame rows: " 
                + smallImage.rows());
        return smallImage;
    }
    
    
}
