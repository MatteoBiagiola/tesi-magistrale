/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.detector;

import behaviours.df.CreateMultipleSearchService;
import jade.core.AID;
import jade.core.Agent;
import jade.util.leap.ArrayList;
import jade.util.leap.Map;

/**
 *
 * @author matteo
 */
public class SearchLocalAgents extends CreateMultipleSearchService{

    private long waitBeforeRequery = 0;
    private int policy;
    
    public SearchLocalAgents(Agent a, int policy, long waitBeforeRequery, 
            ArrayList serviceTypes) {
        super(a, policy, waitBeforeRequery, serviceTypes);
        this.policy = policy;
        this.waitBeforeRequery = waitBeforeRequery;
    }

    public SearchLocalAgents(Agent a, int policy, long waitBeforeRequery, 
            ArrayList serviceTypes, Map servicesMap) {
        super(a, policy, waitBeforeRequery, serviceTypes, servicesMap);
        this.policy = policy;
        this.waitBeforeRequery = waitBeforeRequery;
    }
    
    @Override
    public int handleOnEnd(Map servicesMap,ArrayList serviceTypes)
    {
        ArrayList agentsAID = new ArrayList();
        for(int i = 0; i < serviceTypes.size(); i++)
        {
            if(servicesMap.containsKey(serviceTypes.get(i)))
            {
                agentsAID.add(servicesMap.get(serviceTypes.get(i)));
            }
        }
        if(servicesMap.size() == serviceTypes.size() && serviceTypes.size() != 0)
        {
            System.out.println(this.myAgent.getLocalName() + ": all services found");
            AID[] extractors = (AID[]) agentsAID.get(0);
            AID[] recognizers = (AID[]) agentsAID.get(1);
            AID[] contractors = (AID[]) agentsAID.get(2);
            System.out.println(this.myAgent.getLocalName() + ": AID 1 " + extractors[0].getLocalName()
                    + " AID 2 " + recognizers[0].getLocalName() + " AID 3 " + contractors[0].getLocalName());
            this.myAgent.addBehaviour(new DetectFaces(this.myAgent,extractors[0],
                        recognizers[0],contractors[0]));
            this.myAgent.addBehaviour(new SendFrameRequest(this.myAgent,extractors[0]));
        }
        else
        {
            System.out.println(this.myAgent.getLocalName() + ": requery services not found " 
                    + " wait before requery " + this.waitBeforeRequery);
            return -1;
        }
        return 0;
    }
    
    @Override
    protected void handleRequery(Map servicesMap,ArrayList serviceTypes)
    {
        this.myAgent.addBehaviour(new SearchLocalAgents(this.myAgent,policy,
                                    waitBeforeRequery,serviceTypes,servicesMap));
    }
    
}
