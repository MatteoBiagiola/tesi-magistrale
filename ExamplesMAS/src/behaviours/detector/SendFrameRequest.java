/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.detector;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Faces;
import ontology.Slot;

/**
 *
 * @author matteo
 */
public class SendFrameRequest extends OneShotBehaviour{

    private AID extractorAID;
    
    public SendFrameRequest(Agent a, AID extractorAID)
    {
        super(a);
        this.extractorAID = extractorAID;
    }
    
    @Override
    public void action() {
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        
        msg.addReceiver(this.extractorAID);
        msg.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
        msg.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
        
        Slot slot = new Slot();
        slot.setStart(System.currentTimeMillis());
        
        try 
        {
            this.myAgent.getContentManager().fillContent(msg, slot);
            this.myAgent.send(msg);
        } catch (Codec.CodecException ex) {
            Logger.getLogger(SendFrameRequest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(SendFrameRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
