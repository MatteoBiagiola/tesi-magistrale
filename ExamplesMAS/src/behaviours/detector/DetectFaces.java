/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.detector;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.CuttedFace;
import ontology.Detect;
import ontology.Faces;
import ontology.Frame;
import ontology.Recognize;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class DetectFaces extends CyclicBehaviour{

    private AID recognizerAID;
    private AID extractorAID;
    private AID contractorAID;
    private CascadeClassifier faceDetector;
    private String LbpClassifier;
    private String HaarClassifier;
    private Mat grayEq = new Mat();
    private MatOfRect faceDetections = new MatOfRect();
    private int flags;
    private Size minFeaturesSize;
    private Size maxFeaturesSize;
    private float scaleFactor;
    private int minNeighbors;
    private int numfaces;
    private long time = 0;
    private String containerNumber = "";
    private MessageTemplate myTemplate;
    
    public DetectFaces(Agent a, AID extractorAID, AID recognizerAID, AID contractorAID)
    {
        super(a);
        this.extractorAID = extractorAID;
        this.recognizerAID = recognizerAID;
        this.contractorAID = contractorAID;
        this.numfaces = 0;
        this.flags = Objdetect.CASCADE_SCALE_IMAGE;
        this.minFeaturesSize = new Size(20,20);
        this.maxFeaturesSize = new Size();
        this.scaleFactor = (float) 1.2;
        this.minNeighbors = 3;
        this.LbpClassifier = "resources/input/FaceDetection/lbpcascade_frontalface.xml";
        this.HaarClassifier = "resources/input/FaceDetection/haarcascade_frontalface_default.xml";
        this.faceDetector = new CascadeClassifier(this.LbpClassifier);
        this.containerNumber = this.myAgent.getLocalName().substring(this.myAgent.getLocalName().length()-1);
        try
        {
            Integer.parseInt(this.containerNumber);
        }catch(NumberFormatException ex){
            this.containerNumber = "0";
        }
        this.myTemplate = MessageTemplate.MatchOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(this.myTemplate);
        if(msg != null)
        {
            if(msg.getPerformative() == ACLMessage.INFORM)
            {
                //message coming from Extractor; this is the current frame where detecting faces
                try 
                {
                    Detect detect = (Detect) this.myAgent.getContentManager().extractContent(msg);
                    Frame frame = detect.getFrame();
                    if(frame != null)
                    {
                        MatOfByte byteframe = new MatOfByte();
                        byteframe.fromArray(frame.getFrameImage());
                        Mat image = Imgcodecs.imdecode(byteframe, Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                        Imgproc.equalizeHist(image,this.grayEq);
                        this.time = System.currentTimeMillis();
                        Thread.sleep(1000);
                        this.faceDetector.detectMultiScale(this.grayEq,
                            this.faceDetections,this.scaleFactor, 
                            this.minNeighbors,this.flags,this.minFeaturesSize, 
                            this.maxFeaturesSize);
                        System.out.println("Detection time: " + (System.currentTimeMillis() - this.time) 
                                + " [ms]");
                        ArrayList cuttedFaces = new ArrayList();
                        Recognize recognize = new Recognize();
                        this.numfaces = 0;
                        for(Rect rect: this.faceDetections.toArray())
                        {
                            Rect r = new Rect(rect.x,rect.y,rect.width,rect.height);
                            Mat croppedFace = new Mat(image,r);
                            MatOfByte byteimage = new MatOfByte();
                            Imgcodecs.imencode(".pgm",croppedFace,byteimage);
                            CuttedFace cutFace = new CuttedFace();
                            cutFace.setSize(96);
                            cutFace.setCutface(byteimage.toArray());
                            cutFace.setFrameNum(frame.getFrameNumber());
                            cutFace.setFaceNum(this.numfaces);
                            cuttedFaces.add(cutFace);
                            String faceName = String.format("resources/output/faces/" + this.containerNumber + "_frame%05d",frame.getFrameNumber()) +
                                    String.format("-%02d.pgm",this.numfaces);
                            Imgcodecs.imwrite(faceName,croppedFace);
                            this.numfaces = this.numfaces + 1;
                        }
                        recognize.setCuttedFaces(cuttedFaces);
                        //if there are detected faces in the current frame then send these faces to Recognizer
                        if(recognize.getCuttedFaces().size() > 0)
                        {
                            ACLMessage toRecognizer = new ACLMessage(ACLMessage.REQUEST);
                            toRecognizer.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
                            toRecognizer.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                            toRecognizer.addReceiver(this.recognizerAID);
                            this.myAgent.getContentManager().fillContent(toRecognizer,recognize);
                            this.myAgent.send(toRecognizer);
                        }
                        //else don't send request to Recognizer but send a proxy message to Contractor.
                        else
                        {
                            ACLMessage toContractor = new ACLMessage(ACLMessage.PROXY);
                            toContractor.addReceiver(this.contractorAID);
                            toContractor.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                            this.myAgent.send(toContractor);
                        }
                    }
                } catch (Codec.CodecException ex) {
                    Logger.getLogger(DetectFaces.class.getName()).log(Level.SEVERE, null, ex);
                } catch (OntologyException ex) {
                    Logger.getLogger(DetectFaces.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(DetectFaces.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if(msg.getPerformative() == ACLMessage.PROPAGATE)
            {
                //message coming from Recognizer; the action to do is to send a new frame request to Extractor
                this.myAgent.addBehaviour(new SendFrameRequest(this.myAgent,this.extractorAID));
            }
            else if(msg.getPerformative() == ACLMessage.DISCONFIRM)
            {
                ACLMessage finalMessage = new ACLMessage(ACLMessage.DISCONFIRM);
                finalMessage.addReceiver(this.recognizerAID);
                finalMessage.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                this.myAgent.send(finalMessage);
            }
        }
        else
        {
            this.block();
        }
    }
    
}
