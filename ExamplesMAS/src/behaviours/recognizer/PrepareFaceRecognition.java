/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.recognizer;

import agents.Recognizer;
import behaviours.others.SendPropagateMessage;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Contract;
import ontology.Faces;
import ontology.Recognize;
import utilities.RecognitionTime;
import utilities.WriteNetworkInfo;

/**
 *
 * @author matteo
 */
public class PrepareFaceRecognition extends CyclicBehaviour{

    private Recognizer recognizer;
    private AID contractorAID;
    private AID detectorAID;
    private boolean isResponder = false;
    private boolean areFacesSend = false;
    private boolean isPropagate = true;
    private MessageTemplate myTemplate;
    
    public PrepareFaceRecognition(Agent a, AID detectorAID, AID contractorAID)
    {
        super(a);
        this.recognizer = (Recognizer) a;
        this.detectorAID = detectorAID;
        this.contractorAID = contractorAID;
        this.myTemplate = MessageTemplate.MatchOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(this.myTemplate);
        if(msg != null)
        {
            if(msg.getPerformative() == ACLMessage.REQUEST)
            {
                //message coming from Detector: action is to recognize faces detected in the current frame (>= 1)
                try 
                {
                    Recognize recognize = (Recognize) this.myAgent.getContentManager().extractContent(msg);
                    Contract contract = new Contract();
                    ACLMessage toContractor = new ACLMessage(ACLMessage.INFORM);
                    toContractor.addReceiver(this.contractorAID);
                    int detected = recognize.getCuttedFaces().size();
                    Faces.setDetected(Faces.getDetected() + detected);
                    int[] initiatorDetails = {-1};
                    int[] responderDetails = {-1};
                    RecognitionTime.computeContractDetails(detected,Faces.timeForRecognition,
                            Faces.classNum,Faces.imgNumPerClass,initiatorDetails,responderDetails);
                    contract.setNumFacesToSend(initiatorDetails[0]);
                    contract.setNumFacesToPropose(responderDetails[0]);
                    ArrayList facesToKeep = new ArrayList();
                    System.out.println(this.myAgent.getLocalName() + ": faces detected " + recognize.getCuttedFaces().size());
                    if(initiatorDetails[0] > 0)
                    {
                        //INITIATOR
                        this.isResponder = false;
                        ArrayList facesToSend = new ArrayList();
                        //send the first "initiatorDetails[0] faces"
                        for(int i = 0; i < initiatorDetails[0]; i++)
                        {
                            facesToSend.add(recognize.getCuttedFaces().get(i));
                        }
                        System.out.println(this.myAgent.getLocalName() + ": facesToSend size " + facesToSend.size());
                        //keep the difference
                        for(int i = initiatorDetails[0]; i < recognize.getCuttedFaces().size(); i++)
                        {
                            facesToKeep.add(recognize.getCuttedFaces().get(i));
                        }
                        recognize.getCuttedFaces().clear();
                        System.out.println(this.myAgent.getLocalName() + ": facesToKeep size " + facesToKeep.size());
                        contract.setCuttedFaces(facesToSend);
                    }
                    else
                    {
                        //RESPONDER: keep all detected faces
                        this.isResponder = true;
                        contract.setCuttedFaces(new ArrayList());
                        for(int i = 0; i < recognize.getCuttedFaces().size(); i++)
                        {
                            facesToKeep.add(recognize.getCuttedFaces().get(i));
                        }
                        System.out.println(this.myAgent.getLocalName() + ": facesToKeep size " + facesToKeep.size());
                    }
                    
                    toContractor.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
                    toContractor.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                    this.myAgent.getContentManager().fillContent(toContractor,contract);
                    this.myAgent.send(toContractor);
                    
                    this.areFacesSend = false;
                    Faces.setRecognized(Faces.getRecognized() + facesToKeep.size());
                    this.myAgent.addBehaviour(new RecognizeFaces(this.myAgent,facesToKeep,
                            this.contractorAID,msg.getSender(),this.areFacesSend,this.isResponder));
                    
                } catch (Codec.CodecException ex) {
                    Logger.getLogger(PrepareFaceRecognition.class.getName()).log(Level.SEVERE, null, ex);
                } catch (OntologyException ex) {
                    Logger.getLogger(PrepareFaceRecognition.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if(msg.getPerformative() == ACLMessage.INFORM)
            {
                //message coming from Contractor in order to recognize faces received from remote Contractor (>=1)
                try 
                {
                    Contract contract = (Contract) this.myAgent.getContentManager().extractContent(msg);
                    this.areFacesSend = true;
                    if(msg.getConversationId().equals("afd")){//any face detected
                        this.isPropagate = false;
                    }
                    else{//at least one face detected
                        this.isPropagate = true;
                    }
                    this.myAgent.addBehaviour(new RecognizeFaces(this.myAgent,contract.getCuttedFaces(),
                            this.contractorAID,this.detectorAID,this.areFacesSend,this.isResponder,
                            this.isPropagate));
                } catch (Codec.CodecException ex) {
                    Logger.getLogger(PrepareFaceRecognition.class.getName()).log(Level.SEVERE, null, ex);
                } catch (OntologyException ex) {
                    Logger.getLogger(PrepareFaceRecognition.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else if(msg.getPerformative() == ACLMessage.PROPAGATE)
            {
                this.myAgent.addBehaviour(new SendPropagateMessage(this.myAgent,this.detectorAID));
                
            }
            else if(msg.getPerformative() == ACLMessage.DISCONFIRM)
            {
                WriteNetworkInfo.writeBytesExchanged(this.recognizer);
            }
        }
        else
        {
            this.block();
        }
    }
    
}
