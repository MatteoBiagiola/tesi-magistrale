/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.recognizer;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import ontology.Faces;

/**
 *
 * @author matteo
 */
public class SendCancelMessage extends OneShotBehaviour{

    private AID contractorAID;
    
    public SendCancelMessage(Agent a, AID contractorAID)
    {
        this.contractorAID = contractorAID;
    }
    
    @Override
    public void action() {
        ACLMessage msg = new ACLMessage(ACLMessage.CANCEL);
        msg.addReceiver(this.contractorAID);
        msg.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
        this.myAgent.send(msg);
    }
    
}
