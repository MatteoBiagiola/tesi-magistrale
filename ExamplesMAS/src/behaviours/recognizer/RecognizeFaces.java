/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.recognizer;

import behaviours.others.SendPropagateMessage;
import agents.Recognizer;
import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Contract;
import ontology.CuttedFace;
import ontology.Faces;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author matteo
 */
public class RecognizeFaces extends OneShotBehaviour{

    private ArrayList facesToRecognize = new ArrayList();
    private AID contractorAID;
    private AID detectorAID;
    private boolean areFacesSend = false;
    private boolean isResponder = false;
    private Recognizer recognizer;
    private boolean isPropagate = true;
    private long time = 0;
    private long timeFisher = 0;
    private long timeLbph = 0;
    
    public RecognizeFaces(Agent a, ArrayList facesToRecognize, AID contractorAID, 
            AID detectorAID, boolean areFacesSend, boolean isResponder)
    {
        super(a);
        this.recognizer = (Recognizer) a;
        for(int i = 0; i < facesToRecognize.size(); i++)
        {
            this.facesToRecognize.add(facesToRecognize.get(i));
        }
        this.contractorAID = contractorAID;
        this.detectorAID = detectorAID;
        this.areFacesSend = areFacesSend;
        this.isResponder = isResponder;
    }
    
    public RecognizeFaces(Agent a, ArrayList facesToRecognize, AID contractorAID, 
            AID detectorAID, boolean areFacesSend, boolean isResponder, boolean isPropagate)
    {
        super(a);
        this.recognizer = (Recognizer) a;
        for(int i = 0; i < facesToRecognize.size(); i++)
        {
            this.facesToRecognize.add(facesToRecognize.get(i));
        }
        this.contractorAID = contractorAID;
        this.detectorAID = detectorAID;
        this.areFacesSend = areFacesSend;
        this.isResponder = isResponder;
        this.isPropagate = isPropagate;
    }
    
    @Override
    public void action() {
        if(this.areFacesSend)
        {
            //recognize faces and sends result to remote Contractor agents
            for(int i = 0; i < this.facesToRecognize.size(); i++)
            {
                MatOfByte byteface = new MatOfByte();
                CuttedFace cutFace = (CuttedFace) this.facesToRecognize.get(i);
                byteface.fromArray(cutFace.getCutface());
                Mat face = Imgcodecs.imdecode(byteface, Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                Imgproc.resize(face,face,new Size(cutFace.getSize(),cutFace.getSize()));
                try 
                {
                    //recognize face
                    this.time = System.currentTimeMillis();
                    Thread.sleep(1478);
                    Vector recognitionResults = this.recognize(face);
                    System.out.println("Recognition time: " + (System.currentTimeMillis() - this.time) 
                            + " [ms]");
                    //send result to remote Contractor agent
                    Contract contract = new Contract();
                    ACLMessage toSend = new ACLMessage(ACLMessage.INFORM_REF);
                    CuttedFace cutFaceToSend = new CuttedFace();
                    ArrayList facesToSend = new ArrayList();
                    cutFaceToSend.setPredictlabel((int) recognitionResults.get(0));
                    cutFaceToSend.setConfidence((double) recognitionResults.get(1));
                    cutFaceToSend.setFrameNum(cutFace.getFrameNum());
                    cutFaceToSend.setFaceNum(cutFace.getFaceNum());
                    facesToSend.add(cutFaceToSend);
                    contract.setCuttedFaces(facesToSend);
                    toSend.addReceiver(cutFace.getSender());
                    toSend.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
                    toSend.setOntology(this.myAgent.getContentManager().lookupOntology(Faces.ONT_NAME).getName());
                    this.myAgent.getContentManager().fillContent(toSend,contract);
                    this.myAgent.send(toSend);
                    System.out.println("-----------------------: result sent to " + cutFace.getSender().getLocalName());
                } catch (Codec.CodecException ex) {
                    Logger.getLogger(RecognizeFaces.class.getName()).log(Level.SEVERE, null, ex);
                } catch (OntologyException ex) {
                    Logger.getLogger(RecognizeFaces.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(RecognizeFaces.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //send propagate message to Detector to start a new frame extraction
            if(this.isPropagate)
            {
                this.myAgent.addBehaviour(new SendPropagateMessage(this.myAgent,this.detectorAID));
            }
        }
        else
        {
            //recognize faces and write results in a local file
            for(int i = 0; i < this.facesToRecognize.size(); i++)
            {
                try 
                {
                    MatOfByte byteface = new MatOfByte();
                    CuttedFace cutFace = (CuttedFace) this.facesToRecognize.get(i);
                    byteface.fromArray(cutFace.getCutface());
                    Mat face = Imgcodecs.imdecode(byteface, Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                    Imgproc.resize(face,face,new Size(cutFace.getSize(),cutFace.getSize()));
                    //recognize face
                    this.time = System.currentTimeMillis();
                    Thread.sleep(1478);
                    Vector recognitionResults = this.recognize(face);
                    System.out.println("Recognition time: " + (System.currentTimeMillis() - this.time) 
                            + " [ms]");
                    //write recognition result in a local file
                    this.recognizer.getWriter().println("Frame number: " + cutFace.getFrameNum()
                            + " Face number: " + cutFace.getFaceNum()
                            + " Label: " + recognitionResults.get(0)
                            + " Confidence: " + recognitionResults.get(1));
                    this.recognizer.getWriter().flush();
                } catch (InterruptedException ex) {
                    Logger.getLogger(RecognizeFaces.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            /*send propagate message to Detector to start a new frame extraction.
            If the local Contractor is a Responder this message has to be sent after the 
            recognition of the faces received from remote is finished; otherwise this 
            message has to be sent after the local recognition is ended.*/
            //this.isResponder = false; //TODO: remove this in the final version of the program
            if(!this.isResponder)
            {
                this.myAgent.addBehaviour(new SendPropagateMessage(this.myAgent,this.detectorAID));
            }
            /*send cancel message to Contractor to tell him that local recognition is finished;
            this terminates also negotiation phase*/
            this.myAgent.addBehaviour(new SendCancelMessage(this.myAgent,this.contractorAID));
        }
    }
    
    private Vector recognize(Mat face)
    {
        this.timeFisher = System.currentTimeMillis();
        Vector confidencesFisher = this.recognizer.getFf().predictVector(face);
        System.out.println("Rec time fisher: " + (System.currentTimeMillis() - this.timeFisher));
        this.timeLbph = System.currentTimeMillis();
        Vector confidencesLbph = this.recognizer.getLbph().predictVector(face);
        System.out.println("Rec time lbph: " + (System.currentTimeMillis() - this.timeLbph));
        Vector recognitionResults = new Vector();
        if(confidencesFisher.size() != 0 && confidencesLbph.size() != 0)
        {
            Vector results = new Vector();
            this.normMinMax(confidencesFisher);
            this.normMinMax(confidencesLbph);
            for(int i = 0; i < confidencesFisher.size(); i++)
            {
                results.add((double) confidencesFisher.get(i) + (double) confidencesLbph.get(i));
            }
            int label = this.searchMin(results);
            double dist = (double) results.get(label);
            recognitionResults.add(label);
            recognitionResults.add(dist);
        }
        else
        {
            System.out.println("Prediction doesn't work");
        }
        return recognitionResults;
    }
    
    private int searchMin(Vector v)
    {
        double min = Double.MAX_VALUE;
        int index = -1;
        for(int i = 0; i < v.size(); i++)
        {
            if((double) v.get(i) < min)
            {
                min = (double) v.get(i);
                index = i;
            }
        }
        return index;
    }
    
    private int searchMax(Vector v)
    {
        double max = -1;
        int index = -1;
        for(int i = 0; i < v.size(); i++)
        {
            if((double) v.get(i) > max)
            {
                max = (double) v.get(i);
                index = i;
            }
        }
        return index;
    }
    
    private void normMinMax(Vector v)
    {
        int min_index = this.searchMin(v);
        int max_index = this.searchMax(v);
        double min = (double) v.get(min_index);
        double max = (double) v.get(max_index);
        for(int i = 0; i < v.size(); i++)
        {
            double data = (double) v.remove(i);
            double dataNorm = (data - min) / (max - min);
            v.insertElementAt(dataNorm,i);
        }
    }
}
