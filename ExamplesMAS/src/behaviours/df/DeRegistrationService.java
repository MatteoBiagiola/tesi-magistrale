/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.df;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author matteo
 */
public class DeRegistrationService extends DFInteraction{
    
    
    public DeRegistrationService(Agent a, ACLMessage msg) {
        super(a, msg);
    }

    @Override
    protected void handleDFFailure(ACLMessage inform)
    {
        System.out.println(this.myAgent.getLocalName() + " : unregistration fails");
    }
    
    @Override
    protected void handleDFModifies(AID agent)
    {
        if(this.myAgent.getLocalName().contains("Contractor"))
        {
            System.out.println(this.myAgent.getLocalName() + ": Local-negotiation and remote-recognition services unregistered");
        }
        else if(this.myAgent.getLocalName().contains("Recognizer"))
        {
            System.out.println(this.myAgent.getLocalName() + ": Local-recognition service unregistered");
        }
        else if(this.myAgent.getLocalName().contains("Detector"))
        {
            System.out.println(this.myAgent.getLocalName() + ": Local-detection service unregistered");
        }
        else if(this.myAgent.getLocalName().contains("Extractor"))
        {
            System.out.println(this.myAgent.getLocalName() + ": Local-extraction service unregistered");
        }
    }
}
