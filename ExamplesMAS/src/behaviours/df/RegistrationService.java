/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.df;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author matteo
 */
public class RegistrationService extends DFInteraction{
    
    
    public RegistrationService(Agent a, ACLMessage msg) {
        super(a, msg);
    }
    
    public RegistrationService(Agent a, ACLMessage msg, long removedMainInstant) {
        super(a, msg, removedMainInstant);
    }
    
    
    @Override
    protected void handleDFFailure(ACLMessage inform)
    {
        System.out.println(this.myAgent.getLocalName() + ": registration fails");
    }
    
    @Override
    protected void handleDFModifies(AID agent)
    {
        if(agent.getLocalName().contains("Contractor"))
        {
            System.out.println(this.myAgent.getLocalName() + ": Local-negotiation and remote-recognition services registered");
        }
        else if(agent.getLocalName().contains("Recognizer"))
        {
            System.out.println(this.myAgent.getLocalName() + ": Local-recognition service registered");
        }
        else if(agent.getLocalName().contains("Detector"))
        {
            System.out.println(this.myAgent.getLocalName() + ": Local-detection service registered");
        }
        else if(agent.getLocalName().contains("Extractor"))
        {
            System.out.println(this.myAgent.getLocalName() + ": Local-extraction service registered");
        }
    }
}
