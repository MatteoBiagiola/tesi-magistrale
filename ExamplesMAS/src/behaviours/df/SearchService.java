/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.df;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author matteo
 */
public class SearchService extends DFInteraction{
    
    private ACLMessage msg;
    private long waitBeforeRequery;
    
    public SearchService(Agent a, long waitBeforeRequery, ACLMessage msg) {
        super(a, waitBeforeRequery, msg);
        this.msg = msg;
        this.waitBeforeRequery = waitBeforeRequery;
    }
    
    @Override
    protected void handleSearchResults(AID[] agents)
    {
        if(agents.length > 0)
        {
            
        }
    }
    
    @Override
    protected void handlePeriodElapsed()
    {
        System.out.println(this.myAgent.getLocalName() + ": period elapsed");
        this.myAgent.addBehaviour(new SearchService(this.myAgent,
                this.waitBeforeRequery,this.msg));
    }
    
}
