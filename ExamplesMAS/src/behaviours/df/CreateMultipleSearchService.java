/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.df;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import jade.util.leap.HashMap;
import jade.util.leap.Iterator;
import jade.util.leap.Map;
import jade.util.leap.Set;
import utilities.CreateMessageForDF;

/**
 *
 * @author matteo
 */
public class CreateMultipleSearchService extends OneShotBehaviour{
    
    private int policy;
    private ArrayList serviceTypes = new ArrayList();
    private long waitBeforeRequery;
    private Map servicesMap = new HashMap(); 
    
    public CreateMultipleSearchService(Agent a, int policy, long waitBeforeRequery, 
            ArrayList serviceTypes)
    {
        super(a);
        this.policy = policy;
        this.waitBeforeRequery = waitBeforeRequery;
        for(int i = 0; i < serviceTypes.size(); i++)
        {
            this.serviceTypes.add(serviceTypes.get(i));
        }
    }
    
    public CreateMultipleSearchService(Agent a, int policy, long waitBeforeRequery, 
            ArrayList serviceTypes, Map servicesMap)
    {
        super(a);
        this.policy = policy;
        this.waitBeforeRequery = waitBeforeRequery;
        for(int i = 0; i < serviceTypes.size(); i++)
        {
            this.serviceTypes.add(serviceTypes.get(i));
        }
        Set keySet = servicesMap.keySet();
        for(Iterator it = keySet.iterator();it.hasNext();)
        {
            String serviceType = (String) it.next();
            this.servicesMap.put(serviceType, servicesMap.get(serviceType));
        }
    }
    

    @Override
    public void action() {
        ParallelBehaviour par = new ParallelBehaviour(this.myAgent,this.policy){

            @Override
            public int onEnd()
            {
                System.out.println(this.myAgent.getLocalName() + ": onEnd multiple search service");
                int results = handleOnEnd(servicesMap,serviceTypes);
                if(results != 0)
                {
                    this.myAgent.addBehaviour(new WakerBehaviour(this.myAgent,waitBeforeRequery) 
                    {
                        @Override
                        public void onWake()
                        {
                            System.out.println(this.myAgent.getLocalName() + ": onWake services requery");
                            handleRequery(servicesMap,serviceTypes);
                        }
                    });
                }
                return 0;
            }
        };
        Behaviour b = null;
        for(int i = 0; i < this.serviceTypes.size(); i++)
        {
            if(!servicesMap.containsKey(this.serviceTypes.get(i)))
            {
                ACLMessage toDF = CreateMessageForDF.createSearchMessage(this.myAgent, (String) this.serviceTypes.get(i));
                b = new DFInteraction(this.myAgent,toDF,i){
                    @Override
                    protected void handleSearchResults(AID[] agentsAID, int serviceIndex)
                    {
                        if(agentsAID.length > 0)
                        {
                            servicesMap.put(serviceTypes.get(serviceIndex),agentsAID);
                            System.out.println(this.myAgent.getLocalName() + ": service FOUND " + serviceTypes.get(serviceIndex));
                        }
                        else
                        {
                            System.out.println(this.myAgent.getLocalName() + ": service NOT FOUND " + serviceTypes.get(serviceIndex));
                        }
                        handleResults(agentsAID,serviceIndex);
                    }
                };
                par.addSubBehaviour(b);
            }
        }
        this.myAgent.addBehaviour(par);
    }
    
    
    public int handleOnEnd(Map servicesMap, ArrayList serviceTypes)
    {
        return 0;
    }
    
    protected void handleResults(AID[] agentsAID, int serviceIndex)
    {
        
    }
    
    protected void handleRequery(Map servicesMap, ArrayList serviceTypes)
    {
        
    }
    
}
