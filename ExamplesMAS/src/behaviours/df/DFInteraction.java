/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours.df;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FIPAManagementVocabulary;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matteo
 */
public class DFInteraction extends AchieveREInitiator{
    
    private String action;
    private static long removedMainInstant = 0;
    private long waitBeforeRequery = 0;
    protected int serviceIndex = -1;
    private static PrintWriter writer;
    private static int count = 0;
    
    public DFInteraction(Agent a, ACLMessage msg) {
        super(a, msg);
        this.setAction(msg);
    }
        
    public DFInteraction(Agent a, long waitBeforeRequery, ACLMessage msg) {
        super(a, msg);
        this.setAction(msg);
        this.waitBeforeRequery = waitBeforeRequery;
    }
    
    public DFInteraction(Agent a, ACLMessage msg, int serviceIndex) {
        super(a, msg);
        this.setAction(msg);
        this.waitBeforeRequery = waitBeforeRequery;
        this.serviceIndex = serviceIndex;
    }
    
    public DFInteraction(Agent a, ACLMessage msg, long removedMainInstant) {
        super(a, msg);
        this.setAction(msg);
        
        if(DFInteraction.getRemovedMainInstant() == 0)
        {
            DFInteraction.setRemovedMainInstant(removedMainInstant);
            System.out.println("???????????????????????????????????????? " + this.myAgent.getLocalName() 
                    + ": removed main instant: " + DFInteraction.getRemovedMainInstant());
        }
        
        String containerNumber = this.myAgent.getLocalName().substring(this.myAgent.getLocalName().length()-1);
        try
        {
            Integer.parseInt(containerNumber);
        }catch(NumberFormatException ex){
            containerNumber = "0";
        }
        
        if(DFInteraction.getWriter() == null)
        {
            try 
            {
                DFInteraction.setWriter(new PrintWriter("resources/output/rec_results/" + containerNumber + "_reconfiguration_time.txt","UTF-8"));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DFInteraction.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(DFInteraction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    protected void handleAgree(ACLMessage agree)
    {
        System.out.println(this.myAgent.getLocalName() + ": agree message received for the action "
                + this.action + " content " + agree.getContent());
    }
    
    @Override
    protected void handleRefuse(ACLMessage refuse)
    {
        System.out.println(this.myAgent.getLocalName() + ": refuse message received for the action "
                + this.action + " refuse " + refuse.getContent());
    }
    
    @Override
    protected void handleInform(ACLMessage inform)
    {
        System.out.println(this.myAgent.getLocalName() + ": inform message received for the action "
                + this.action);
        if(this.action.equals(FIPAManagementVocabulary.REGISTER) || 
                this.action.equals(FIPAManagementVocabulary.DEREGISTER) ||
                this.action.equals(FIPAManagementVocabulary.MODIFY))
        {
            try 
            {
                DFAgentDescription agent = DFService.decodeDone(inform.getContent());
                if(DFInteraction.getRemovedMainInstant() > 0 && DFInteraction.getWriter() != null) 
                {
                    if(DFInteraction.getCount() == 3)
                    {
                        System.out.println("???????????????????????????????????????? = 3: " + DFInteraction.getCount());
                        DFInteraction.setCount(0);
                        DFInteraction.getWriter().println(System.currentTimeMillis() + "-Total re-configuration time: " 
                            + (System.currentTimeMillis() - DFInteraction.getRemovedMainInstant()) + " [ms]");
                        DFInteraction.getWriter().println();
                        DFInteraction.getWriter().flush();
                        DFInteraction.setRemovedMainInstant(0);
                    }
                    else if(DFInteraction.getCount() < 4)
                    {
                        System.out.println("???????????????????????????????????????? < 4: " + DFInteraction.getCount());
                        DFInteraction.setCount(DFInteraction.getCount() + 1);
                        if(this.myAgent.getLocalName().contains("Contractor"))
                        {
                            DFInteraction.getWriter().println(System.currentTimeMillis() + "-" 
                                    + this.myAgent.getLocalName() + " re-configuration time: " 
                            + (System.currentTimeMillis() - DFInteraction.getRemovedMainInstant()) + " [ms]");
                            DFInteraction.getWriter().flush();
                        }
                    }
                }
                System.out.println(this.myAgent.getLocalName() + ": agent requesting " + this.action + " action " + agent.getName().getLocalName());
                handleDFModifies(agent.getName());
            } catch (FIPAException ex) {
                handleDFFailure(inform);
                Logger.getLogger(DFInteraction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(this.action.equals(FIPAManagementVocabulary.SEARCH))
        {
            try 
            {
                DFAgentDescription[] agents = DFService.decodeResult(inform.getContent());
                AID[] agentsAID = new AID[agents.length];
                System.out.println(this.myAgent.getLocalName() + ": search results ");
                for(int i = 0; i < agents.length; i++)
                {
                    System.out.println(agents[i].getName());
                    agentsAID[i] = agents[i].getName();
                }
                System.out.println();
                if(this.serviceIndex > -1)
                {
                    handleSearchResults(agentsAID,this.serviceIndex);
                }
                else
                {
                    handleSearchResults(agentsAID);
                }
                if(this.waitBeforeRequery > 0 && agents.length == 0)
                {
                    this.myAgent.addBehaviour(new WakerBehaviour(this.myAgent,this.waitBeforeRequery) 
                    {
                        @Override
                        public void onWake()
                        {
                            if(serviceIndex > -1)
                            {
                                handlePeriodElapsed(serviceIndex);
                            }
                            else
                            {
                                handlePeriodElapsed();
                            }
                        }
                    });
                }
            } catch (FIPAException ex) {
                Logger.getLogger(DFInteraction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    protected void handleDFFailure(ACLMessage inform)
    {
        
    }
    
    protected void handleDFModifies(AID agent)
    {
        
    }
    
    protected void handleSearchResults(AID[] agentsAID)
    {
        
    }
    
    protected void handleSearchResults(AID[] agentsAID, int serviceIndex)
    {
        
    }
    
    protected void handlePeriodElapsed()
    {
        
    }
    
    protected void handlePeriodElapsed(int serviceIndex)
    {
        
    }
    
    private void setAction(ACLMessage msg)
    {
        if(msg.getContent().contains(FIPAManagementVocabulary.REGISTER))
            this.action = FIPAManagementVocabulary.REGISTER;
        else if(msg.getContent().contains(FIPAManagementVocabulary.DEREGISTER))
            this.action = FIPAManagementVocabulary.DEREGISTER;
        else if(msg.getContent().contains(FIPAManagementVocabulary.MODIFY))
            this.action = FIPAManagementVocabulary.MODIFY;
        else if(msg.getContent().contains(FIPAManagementVocabulary.SEARCH))
            this.action = FIPAManagementVocabulary.SEARCH;
        else this.action = "error";
    }
    
    public synchronized static long getRemovedMainInstant() {
        return removedMainInstant;
    }

    public synchronized static void setRemovedMainInstant(long aRemovedMainInstant) {
        removedMainInstant = aRemovedMainInstant;
    }
    
    public synchronized static PrintWriter getWriter() {
        return writer;
    }

    public synchronized static void setWriter(PrintWriter aWriter) {
        writer = aWriter;
    }

    public synchronized static int getCount() {
        return count;
    }

    public synchronized static void setCount(int aCount) {
        count = aCount;
    }
}