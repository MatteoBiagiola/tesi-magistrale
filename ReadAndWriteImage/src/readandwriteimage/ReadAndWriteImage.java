/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readandwriteimage;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

/**
 *
 * @author matteo
 */
public class ReadAndWriteImage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);
        Mat img = new Mat();
        img = Imgcodecs.imread("img.bmp");
        Imgcodecs.imwrite("out.bmp", img);
    }
    
}
