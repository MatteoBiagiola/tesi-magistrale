#!/bin/bash 
export JAVA_HOME=/opt/jdk1.8.0_60                                 
export PATH=$JAVA_HOME/bin:$PATH                                  
export CLASSPATH=.:/opt/JADE:/opt/JADE/lib/jade.jar:/opt/JADE/lib/commons-codec-1.3.jar:/opt/opencv/lib/opencv-300.jar:bin
cd ExamplesMAS-Replication/
java jade.Boot -backupmain -container-name Main-Container-4 -host 192.168.0.23 -agents Recognizer4:agents.Recognizer -services "jade.core.replication.MainReplicationService;jade.core.replication.AddressNotificationService;jade.core.event.NotificationService"
