#!/bin/bash
xterm -hold -title Galileo1 -e "cd /mnt/hgfs/tesi/Script/MAS/NoFaultTolerance/MultipleHosts/ && ssh root@$GALILEO1 'bash -s' < Galileo1.sh" &
sleep 12s
xterm -hold -title Galileo2 -e "cd /mnt/hgfs/tesi/Script/MAS/NoFaultTolerance/MultipleHosts/ && ssh root@$GALILEO2 'bash -s' < Galileo2.sh" &
xterm -hold -title Galileo3 -e "cd /mnt/hgfs/tesi/Script/MAS/NoFaultTolerance/MultipleHosts/ && ssh root@$GALILEO3 'bash -s' < Galileo3.sh" &
xterm -hold -title Galileo4 -e "cd /mnt/hgfs/tesi/Script/MAS/NoFaultTolerance/MultipleHosts/ && ssh root@$GALILEO4 'bash -s' < Galileo4.sh" &
xterm -hold -title Galileo5 -e "cd /mnt/hgfs/tesi/Script/MAS/NoFaultTolerance/MultipleHosts/ && ssh root@$GALILEO5 'bash -s' < Galileo5.sh" &
xterm -hold -title Galileo6 -e "cd /mnt/hgfs/tesi/Script/MAS/NoFaultTolerance/MultipleHosts/ && ssh root@$GALILEO6 'bash -s' < Galileo6.sh" &
