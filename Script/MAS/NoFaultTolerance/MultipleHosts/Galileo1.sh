#!/bin/bash 
export JAVA_HOME=/opt/jdk1.8.0_60                                 
export PATH=$JAVA_HOME/bin:$PATH                                  
export CLASSPATH=.:/opt/JADE:/opt/JADE/lib/jade.jar:/opt/JADE/lib/commons-codec-1.3.jar:/opt/opencv/lib/opencv-300.jar:bin
cd ExamplesMAS-NoReplication/
java jade.Boot -name Platform -container-name Main-Container-1 -host 192.168.0.34 -agents Recognizer1:agents.Recognizer
