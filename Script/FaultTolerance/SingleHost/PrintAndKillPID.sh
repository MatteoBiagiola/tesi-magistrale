#!/bin/bash
killPID()
{
	sleep 10s
	kill -9 $PID_MainContainer
	echo "MainContainer killed"
	sleep 10s
	kill -9 $PID_MainContainerReplica1
	echo "MainContainerReplica1 killed"
	sleep 10s
	kill -9 $PID_MainContainerReplica2
	echo "MainContainerReplica2 killed"
}
killPIDReverse()
{
	sleep 10s
	kill -9 $PID_MainContainerReplica1
	echo "MainContainerReplica1 killed"
	sleep 10s
	kill -9 $PID_MainContainerReplica2
	echo "MainContainerReplica2 killed"
	sleep 10s
	kill -9 $PID_MainContainer
	echo "MainContainer killed"
}
PID_MainContainer=0
PID_MainContainerReplica1=0
PID_MainContainerReplica2=0
echo "MainContainer: my process id (PID) is the following: "
/bin/ps -fu $USER | grep java | grep Platform | awk '{print $2}'
echo "MainContainerReplica1: my process id (PID) is the following: "
/bin/ps -fu $USER | grep java | grep Agent0 | awk '{print $2}'
echo "MainContainerReplica2: my process id (PID) is the following: "
/bin/ps -fu $USER | grep java | grep Agent1 | awk '{print $2 "\n"}'
echo "Killing containers..."
PID_MainContainer=$(/bin/ps -fu $USER | grep java | grep Platform | awk '{print $2}')
PID_MainContainerReplica1=$(/bin/ps -fu $USER | grep java | grep Agent0 | awk '{print $2}')
PID_MainContainerReplica2=$(/bin/ps -fu $USER | grep java | grep Agent1 | awk '{print $2}')
test "$1" = "" && killPID
test "$1" = "reverse" && killPIDReverse



