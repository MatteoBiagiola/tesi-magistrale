#!/bin/bash
export JAVA_HOME=/opt/jdk1.8.0_60                                 
export PATH=$JAVA_HOME/bin:$PATH                                  
export CLASSPATH=.:/opt/JADE:/opt/JADE/lib/jade.jar:/opt/JADE/lib/commons-codec-1.3.jar:/opt/opencv/lib/opencv-300.jar:bin
cd FaultTolerance/
java jade.Boot -container -container-name Container-2 -host 192.168.0.23 -local-host 192.168.0.23 -agents Agent1:agents.Agent1 -services "jade.core.replication.AddressNotificationService;jade.core.event.NotificationService"
