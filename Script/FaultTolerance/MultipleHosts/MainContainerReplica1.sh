#!/bin/bash
export JAVA_HOME=/opt/jdk1.8.0_60                                 
export PATH=$JAVA_HOME/bin:$PATH                                  
export CLASSPATH=.:/opt/JADE:/opt/JADE/lib/jade.jar:/opt/JADE/lib/commons-codec-1.3.jar:/opt/opencv/lib/opencv-300.jar:bin
cd FaultTolerance/
java jade.Boot -backupmain -local-port 1100 -host 192.168.0.22 -port 1099 -agents Agent0:agents.Agent0 -mtps jade.mtp.http.MessageTransportProtocol"(http://192.168.0.23:7780/acc)" -services "jade.core.replication.MainReplicationService;jade.core.replication.AddressNotificationService;jade.core.event.NotificationService"
