#!/bin/bash
killPID()
{
	xterm -title KillMain -hold -e "cd /mnt/hgfs/tesi/Script/FaultTolerance/MultipleHosts/ && ssh root@192.168.0.22 'bash -s' < KillMain.sh" &
	sleep 5s
	xterm -title KillReplica1 -hold -e "cd /mnt/hgfs/tesi/Script/FaultTolerance/MultipleHosts/ && ssh root@192.168.0.23 'bash -s' < KillReplica1.sh" &
	sleep 5s
	xterm -title KillReplica2 -hold -e "cd /mnt/hgfs/tesi/Script/FaultTolerance/MultipleHosts/ && ssh root@192.168.0.24 'bash -s' < KillReplica2.sh" &
}
killPIDReverse()
{
	xterm -title KillReplica1 -hold -e "cd /mnt/hgfs/tesi/Script/FaultTolerance/MultipleHosts/ && ssh root@192.168.0.23 'bash -s' < KillReplica1.sh" &
	sleep 5s
	xterm -title KillReplica2 -hold -e "cd /mnt/hgfs/tesi/Script/FaultTolerance/MultipleHosts/ && ssh root@192.168.0.24 'bash -s' < KillReplica2.sh" &
	sleep 5s
	xterm -title KillMain -hold -e "cd /mnt/hgfs/tesi/Script/FaultTolerance/MultipleHosts/ && ssh root@192.168.0.22 'bash -s' < KillMain.sh" &
}
xterm -title MainContainer -hold -e "cd /mnt/hgfs/tesi/Script/FaultTolerance/MultipleHosts/ && ssh root@192.168.0.22 'bash -s' < MainContainer.sh" &
sleep 10s
xterm -title MainContainerReplica1 -hold -e "cd /mnt/hgfs/tesi/Script/FaultTolerance/MultipleHosts/ && ssh root@192.168.0.23 'bash -s' < MainContainerReplica1.sh" &
sleep 10s
xterm -title MainContainerReplica2 -hold -e "cd /mnt/hgfs/tesi/Script/FaultTolerance/MultipleHosts/ && ssh root@192.168.0.24 'bash -s' < MainContainerReplica2.sh" &
sleep 10s
if [ -z "$1" ]
then
	killPID
elif [ "$1" = "reverse" ]
then
	killPIDReverse
else
	echo "Argument not take"
fi


