#!/bin/bash
export JAVA_HOME=/opt/jdk1.8.0_60                                 
export PATH=$JAVA_HOME/bin:$PATH                                  
export CLASSPATH=.:/opt/JADE:/opt/JADE/lib/jade.jar:/opt/JADE/lib/commons-codec-1.3.jar:/opt/opencv/lib/opencv-300.jar:bin
echo "Killing MainReplica2..."
sleep 20s
PID_Main=$(/bin/ps | grep java | grep backupmain | awk '{print $1}')
kill -9 $PID_Main
echo "Killed"
