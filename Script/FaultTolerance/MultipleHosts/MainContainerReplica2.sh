#!/bin/bash
export JAVA_HOME=/opt/jdk1.8.0_60                                 
export PATH=$JAVA_HOME/bin:$PATH                                  
export CLASSPATH=.:/opt/JADE:/opt/JADE/lib/jade.jar:/opt/JADE/lib/commons-codec-1.3.jar:/opt/opencv/lib/opencv-300.jar:bin
cd FaultTolerance/
java jade.Boot -backupmain -local-port 1101 -host 192.168.0.23 -port 1100 -agents Agent1:agents.Agent1 -mtps jade.mtp.http.MessageTransportProtocol"(http://192.168.0.24:7781/acc)" -services "jade.core.replication.MainReplicationService;jade.core.replication.AddressNotificationService;jade.core.event.NotificationService"
