#include <jni.h>
#include "vectorjni_VectorJNI.h"
#include <iostream>
#include <vector>
using namespace std;
extern "C" 
{
	void fillVector(vector<int>& labels,vector<double>& confidences)
	{
		int size = 10;
		for(int i = 0; i < size; i++)
		{
			labels.push_back(i);
			confidences.push_back(0.5);
		}
	}
	/*JNIEXPORT void JNICALL Java_vectorjni_VectorJNI_fillvector(JNIEnv *env, jobject thisObj, jobject obj1, jobject obj2)
	{
		jclass classVector = env->FindClass("java/util/Vector");
		jmethodID add = env->GetMethodID(classVector,"add","(Ljava/lang/Object;)Z");
		vector<int> labels;
		vector<double> confidences;
		fillVector(labels,confidences);
		jsize length = labels.size(); 
		for(int i = 0;i < length;i++)
		{
			jint numInt = labels[i];
			jdouble numDouble = confidences[i];
			env->CallBooleanMethod(obj1,add,numInt);
			env->CallBooleanMethod(obj2,add,numDouble);
		}
	}*/
	
	/*JNIEXPORT void JNICALL Java_vectorjni_VectorJNI_fillvector(JNIEnv *env, jobject thisObj, jintArray intArray, jdoubleArray doubleArray)
	{
		vector<int> labels;
		vector<double> confidences;
		fillVector(labels,confidences);
		jsize len = env->GetArrayLength(intArray);
		cout<<"len: "<<len;
		jsize length = labels.size();
		intArray = env->NewIntArray(length);
		doubleArray = env->NewDoubleArray(length);
		jint fillInt[length];
		jdouble fillDouble[length];
		jint* intptr = env->GetIntArrayElements(intArray,0);
		jdouble* doubleptr = env->GetDoubleArrayElements(doubleArray,0);
		for(int i = 0;i < length;i++)
		{
			jint numInt = labels[i];
			jdouble numDouble = confidences[i];
			intptr[i] = numInt;
			doubleptr[i] = numDouble;
			fillInt[i] = numInt;
			fillDouble[i] = numDouble;
		}
		for(int i = 0;i < length; i++)
		{
			cout<<intptr[i]<<"-"<<doubleptr[i]<<endl;
		}
		env->ReleaseIntArrayElements(intArray,intptr,0);
		env->ReleaseDoubleArrayElements(doubleArray,doubleptr,0);
		env->SetIntArrayRegion(intArray,0,length,intptr);
		env->SetDoubleArrayRegion(doubleArray,0,length,doubleptr);
	}*/
	
	JNIEXPORT jobject JNICALL Java_vectorjni_VectorJNI_fillvector(JNIEnv *env, jobject thisObj)
	{
		jclass classVector = env->FindClass("java/util/Vector");
		jclass classInteger = env->FindClass("java/lang/Integer");
		jclass classDouble = env->FindClass("java/lang/Double");
		jmethodID cntVector = env->GetMethodID(classVector,"<init>","()V");
		jmethodID cntInteger = env->GetMethodID(classInteger,"<init>","(I)V");
		jmethodID cntDouble = env->GetMethodID(classDouble,"<init>","(D)V");
		jmethodID add = env->GetMethodID(classVector,"add","(Ljava/lang/Object;)Z");
		jobject objVectorResult = env->NewObject(classVector,cntVector);
		jobject objVectorInt = env->NewObject(classVector,cntVector);
		jobject objVectorDouble = env->NewObject(classVector,cntVector);
		vector<int> labels;
		vector<double> confidences;
		fillVector(labels,confidences);
		jsize length = labels.size();
		for(int i = 0;i < length; i++)
		{
			jobject objInteger = env->NewObject(classInteger,cntInteger,labels[i]);
			jobject objDouble = env->NewObject(classDouble,cntDouble,confidences[i]);
			env->CallBooleanMethod(objVectorInt,add,objInteger);
			env->CallBooleanMethod(objVectorDouble,add,objDouble);
		}
		env->CallBooleanMethod(objVectorResult,add,objVectorInt);
		env->CallBooleanMethod(objVectorResult,add,objVectorDouble);
		return objVectorResult;
	}
}
