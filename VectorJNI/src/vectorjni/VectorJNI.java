/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vectorjni;

import java.util.Vector;

/**
 *
 * @author matteo
 */
public class VectorJNI {

    static{
        System.loadLibrary("VectorJNI");
    }
    /**
     * @param args the command line arguments
     */
    private native Vector fillvector();
    
    public static void main(String[] args) {
        // TODO code application logic here
        Vector v = new VectorJNI().fillvector();
        if(v.size() != 0)
        {
            Vector v1 = (Vector) v.get(0);
            Vector v2 = (Vector) v.get(1);
            System.out.println("The first vector is");
            for(int i = 0; i < v1.size(); i++)
            {
                System.out.println(v1.get(i));
            }
            System.out.println("The second vector is");
            for(int i = 0; i < v2.size(); i++)
            {
                System.out.println(v2.get(i));
            }
        }
        else
        {
            System.out.println("The vector is empty");
        }
    }
    
}
