/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videoframes;

import com.sun.management.OperatingSystemMXBean;
import java.lang.management.ManagementFactory;
/**
 *
 * @author matteo
 */
public class SystemMonitor implements Runnable{
    
    private static final long mb = 1024L * 1024L;
    private static final long percent = 100;
    private OperatingSystemMXBean osBean;
    
    public SystemMonitor()
    {
        this.setOsBean(ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class));
    }
        
    public double getJvmCpuUsed() {
        return (this.getOsBean().getProcessCpuLoad()*percent);
    }

    public double getSystemCpuUsed() {
        return (this.getOsBean().getSystemCpuLoad()*percent);
    }
    
    public long getTotalRamSize()
    {
        return (this.getOsBean().getTotalPhysicalMemorySize())/mb;
    }
    
    public long getTotalSwapSize()
    {
        return (this.getOsBean().getTotalSwapSpaceSize())/mb;
    }
        
    public long getSwapMemoryUsed()
    {
        return (this.getOsBean().getTotalSwapSpaceSize() - this.getOsBean().getFreeSwapSpaceSize())/mb;
    }
    
    public long getRamUsed()
    {
        return (this.getOsBean().getTotalPhysicalMemorySize() -  this.getOsBean().getFreePhysicalMemorySize())/mb;
    }
    
    @Override
    public void run() {
        if(this.getRamUsed() > this.getTotalRamSize() - 50)
        {
            System.gc();
        }
    }

    public OperatingSystemMXBean getOsBean() {
        return osBean;
    }

    public void setOsBean(OperatingSystemMXBean osBean) {
        this.osBean = osBean;
    }
}