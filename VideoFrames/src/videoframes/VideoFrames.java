/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videoframes;

import java.io.File;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;
import org.opencv.videoio.VideoWriter;

/**
 *
 * @author matteo
 */
public class VideoFrames {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    private CascadeClassifier faceDetector;
    private String LbpClassifier;
    private String HaarClassifier;
    private MatOfRect faceDetections = new MatOfRect();
    private int flags;
    private Size minFeaturesSize;
    private Size maxFeaturesSize;
    private float scaleFactor;
    private int minNeighbors;
    private int numfaces;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        SystemMonitor sm = new SystemMonitor();
        VideoFrames vf = new VideoFrames();
        vf.numfaces = 0;
        vf.flags = Objdetect.CASCADE_SCALE_IMAGE;
        vf.minFeaturesSize = new Size(20,20);
        vf.maxFeaturesSize = new Size();
        vf.scaleFactor = (float) 1.2;
        vf.minNeighbors = 3;
        vf.LbpClassifier = "resources/lbpcascade_frontalface.xml";
        vf.HaarClassifier = "resources/haarcascade_frontalface_default.xml";
        vf.faceDetector = new CascadeClassifier(vf.LbpClassifier);
        long start = System.nanoTime();
        String videoName = "resources/video.mp4";
        if(args.length != 0)
        {
            videoName = "resources/" + args[0];
        }
        System.out.println(videoName);
        VideoCapture vd_read = new VideoCapture(videoName);
        int codec = (int) vd_read.get(Videoio.CAP_PROP_FOURCC);
        VideoWriter vd_make = new VideoWriter();
        vf.videoRead(vd_read);
        /*vf.videoMake(vd_make,codec,"resources/P2E_S1_C3.mp4",
                new File("resources/input_frames/P2E_S1_C3.1"), sm);
        long end = System.nanoTime();
        float timeElapsed = (float) ((end - start) / 10e8);
        System.out.println(String.format("Elapsed time: %s", timeElapsed));*/
    }
    
    public void videoRead(VideoCapture vd)
    {
        Mat frame = new Mat();
        Mat gray = new Mat();
        Mat grayEq = new Mat();
        int frameNum = 0;
        long time = 0;
        boolean extract = false;
        while(true)
        {
            frameNum = (int) vd.get(Videoio.CAP_PROP_POS_FRAMES);
            System.out.println("Frame num: " + frameNum);
            vd.set(Videoio.CAP_PROP_POS_FRAMES, frameNum);
            time = System.currentTimeMillis();
            extract = vd.read(frame);
            if(extract)
            {
                System.out.println("Extraction-time: " + 
                        (System.currentTimeMillis() - time) + " [ms]");
                Imgproc.cvtColor(this.shrinkFrame(frame,320),gray,Imgproc.COLOR_BGR2GRAY);
                /*Imgcodecs.imwrite(String.format("resources/frames/frame%05d.bmp",
                    (int) vd.get(Videoio.CAP_PROP_POS_FRAMES)),gray);*/
                Imgproc.equalizeHist(gray,grayEq);
                time = System.currentTimeMillis();
                this.faceDetector.detectMultiScale(grayEq,
                    this.faceDetections,this.scaleFactor, 
                    this.minNeighbors,this.flags,this.minFeaturesSize, 
                    this.maxFeaturesSize);
                System.out.println("Detection time: " + (System.currentTimeMillis() - time) 
                                + " [ms]");
                this.numfaces = 0;
                for(Rect rect: this.faceDetections.toArray())
                {
                    Rect r = new Rect(rect.x,rect.y,rect.width,rect.height);
                    Mat croppedFace = new Mat(gray,r);
                    String faceName = String.format("resources/faces/frame%05d",
                            (int) vd.get(Videoio.CAP_PROP_POS_FRAMES)) +
                            String.format("-%02d.pgm",this.numfaces);
                    Imgcodecs.imwrite(faceName,croppedFace);
                    this.numfaces = this.numfaces + 1;
                }
            }
            else
            {
                vd.release();
                System.out.println("Video ended");
                break;
            }
        }
    }
    
    public void videoMake(VideoWriter vw,int codec, String videoName, File imagesPath, SystemMonitor sm)
    {
        if(imagesPath.listFiles()[0].getPath().contains(".DS_Store"))
        {
            imagesPath.listFiles()[0].delete();
        }
        Mat image = Imgcodecs.imread(imagesPath.listFiles()[0].getPath());
        vw.open(videoName, codec, 30.0,
                new Size(image.width(),image.height()));
        for(File frame: imagesPath.listFiles())
        {
            if(frame.getPath().contains(".DS_Store"))
            {
                frame.delete();
            }
            else if(sm.getRamUsed() > sm.getTotalRamSize() - 70)
            {
                System.gc();
            }
            vw.write(Imgcodecs.imread(frame.getPath()));
        }
        vw.release();
    }
    
    private Mat shrinkFrame(Mat image, int detection_width)
    {
        Mat smallImage = new Mat();
        int scaledHeight = 0;
        Size rescale;
        float scale = image.cols()/(float) detection_width;
        System.out.println("Frame cols: " + image.cols() + " Frame rows: " + image.rows());
        if(image.cols() > detection_width)
        {
            scaledHeight = Math.round(image.rows()/scale);
            rescale = new Size(detection_width,scaledHeight);
            Imgproc.resize(image,smallImage,rescale);
        }
        else
        {
            smallImage = image;
        }
        System.out.println("Shrinked-frame cols: " + smallImage.cols() + " Shrinked-frame rows: " 
                + smallImage.rows());
        return smallImage;
    }

}
