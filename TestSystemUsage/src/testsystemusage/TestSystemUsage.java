/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testsystemusage;

import com.sun.management.OperatingSystemMXBean;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.management.ManagementFactory;

/**
 *
 * @author matteo
 */
public class TestSystemUsage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, FileNotFoundException, IOException {
        // TODO code application logic here  
        OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(  
                        OperatingSystemMXBean.class);  
        // What % CPU load this current JVM is taking, from 0.0-1.0  
        System.out.println(osBean.getProcessCpuLoad());  

        // What % load the overall system is at, from 0.0-1.0  
        System.out.println(osBean.getSystemCpuLoad());
        /*int mb = 1024*1024;
        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();
        System.out.println("##### Heap utilization statistics [MB] #####");
        //Print used memory
        System.out.println("Used Memory:"
            + (runtime.totalMemory() - runtime.freeMemory()) / mb);
        //Print free memory
        System.out.println("Free Memory:"
            + runtime.freeMemory() / mb);
        //Print total available memory
        System.out.println("Total Memory:" + runtime.totalMemory() / mb);
        //Print Maximum available memory
        System.out.println("Max Memory:" + runtime.maxMemory() / mb);*/
        
    } 
}
