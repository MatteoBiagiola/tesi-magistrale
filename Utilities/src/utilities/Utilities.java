/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

/**
 *
 * @author matteo
 */
public class Utilities {

    /**
     * @param args the command line arguments
     */
    private PrintWriter writer;
    private BufferedReader reader;
    
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        File file = new File("resources/input");
        Utilities utilities = new Utilities();
        //utilities.renameImages(file.listFiles());
        /*try {
            PrintWriter pw = new PrintWriter("resources/numImages.txt","UTF-8");
            utilities.countImages(file.listFiles(),pw);
            pw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        //utilities.deleteJpg(file.listFiles());
        /*try {
            utilities.formatResults(file.listFiles());
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        StringBuffer sb = new StringBuffer();
        utilities.convertImages(file.listFiles(),sb,"bmp","pgm");
    }
    
    public void deleteJpg(File[] files)
    {
        for(File file: files)
        {
            if(file.isDirectory())
            {
                this.deleteJpg(file.listFiles());
            }
            else if(file.getName().endsWith(".jpg"))
            {
                file.delete();
            }
        }
    }
    
    public void formatResults(File[] files) throws FileNotFoundException, UnsupportedEncodingException, IOException
    {
        for(File file: files)
        {
            if(file.isDirectory())
            {
                String outputString = file.getPath().replaceAll("input","output");
                File outputFile = new File(outputString);
                if(!outputFile.exists())
                {
                    outputFile.mkdir();
                }
                this.formatResults(file.listFiles());
            }
            else if (file.getPath().endsWith(".txt"))
            {
                this.setReader(new BufferedReader (new FileReader(file.getPath())));
                File fileOut = new File(file.getPath().replaceAll("input","output"));
                this.setWriter(new PrintWriter(fileOut.getPath(),"UTF-8"));
                String line;
                int cont = 0;
                int label = 0;
                int identified = 0;
                int semicolonIndex = 0;
                int prediction = -1;
                while((line = this.getReader().readLine()) != null)
                {
                    semicolonIndex = line.indexOf(";");
                    if(cont == 0)
                    {
                        this.getWriter().println(label);
                    }
                    else if(cont % 4 == 0)
                    {
                        label = label + 1;
                        this.getWriter().println(label);
                    }
                    if(label/10 < 1)
                    {
                        if(semicolonIndex == 8)
                        {
                            prediction = Integer.parseInt(line.substring(semicolonIndex-1,semicolonIndex));
                        }
                        else
                        {
                            prediction = Integer.parseInt(line.substring(semicolonIndex-2,semicolonIndex));
                        }
                        if(prediction == label)
                        {
                            identified++;
                        }
                    }
                    else
                    {
                        if(semicolonIndex == 8)
                        {
                            prediction = Integer.parseInt(line.substring(semicolonIndex-1,semicolonIndex));
                        }
                        else
                        {
                            prediction = Integer.parseInt(line.substring(semicolonIndex-2,semicolonIndex));
                        }
                        if(prediction == label)
                        {
                            identified++;
                        }
                    }
                    this.getWriter().println(line);
                    cont++;
                }
                this.getReader().close();
                this.getWriter().println();
                this.getWriter().println(
                        "Identified subjects: " + identified + " on " + 
                                cont + " so True Success Rate (TSR) is " + (float) (identified*100)/cont + 
                                " %");
                
                this.getWriter().close();
            }
        }
    }
    
    public void countImages(File[] files,PrintWriter pw)
    {
        for(File file: files)
        {
            if(file.isDirectory())
            {
                try
                {
                    pw.println(file.getPath() + ": " + file.listFiles().length);
                }
                catch(NullPointerException ex)
                {
                    pw.println(file.getPath() + ": " + 0);
                }
                pw.flush();
                this.countImages(file.listFiles(),pw);
            }
            else if(file.getPath().contains(".directory") || file.getPath().contains("DS_Store"))
            {
                file.delete();
            }
        }
    }
    
    public void renameImages(File[] files)
    {
        for(File file: files)
        {
            if(file.isDirectory())
            {
                this.renameImages(file.listFiles());
            }
            else if(file.getPath().contains(".directory") || file.getPath().contains("DS_Store"))
            {
                file.delete();
            }
            else
            {
                if(file.getPath().contains("C1"))
                {
                    String name = file.getName().replaceFirst("0","1-0");
                    File file1 = new File(file.getParent() + "/" + name);
                    file.renameTo(file1);
                    file.delete();
                }
                else if(file.getPath().contains("C2"))
                {
                    String name = file.getName().replaceFirst("0","2-0");
                    File file1 = new File(file.getParent() + "/" + name);
                    file.renameTo(file1);
                    file.delete();
                }
                else if(file.getPath().contains("C3"))
                {
                    String name = file.getName().replaceFirst("0","3-0");
                    File file1 = new File(file.getParent() + "/" + name);
                    file.renameTo(file1);
                    file.delete();
                }
            }
        }
    }
    
    public void convertImages(File[] imagesTest,StringBuffer sb,String extIn, String extOut)
    {
        for(File imageTest: imagesTest)
        {
            if(imageTest.getPath().contains(".DS_Store") || imageTest.getPath().contains(".directory"))
            {
                imageTest.delete();
            }
            else if(imageTest.isDirectory())
            {
                String inputDirName = imageTest.getPath();
                String outputDirName = inputDirName.replace("input","output");
                File outputDirFile = new File(outputDirName);
                if(!outputDirFile.exists())
                {
                    outputDirFile.mkdir();
                }
                this.convertImages(imageTest.listFiles(),sb,extIn,extOut);
            }
            else   
            {
                Mat image = this.readImage(imageTest.getPath(),sb,extIn,extOut);
                Imgcodecs.imwrite(sb.toString(),image);
            }
        }
    }
    
    public Mat readImage(String lineToRead, StringBuffer outputPath,String extIn,String extOut)
    {
        outputPath.delete(0,outputPath.length());
        outputPath.append(lineToRead.replace("input","output").replace("." + extIn,"." + extOut));
        return Imgcodecs.imread(lineToRead,Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }

    public BufferedReader getReader() {
        return reader;
    }

    public void setReader(BufferedReader reader) {
        this.reader = reader;
    }
}
