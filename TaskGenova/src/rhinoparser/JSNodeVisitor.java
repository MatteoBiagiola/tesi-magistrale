/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rhinoparser;

import java.util.HashMap;
import java.util.Map;
import org.mozilla.javascript.Token;
import org.mozilla.javascript.ast.AstNode;
import org.mozilla.javascript.ast.Block;
import org.mozilla.javascript.ast.FunctionNode;
import org.mozilla.javascript.ast.Name;
import org.mozilla.javascript.ast.NodeVisitor;

/**
 *
 * @author matteo
 */
class JSNodeVisitor implements NodeVisitor{

    private Map names = new HashMap();
    private String nodeString;
    
    @Override
    public boolean visit(AstNode an) {

        printNode(an);
        return true;
    }
    
    public void printNode(AstNode an)
    {
        
	int type = an.getType();
	switch (type)
	{
            case Token.NAME:
                    if (an instanceof Name)
                    {
                            Name name = (Name)an;
                            //String nodeStr = name.getIdentifier();
                            nodeString = name.getIdentifier();
                            //names.put("function", nodeStr);
                    }
                    break;
            case Token.FUNCTION:
                    FunctionNode fn = (FunctionNode) an;
                    //String nodeStr = (String) names.get("function");
                    if(fn.getFunctionName() != null)
                    {
                        //nodeStr = fn.getFunctionName().getIdentifier();
                        nodeString = fn.getFunctionName().getIdentifier();
                        System.out.println(nodeString);
                    }
                    AstNode body = fn.getBody();
                    Block block = (Block) body;
                    final String nodeStrFinal = nodeString;
                    AstNode newNode = new AstNode() {
                        @Override
                        public String toSource(int i) {
                            return "  console.log('" + nodeStrFinal + "');\n";
                        }

                        @Override
                        public void visit(NodeVisitor nv) {
                        }
                    };
                    block.addChildToFront(newNode);
                    break;
            default:
                    return;
	}
        
    }
    
    
}
