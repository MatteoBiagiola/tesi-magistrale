/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rhinoparser;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mozilla.javascript.CompilerEnvirons;
import org.mozilla.javascript.IRFactory;
import org.mozilla.javascript.ast.AstRoot;

/**
 *
 * @author matteo
 */
public class RhinoParser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String filePath = "resources/input.js";
        RhinoParser rp = new RhinoParser();
        try 
        {
            rp.parseJS(filePath);
        } catch (Exception ex) {
            Logger.getLogger(RhinoParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }
    
    public void parseJS (String filePath) throws Exception
    {
            CompilerEnvirons env = new CompilerEnvirons();
            env.setRecoverFromErrors(true);

            FileReader strReader = new FileReader(filePath);
            FileWriter strWriter = new FileWriter("resources/output.js");

            IRFactory factory = new IRFactory(env);
            //third argument: line number of the error if it occurs?
            AstRoot rootNode = factory.parse(strReader, null, 0);

            rootNode.visitAll(new JSNodeVisitor());
            
            System.out.println(rootNode.toSource());
            
            strWriter.write(rootNode.toSource());
            strWriter.close();
    }	
    
}
