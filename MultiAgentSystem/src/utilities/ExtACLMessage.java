/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import jade.lang.acl.ACLMessage;

/**
 *
 * @author matteo
 */
public class ExtACLMessage extends ACLMessage{
    
    public ExtACLMessage()
    {
        super();
    }
    
    public ExtACLMessage(int performative)
    {
        super(performative);
    }
    
    public static final int ANNOUNCE = 22;
    public static final int PREBID = 23;
    public static final int PREACCEPT = 24;
    public static final int PREREJECT = 25;
    public static final int DEFINITIVEBID = 26;
    public static final int DEFINITIVEACCEPT = 27;
    public static final int DEFINITIVEREJECT = 28;
    
}
