/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

import jade.content.AgentAction;
import jade.util.leap.ArrayList;

/**
 *
 * @author matteo
 */
public class Recognize implements AgentAction{
    private ArrayList cuttedFaces;
    private Slot slot;

    public ArrayList getCuttedFaces() {
        return cuttedFaces;
    }

    public void setCuttedFaces(ArrayList cuttedFaces) {
        this.cuttedFaces = cuttedFaces;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }
}
