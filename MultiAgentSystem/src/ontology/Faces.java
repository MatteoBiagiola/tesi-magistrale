/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.AgentActionSchema;
import jade.content.schema.ConceptSchema;
import jade.content.schema.ObjectSchema;
import jade.content.schema.PredicateSchema;
import jade.content.schema.PrimitiveSchema;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matteo
 */
public class Faces extends Ontology{
    
    public static final String ONT_NAME = "Faces";
    
    public static final String DETECT = "Detect";
    public static final String RECOGNIZE = "Recognize";
    
    public static final String SLOT = "Slot";
    public static final String START = "start";
    public static final String PERIOD = "period";
    
    public static final String FRAME = "Frame";
    public static final String FRAME_IMAGE = "frame_image";
    public static final String FRAME_NUMBER = "frame_number";
    
    public static final String CUTTED_FACE = "CuttedFace";
    public static final String CUT_FACE = "cutface";
    public static final String FRAME_NUM = "frame_num";
    public static final String FACE_NUM = "face_num";
    public static final String SIZE = "size";
    public static final String PREDICT_LABEL = "predictlabel";
    public static final String CONFIDENCE = "confidence";
    
    public static final String CUTTED_FACES = "cuttedFaces";
    
    
    private static Ontology instance = new Faces();
    
    private Faces()
    {
        super(ONT_NAME, BasicOntology.getInstance());
        try 
        {
            this.add(new PredicateSchema(SLOT),Slot.class);
            this.add(new ConceptSchema(FRAME),Frame.class);
            this.add(new ConceptSchema(CUTTED_FACE),CuttedFace.class);
            this.add(new AgentActionSchema(DETECT),Detect.class);
            this.add(new AgentActionSchema(RECOGNIZE),Recognize.class);
            
            PredicateSchema slot = (PredicateSchema) this.getSchema(SLOT);
            slot.add(START, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER));
            slot.add(PERIOD, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER));
            
            ConceptSchema frame = (ConceptSchema) this.getSchema(FRAME);
            frame.add(FRAME_IMAGE, (PrimitiveSchema) this.getSchema(BasicOntology.BYTE_SEQUENCE));
            frame.add(FRAME_NUMBER, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER));
            
            ConceptSchema cutface = (ConceptSchema) this.getSchema(CUTTED_FACE);
            cutface.add(CUT_FACE, (PrimitiveSchema) this.getSchema(BasicOntology.BYTE_SEQUENCE));
            cutface.add(FRAME_NUM, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER));
            cutface.add(FACE_NUM, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER));
            cutface.add(SIZE, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER));
            cutface.add(PREDICT_LABEL, (PrimitiveSchema) this.getSchema(BasicOntology.INTEGER), ObjectSchema.OPTIONAL);
            cutface.add(CONFIDENCE, (PrimitiveSchema) this.getSchema(BasicOntology.FLOAT), ObjectSchema.OPTIONAL);
            
            AgentActionSchema detection = (AgentActionSchema) this.getSchema(DETECT);
            detection.add(FRAME,frame);
            detection.add(SLOT,slot);
            
            AgentActionSchema recognition = (AgentActionSchema) this.getSchema(RECOGNIZE);
            recognition.add(CUTTED_FACES,(ConceptSchema) this.getSchema(CUTTED_FACE),0,ObjectSchema.UNLIMITED);
            recognition.add(SLOT,slot);
            
        } catch (OntologyException ex) {
            Logger.getLogger(Faces.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Ontology getInstance(){
	return instance;
    }
}
