/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

import jade.content.Concept;

/**
 *
 * @author matteo
 */
public class CuttedFace implements Concept{
    private byte[] cutface;
    private int frame_num;
    private int face_num;
    private int size;
    private int predictlabel;
    private double confidence;

    public byte[] getCutface() {
        return cutface;
    }

    public void setCutface(byte[] cutface) {
        this.cutface = cutface;
    }

    public int getFrame_num() {
        return frame_num;
    }

    public void setFrame_num(int frame_num) {
        this.frame_num = frame_num;
    }

    public int getFace_num() {
        return face_num;
    }

    public void setFace_num(int face_num) {
        this.face_num = face_num;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public int getPredictlabel() {
        return predictlabel;
    }

    public void setPredictlabel(int predictlabel) {
        this.predictlabel = predictlabel;
    }
    
}
