/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

/**
 *
 * @author matteo
 */
import jade.content.Concept;

public class Frame implements Concept{
	
    private int frame_number;
    private byte[] frame_image;

    public byte[] getFrame_image() {
        return frame_image;
    }

    public void setFrame_image(byte[] frame_image) {
        this.frame_image = frame_image;
    }

    public int getFrame_number() {
        return frame_number;
    }

    public void setFrame_number(int frame_number) {
        this.frame_number = frame_number;
    }
}
