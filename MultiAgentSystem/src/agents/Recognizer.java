/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.ExampleFSMBehaviour;
import behaviours.MessageReceiver;
import behaviours.PreRecognizeFaces;
import behaviours.ReceiveMessageBehaviour;
import behaviours.SimulatePreRecognizeFaces;
import behaviours.SimulateRecognizeFaces;
import jade.content.lang.leap.LEAPCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.ThreadedBehaviourFactory;
import jade.core.behaviours.TickerBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.states.MsgReceiver;
import jade.util.leap.ArrayList;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Faces;
import org.opencv.core.Core;
import org.opencv.face.Face;
import org.opencv.face.FaceRecognizer;

/**
 *
 * @author matteo
 */
public class Recognizer extends Agent{
    static{
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    private PrintWriter writer = null;
    
    protected void setup()
    {
        Behaviour b = new ExampleFSMBehaviour(this);
        this.addBehaviour(b);
        /*Object[] args = {this.getAID()};
        ContainerController cc = this.getContainerController();
        AgentController extractor;
        Behaviour b = new MessageReceiver(this,null,5000,null,null);
        try
        {
            extractor = cc.createNewAgent("Detector","agents.Detector",args);
            extractor.start();
            this.addBehaviour(b);
        }catch (StaleProxyException ex) {
            Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        /*Object[] args = {this.getAID()};
        ContainerController cc = this.getContainerController();
        AgentController extractor,detector;
        try 
        {
            extractor = cc.createNewAgent("Extractor","agents.Extractor",args);
            detector = cc.createNewAgent("Detector","agents.Detector",args);
            extractor.start();
            detector.start();
            ArrayList messages = new ArrayList();
            DataStore ds = new DataStore();
            ds.put("Vector",messages);
            Behaviour b1 = new CyclicBehaviour(this) {
                @Override
                public void action() {
                    ACLMessage msg = this.myAgent.receive();
                    if(msg != null)
                    {
                        ArrayList v = (ArrayList) this.getDataStore().get("Vector");
                        v.add(msg);
                        this.getDataStore().put("Vector",v);
                    }
                    else
                    {
                        this.block();
                    }
                }
            };
            b1.setDataStore(ds);
            Behaviour b2 = new TickerBehaviour(this,2000) {
                @Override
                protected void onTick() {
                    ArrayList v = (ArrayList) this.getDataStore().get("Vector");
                    System.out.println(this.myAgent.getLocalName() + ": messages queue " + v.size());
                }
            };
            b2.setDataStore(ds);
            this.addBehaviour(b1);
            this.addBehaviour(b2);*/
            
            /*ContainerController cc = this.getContainerController();
            Object[] in = {0};
            Object[] part = {1};
            AgentController extractorIn,extractorPart,detectorIn,detectorPart;
            try
            {
            extractorIn = cc.createNewAgent("ExtractorIn","agents.Extractor",in);
            extractorPart = cc.createNewAgent("ExtractorPart","agents.Extractor",part);
            detectorIn = cc.createNewAgent("DetectorIn","agents.Detector",in);
            detectorPart = cc.createNewAgent("DetectorPart","agents.Detector",part);
            extractorIn.start();
            extractorPart.start();
            detectorIn.start();
            detectorPart.start();
            } catch (StaleProxyException ex) {
            Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            
            /*String containerName = "";
            try {
            containerName = this.getContainerController().getContainerName();
            } catch (ControllerException ex) {
            Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
            }
            String args[] = {containerName};
            FaceRecognizer fr = Face.createFisherFaceRecognizer();
            File fileTraining = new File("resources/input/trainingFisher.xml");
            if(fileTraining.exists())
            {
            System.out.print(this.getLocalName() + ": loading training file...");
            fr.load(fileTraining.getPath());
            System.out.println("done");
            }
            System.out.print(this.getLocalName() + ": creating agents Extractor and Detector...");
            ContainerController cc = this.getContainerController();
            AgentController extractor,detector;
            try
            {
            String suffix = "";
            if(!containerName.contains("Main"))
            {
            suffix = containerName.substring(containerName.length() - 1);
            }
            extractor = cc.createNewAgent("Extractor" + suffix,"agents.Extractor", args);
            detector = cc.createNewAgent("Detector" + suffix,"agents.Detector", args);
            extractor.start();
            detector.start();
            } catch (StaleProxyException ex) {
            Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("done");
            System.out.println();
            String rightEyeClassificator = "resources/input/haarcascade_righteye_2splits.xml";
            String leftEyeClassificator = "resources/input/haarcascade_lefteye_2splits.xml";
            DFAgentDescription df = new DFAgentDescription();
            df.setName(this.getAID());
            ServiceDescription sd = new ServiceDescription();
            sd.setName(this.getLocalName() + "-local-service");
            sd.setType("Local-recognition " + containerName);
            df.addServices(sd);
            
            sd = new ServiceDescription();
            sd.setName(this.getLocalName() + "-remote-service");
            sd.setType("Remote-recognition");
            df.addServices(sd);
            
            LEAPCodec codec = new LEAPCodec();
            Ontology ont = Faces.getInstance();
            this.getContentManager().registerLanguage(codec);
            this.getContentManager().registerOntology(ont);
            
            try
            {
            this.setWriter(new PrintWriter("resources/output/results.txt","UTF-8"));
            } catch (FileNotFoundException ex) {
            Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            this.addBehaviour(new PreRecognizeFaces(this,fr,this.getWriter(),
            rightEyeClassificator,leftEyeClassificator));
            
            try
            {
            DFService.register(this, df);
            System.out.println("Local-recognition and remote-recognition services registered");
            }
            catch(FIPAException e)
            {
            // TODO Auto-generated catch block
            e.printStackTrace();
            }*/
    }
    
    protected void takeDown()
    {
        /*try 
        {
            DFService.deregister(this);
            System.out.println("Local-recognition and remote-recognition service unregistered");
            this.getWriter().close();
        } catch (FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }
}
