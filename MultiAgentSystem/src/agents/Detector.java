/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.SearchExtractorAndLocalRecognizer;
import behaviours.SimulateRecognizeFaces;
import behaviours.SimulateSearchExtractorRecognizer;
import jade.content.lang.leap.LEAPCodec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Faces;
import org.opencv.core.Core;
import utilities.ExtACLMessage;

/**
 *
 * @author matteo
 */
public class Detector extends Agent{
    static{
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    protected void setup()
    {
        
        AID recognizer = (AID) this.getArguments()[0];
        this.addBehaviour(new WakerBehaviour(this,6000) {
            @Override
            public void onWake() {
                ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
                msg.addReceiver(recognizer);
                this.myAgent.send(msg);
                System.out.println(this.myAgent.getLocalName() + ": message sent");
            }
        });
        /*Integer type = 0;
        if(this.getArguments() != null)
        {
            type = (Integer) this.getArguments()[0];
        }
        if(type.intValue() == 0)
        {
            //initiator
            Random random = new Random();
            int numFacesToSend = random.nextInt(3);
            DFAgentDescription ad = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("Remote-recognition");
            ad.addServices(sd);
            this.addBehaviour(new TickerBehaviour(this,1000) {
                @Override
                protected void onTick() {
                    try 
                    {
                        DFAgentDescription[] remoteAgents = DFService.search(this.myAgent,ad);
                        ExtACLMessage remote = new ExtACLMessage(ExtACLMessage.ANNOUNCE);
                        if(remoteAgents.length > 0)
                        {
                            for(int i = 0; i < remoteAgents.length; i++)
                            {
                                AID remoteAgent = remoteAgents[i].getName();
                                remote.addReceiver(remoteAgent);
                            }
                            this.stop();
                        }
                    } catch (FIPAException ex) {
                        Logger.getLogger(Detector.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        }
        else
        {
            //partecipant
            DFAgentDescription ad = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType("Remote-recognition");
            ad.addServices(sd);
            try {
                DFService.register(this, ad);
            } catch (FIPAException ex) {
                Logger.getLogger(Detector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/
        /*final String containerName = (String) this.getArguments()[0];
        DFAgentDescription df = new DFAgentDescription();
        df.setName(this.getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setName(this.getLocalName() + "-local-service");
        sd.setType("Detection " + containerName);
        LEAPCodec codec = new LEAPCodec();
        Ontology ont = Faces.getInstance();
        this.getContentManager().registerLanguage(codec);
        this.getContentManager().registerOntology(ont);
        this.addBehaviour(new WakerBehaviour(this,10000){
            @Override
            public void onWake()
            {
                this.myAgent.addBehaviour(new SearchExtractorAndLocalRecognizer(this.myAgent,1000,containerName));
            }
        });
        df.addServices(sd);
        try {
            DFService.register(this,df);
        } catch (FIPAException ex) {
            Logger.getLogger(Detector.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Local-detection service registered");*/
    }
    
    protected void takeDown()
    {
        /*try 
        {
            DFService.deregister(this);
            System.out.println("Local-detection service unregistered");
        } catch (FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
    }
}
