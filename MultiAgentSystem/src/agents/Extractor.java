/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.ExtractFrames;
import behaviours.SimulateExtractFrames;
import jade.content.lang.leap.LEAPCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Faces;
import org.opencv.core.Core;
import org.opencv.videoio.VideoCapture;

/**
 *
 * @author matteo
 */
public class Extractor extends Agent{
    static{
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    protected void setup()
    {
        this.addBehaviour(new OneShotBehaviour(this) {
            @Override
            public void action() {
                try {
                    Thread.sleep(20000);
                    System.out.println(this.myAgent.getLocalName() + ": sleep finished");
                } catch (InterruptedException ex) {
                    Logger.getLogger(Extractor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        /*Integer type = 0;
        if(this.getArguments() != null)
        {
            type = (Integer) this.getArguments()[0];
        }
        if(type.intValue() == 0)
        {
            //initiator
            System.out.println(this.getLocalName() + ": initiator");
        }
        else
        {
            //partecipant
            System.out.println(this.getLocalName() + ": partecipant");
        }*/
        /*String containerName = "";
        if(this.getArguments() != null)
        {
            containerName = (String) this.getArguments()[0];
        }
        DFAgentDescription df = new DFAgentDescription();
        df.setName(this.getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setName(this.getLocalName() + "-local-service");
        sd.setType("Extraction " + containerName);
        
        LEAPCodec codec = new LEAPCodec();
        Ontology ont = Faces.getInstance();
        this.getContentManager().registerLanguage(codec);
        this.getContentManager().registerOntology(ont);
        
        this.addBehaviour(new WakerBehaviour(this,10000){
                @Override
                public void onWake()
                {
                    File videoFile = new File("resources/input/video/");
                    if(videoFile.listFiles()[0].getPath().contains(".DS_Store"))
                    {
                        videoFile.listFiles()[0].delete();
                    }
                    String videoName = videoFile.listFiles()[0].getPath();
                    this.myAgent.addBehaviour(new ExtractFrames(this.myAgent,
                            new VideoCapture(videoName)));
                }
        });
        df.addServices(sd);
        try 
        {
            DFService.register(this, df);
            System.out.println("Local-extraction service registered");
	}catch(FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
	}*/
    }
    
    protected void takeDown()
    {
        try 
        {
            DFService.deregister(this);
            System.out.println("Local-extraction service unregistered");
        } catch (FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
