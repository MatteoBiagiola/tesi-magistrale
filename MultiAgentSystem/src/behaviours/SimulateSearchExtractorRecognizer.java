/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

/**
 *
 * @author matteo
 */
public class SimulateSearchExtractorRecognizer extends TickerBehaviour{

    
    public SimulateSearchExtractorRecognizer(Agent a, long period) {
        super(a, period);
    }

    @Override
    protected void onTick() {
        DFAgentDescription ad = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Extraction");
        ad.addServices(sd);
        try 
        {
            DFAgentDescription[] extractor = DFService.search(this.myAgent, ad);
            sd.setType("Local-recognition");
            ad.addServices(sd);
            DFAgentDescription[] localRecognizer = DFService.search(this.myAgent, ad);
            if(extractor.length > 0 && localRecognizer.length > 0)
            {
                this.myAgent.addBehaviour(new SimulateDetectFaces(this.myAgent,localRecognizer[0].getName()));
                this.myAgent.addBehaviour(new SimulateSendFrameRequest(this.myAgent,extractor[0].getName()));
                this.stop();
            }
            else
            {
                System.out.println(this.myAgent.getLocalName() + ": no extractor found");
            }

        }catch(FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
}
