/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Detect;
import ontology.Frame;
import ontology.Slot;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

/**
 *
 * @author matteo
 */
public class ExtractFrames extends CyclicBehaviour{
    
    private VideoCapture vd;
    private double timestamp;
    private long previousTime;
    private long currentTime;
    private boolean extract;
    
    public ExtractFrames(Agent a, VideoCapture vd) {
        super(a);
        this.setVd(vd);
        this.setPreviousTime(0);
        this.setCurrentTime(0);
    }
    
    public void action(){
        ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchConversationId("local"));
        this.setExtract(false);
        if(msg != null && (msg.getPerformative() == ACLMessage.REQUEST))
        {
            try 
            {
                Slot slot = (Slot) this.myAgent.getContentManager().extractContent(msg);
                this.setCurrentTime(slot.getStart());
                Mat frame =  new Mat();
                if(this.getPreviousTime() != 0)
                {
                    this.setTimestamp(this.getVd().get(Videoio.CAP_PROP_POS_MSEC));
                    this.getVd().set(Videoio.CAP_PROP_POS_MSEC,this.getTimestamp() + 
                        (this.getCurrentTime() - this.getPreviousTime()));
                }
                this.setExtract(this.getVd().read(frame));
                if(this.isExtract())
                {
                    this.setPreviousTime(this.getCurrentTime());
                    Mat gray = new Mat();
                    Imgproc.cvtColor(this.shrinkFrame(frame,320),gray,Imgproc.COLOR_BGR2GRAY);
                    MatOfByte byteimage = new MatOfByte();
                    Imgcodecs.imencode(".pgm",gray,byteimage);
                    Imgcodecs.imwrite(String.format("resources/output/frames/frame%05d.pgm",
                            (int) this.getVd().get(Videoio.CAP_PROP_POS_FRAMES)),gray);

                    Frame frameClass = new Frame();
                    frameClass.setFrame_number((int) this.getVd().get(Videoio.CAP_PROP_POS_FRAMES));
                    frameClass.setFrame_image(byteimage.toArray());

                    ACLMessage reply = msg.createReply();
                    reply.setConversationId("local");
                    reply.setPerformative(ACLMessage.REQUEST);
                    reply.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
                    reply.setOntology(this.myAgent.getContentManager().getOntologyNames()[0]);

                    Detect detect = new Detect();
                    detect.setFrame(frameClass);
                    detect.setSlot(slot);

                    this.myAgent.getContentManager().fillContent(reply, detect);
                    this.myAgent.send(reply);
                }
                else
                {
                    System.out.println(this.myAgent.getLocalName() + ": video ended");
                    /*ACLMessage cancel = new ACLMessage(ACLMessage.CANCEL);
                    cancel.addReceiver(msg.getSender());
                    cancel.setContent("stop");
                    this.myAgent.send(cancel);
                    this.myAgent.doDelete();*/
                    this.getVd().release();
                }
            } catch (Codec.CodecException ex) {
                Logger.getLogger(ExtractFrames.class.getName()).log(Level.SEVERE, null, ex);
            } catch (OntologyException ex) {
                Logger.getLogger(ExtractFrames.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            this.block();
        }
    }
    
    private Mat shrinkFrame(Mat image, int detection_width)
    {
        Mat smallImage = new Mat();
        int scaledHeight = 0;
        Size rescale;
        float scale = image.cols()/(float) detection_width;
        if(image.cols() > detection_width)
        {
            scaledHeight = Math.round(image.rows()/scale);
            rescale = new Size(detection_width,scaledHeight);
            Imgproc.resize(image,smallImage,rescale);
        }
        else
        {
            smallImage = image;
        }
        return smallImage;
    }

    public VideoCapture getVd() {
        return vd;
    }

    public void setVd(VideoCapture vd) {
        this.vd = vd;
    }

    public double getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(double timestamp) {
        this.timestamp = timestamp;
    }

    public long getPreviousTime() {
        return previousTime;
    }

    public void setPreviousTime(long previousTime) {
        this.previousTime = previousTime;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }
    
    public boolean isExtract() {
        return extract;
    }

    public void setExtract(boolean extract) {
        this.extract = extract;
    }
}
