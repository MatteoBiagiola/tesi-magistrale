/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matteo
 */
public class SimulateRecognizeFaces extends SimpleBehaviour{

    private ArrayList v = new ArrayList();
    private boolean done = false;
    private Random random = new Random();
    private int sleep = 0;
    
    public SimulateRecognizeFaces(Agent a)
    {
        super(a);
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchConversationId("remote"));
        if(msg != null)
        {
            this.getV().add(msg);
        }
        else
        {
            this.sleep = this.random.nextInt(5000);
            try 
            {
                Thread.sleep(this.sleep);
                System.out.println(this.myAgent.getLocalName() + ": recognition finished");
                this.done = true;
            } catch (InterruptedException ex) {
                Logger.getLogger(SimulateRecognizeFaces.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public boolean done() {
        return this.done;
    }
    
    public ArrayList getV() {
        return v;
    }

    public void setV(ArrayList v) {
        this.v = v;
    }
    
}
