/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.LifeCycle;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentState;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Slot;

/**
 *
 * @author matteo
 */
public class SimulateSendFrameRequest extends CyclicBehaviour{

    private AID extractor;
    
    public SimulateSendFrameRequest(Agent a, AID extractor) {
        super(a);
        this.extractor = extractor;
        this.sendMessage();
    }

    @Override
    public void action() {
        //agent receives message from recognizer that informs him finishing of recognition
        //agent detector has to propagate this information to extractor asking for a new frame
        ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.PROPAGATE));
        if(msg != null)
        {
            this.sendMessage();
        }
        else
        {
            this.block();
        }
    }
    
    public void sendMessage()
    {
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.addReceiver(this.extractor);
        msg.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
        msg.setOntology(this.myAgent.getContentManager().getOntologyNames()[0]);
        
        Slot slot = new Slot();
        slot.setStart(System.currentTimeMillis()); 
        slot.setPeriod(0);
        
        try {
            this.myAgent.getContentManager().fillContent(msg,slot);
            this.myAgent.send(msg);
            System.out.println(this.myAgent.getLocalName() + ": frame request sent");
        } catch (Codec.CodecException ex) {
            Logger.getLogger(SendFrameRequest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(SendFrameRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
