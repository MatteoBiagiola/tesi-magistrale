/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

/**
 *
 * @author matteo
 */
public class SearchExtractorAndLocalRecognizer extends TickerBehaviour{
    
    private String containerName;
    
    public SearchExtractorAndLocalRecognizer(Agent a, long period, String containerName) {
        super(a, period);
        this.setContainerName(containerName);
    }

    @Override
    protected void onTick()
    {
        DFAgentDescription ad = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Extraction " + this.getContainerName());
        ad.addServices(sd);
        try 
        {
            DFAgentDescription[] extractor = DFService.search(this.myAgent, ad);
            sd.setType("Local-recognition " + this.getContainerName());
            ad.addServices(sd);
            DFAgentDescription[] localRecognizer = DFService.search(this.myAgent, ad);
            if(extractor.length > 0 && localRecognizer.length > 0)
            {
                this.myAgent.addBehaviour(new DetectFaces(this.myAgent,localRecognizer[0].getName()));
                this.myAgent.addBehaviour(new SendFrameRequest(this.myAgent,3000,
                        extractor[0].getName(),localRecognizer[0].getName()));
                this.stop();
            }
            else
            {
                System.out.println(this.myAgent.getLocalName() + ": no extractor found");
            }

        }catch(FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }
}
