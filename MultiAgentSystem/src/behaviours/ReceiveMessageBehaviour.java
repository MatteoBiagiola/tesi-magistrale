/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 *
 * @author matteo
 */
public abstract class ReceiveMessageBehaviour extends SimpleBehaviour{
    
    private boolean done;
    private long deadline;
    private long count;
    private long start;
    private MessageTemplate mt;
    
    public ReceiveMessageBehaviour(Agent a, long deadline, long start, MessageTemplate mt)
    {
        super(a);
        this.setDone(false);
        this.setDeadline(deadline);
        this.setStart(start);
        this.setCount(0);
        this.setMt(mt);
    }

    @Override
    public void action() {
        ACLMessage msg;
        if(this.getMt() == null)
        {
            msg = this.myAgent.receive();
        }
        else
        {
            msg = this.myAgent.receive(this.getMt());
        }
        
        if(msg != null)
        {
            this.handleReceive(msg);
        }
        else if(this.getDeadline() - this.getCount() >= 0)
        {
            this.setCount(System.currentTimeMillis() - this.getStart());
        }
        else
        {
            this.setDone(true);
            this.handleTimeout();
        }
    }

    abstract void handleReceive(ACLMessage msg);
    
    abstract void handleTimeout();
    
    @Override
    public boolean done() {
        return this.isDone();
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public long getDeadline() {
        return deadline;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public MessageTemplate getMt() {
        return mt;
    }

    public void setMt(MessageTemplate mt) {
        this.mt = mt;
    }
    
}
