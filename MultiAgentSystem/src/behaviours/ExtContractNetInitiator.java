/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.FSMBehaviour;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author matteo
 */
public class ExtContractNetInitiator extends FSMBehaviour{

    protected static final String ANNOUNCE = "Announce";
    protected static final String EFF_COUNTER = "Eff-counter";
    protected static final String TEMP_COUNTER = "Temp-counter";
    // FSM states names
    protected static final String INITIATION = "Initiation";
    protected static final String HANDLE_PRE_BID = "Handle-pre-bid";
    protected static final String HANDLE_DEFINITIVE_BID = "Handle-definitive-bid";
    protected static final String HANDLE_ALL_RESULT_NOTIFICATIONS = "Handle-all-result-notifications";
    
    public ExtContractNetInitiator(Agent a,ACLMessage announce,int numFacesToSend)
    {
        super(a);
        DataStore store = new DataStore();
        store.put(ANNOUNCE,announce);
        store.put(EFF_COUNTER,numFacesToSend);
        store.put(TEMP_COUNTER,numFacesToSend);
        this.setDataStore(store);
        
        // Register the FSM transitions specific to the Extended ContractNet protocol
        this.registerTransition(INITIATION,HANDLE_PRE_BID,0);
    }
}
