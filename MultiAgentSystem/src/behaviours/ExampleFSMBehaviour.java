/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import java.util.Random;

/**
 *
 * @author matteo
 */
public class ExampleFSMBehaviour extends FSMBehaviour{
    
    private static final String VARIABLE = "Variable";
    //FSM states
    protected static final String X = "x";
    protected static final String Y = "y";
    protected static final String Z = "z";
    
    public ExampleFSMBehaviour(Agent a)
    {
        super(a);
        DataStore store = new DataStore();
        int prova = 0;
        store.put(VARIABLE,prova);
        this.setDataStore(store);
        this.registerTransition(X,Y,1);
        this.registerTransition(X,Z,0);
        this.registerDefaultTransition(Z,X,new String[]{X,Z});
        
        Behaviour b = null;
        
        b = new OneShotBehaviour(this.myAgent) {
            private int success = 0;
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + X);
                Random random = new Random();
                int result = random.nextInt(2);
                if(result == 1)
                {
                    this.success = 1;
                    int prova = (int) this.getDataStore().get(VARIABLE);
                    prova = 10;
                    this.getDataStore().put(VARIABLE,prova);
                }
            }
            public int onEnd()
            {
                return this.success;
            }
        };
        b.setDataStore(store);
        this.registerFirstState(b,X);
        
        b = new OneShotBehaviour(this.myAgent) {
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + Y +
                        " the value of modified variable is " + this.getDataStore().get(VARIABLE));
            }
        };
        b.setDataStore(store);
        this.registerLastState(b,Y);
        
        b = new OneShotBehaviour(this.myAgent){
            @Override
            public void action() {
                System.out.println(this.myAgent.getLocalName() + ": state " + Z +
                        " the value of modified variable is " + this.getDataStore().get(VARIABLE));
            }
        };
        b.setDataStore(store);
        this.registerState(b,Z);
    }
    
}
