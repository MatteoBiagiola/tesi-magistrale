/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import jade.util.leap.ArrayList;
import java.io.PrintWriter;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Recognize;
import org.opencv.face.FaceRecognizer;

/**
 *
 * @author matteo
 */
public class SendRemoteRecognitionRequest extends SimpleBehaviour{

    private boolean done;
    private long deadline;
    private long count;
    private long start;
    private FaceRecognizer fr;
    private PrintWriter writer;
    private long time_left;
    private long recognition_time;
    private ArrayList toKeep;
    private int numToSend;
    private int propose;
    private Vector responses;
    private Recognize recognize;
    
    public SendRemoteRecognitionRequest(Agent a,ACLMessage cfp,FaceRecognizer fr,PrintWriter writer,
            long time_left, long recognition_time, ArrayList toKeep, int numToSend, long deadline,
            long start, Recognize recognize) {
        super(a);
        this.myAgent.send(cfp);
        this.setFr(fr);
        this.setWriter(writer);
        this.setTime_left(time_left);
        this.setRecognition_time(recognition_time);
        this.setToKeep(toKeep);
        this.setNumToSend(numToSend);
        this.setPropose(0);
        this.setDone(false);
        this.setDeadline(deadline);
        this.setStart(start);
        this.setCount(0);
        this.setResponses(new Vector());
        this.setRecognize(recognize);
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchConversationId("remote"));
        if(msg != null && (msg.getPerformative() == ACLMessage.PROPOSE || 
                msg.getPerformative() == ACLMessage.REFUSE))
        {
            System.out.println(this.myAgent.getLocalName() + ": propose or refuse received");
            this.getResponses().add(msg);
        }
        else if(this.getDeadline() - this.getCount() >= 0)
        {
            this.setCount(System.currentTimeMillis() - this.getStart());
        }
        else
        {
            this.setDone(true);
            if(this.getResponses().size() != 0)
            {
                boolean first = false;
                for(Object o: this.getResponses())
                {
                    ACLMessage propose = (ACLMessage) o;
                    ACLMessage reply = propose.createReply();
                    reply.setConversationId("remote");
                    if(propose.getPerformative() == ACLMessage.PROPOSE)
                    {
                        System.out.println(this.myAgent.getLocalName() + ": handle propose");
                        try {
                            this.setPropose((int) propose.getContentObject());
                        } catch (UnreadableException ex) {
                            Logger.getLogger(SendRemoteRecognitionRequest.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if(this.getPropose() > this.getNumToSend() && !first)
                        {
                            reply.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
                            first = true;
                            reply.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
                            reply.setOntology(this.myAgent.getContentManager().getOntologyNames()[0]);
                            try {
                                this.myAgent.getContentManager().fillContent(reply,this.getRecognize());
                                this.myAgent.send(reply);
                            } catch (Codec.CodecException ex) {
                                Logger.getLogger(SendRemoteRecognitionRequest.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (OntologyException ex) {
                                Logger.getLogger(SendRemoteRecognitionRequest.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        else
                        {
                            reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
                            this.myAgent.send(reply);
                        }
                    }
                    else
                    {
                        System.out.println(this.myAgent.getLocalName() + ": handle refuse");
                    }
                }
                this.myAgent.addBehaviour(new CustomContractNetInitiator(this.myAgent,
                        this.getTime_left() - (System.currentTimeMillis() - this.getStart()),
                        System.currentTimeMillis(),this.getWriter()));
            }
            else
            {
                //Local recognition, faces that is possible to recognize according with time left
                System.out.println(this.myAgent.getLocalName() + ": timeout expired "
                        + "for receiving reply to cfp help request, local recognition according with time left");
                this.myAgent.addBehaviour(new RecognizeFaces(this.myAgent,this.getToKeep(),
                        this.getFr(),this.getWriter(),false));
            }
        }
    }

    @Override
    public boolean done() {
        return this.isDone();
    }

    public FaceRecognizer getFr() {
        return fr;
    }

    public void setFr(FaceRecognizer fr) {
        this.fr = fr;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }

    public long getTime_left() {
        return time_left;
    }

    public void setTime_left(long time_left) {
        this.time_left = time_left;
    }

    public long getRecognition_time() {
        return recognition_time;
    }

    public void setRecognition_time(long recognition_time) {
        this.recognition_time = recognition_time;
    }

    public ArrayList getToKeep() {
        return toKeep;
    }

    public void setToKeep(ArrayList toKeep) {
        this.toKeep = toKeep;
    }

    public int getNumToSend() {
        return numToSend;
    }

    public void setNumToSend(int numToSend) {
        this.numToSend = numToSend;
    }

    public int getPropose() {
        return propose;
    }

    public void setPropose(int propose) {
        this.propose = propose;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public long getDeadline() {
        return deadline;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public Vector getResponses() {
        return responses;
    }

    public void setResponses(Vector responses) {
        this.responses = responses;
    }

    public Recognize getRecognize() {
        return recognize;
    }

    public void setRecognize(Recognize recognize) {
        this.recognize = recognize;
    }
    
}
