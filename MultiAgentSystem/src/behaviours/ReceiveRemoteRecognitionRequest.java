/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Recognize;
import org.opencv.face.FaceRecognizer;

/**
 *
 * @author matteo
 */
public class ReceiveRemoteRecognitionRequest extends SimpleBehaviour{

    private boolean done;
    private long deadline;
    private long count;
    private long start;
    private FaceRecognizer fr;
    private PrintWriter writer;
    private ArrayList toRecognizeLocal;
    private int propose;
    
    public ReceiveRemoteRecognitionRequest(Agent a,long deadline,long start,
            FaceRecognizer fr,PrintWriter writer, ArrayList toRecognizeLocal, int propose)
    {
        super(a);
        this.setDone(false);
        this.setDeadline(deadline);
        this.setStart(start);
        this.setCount(0);
        this.setFr(fr);
        this.setWriter(writer);
        this.setToRecognizeLocal(toRecognizeLocal);
        this.setPropose(propose);
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchConversationId("remote"));
        if(msg != null && (msg.getPerformative() == ACLMessage.CFP))
        {
            System.out.println(this.myAgent.getLocalName() + ": receive cfp");
            try 
            {
                ACLMessage reply = new ACLMessage(ACLMessage.PROPOSE);
                reply.addReceiver(msg.getSender());
                reply.setConversationId("remote");
                reply.setContentObject(this.getPropose());
                this.myAgent.send(reply);
                System.out.println(this.myAgent.getLocalName() + ": send propose " 
                        + this.getPropose() + " faces");
            } catch (IOException ex) {
                Logger.getLogger(ReceiveRemoteRecognitionRequest.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.myAgent.addBehaviour(new CustomContractNetResponder(this.myAgent,
                    this.getToRecognizeLocal(),this.getFr(),this.getWriter()));
            this.setDone(true);
            //local and remote recognition
        }
        else if(this.getDeadline() - this.getCount() >= 0)
        {
            this.setCount(System.currentTimeMillis() - this.getStart());
        }
        else
        {
            System.out.print(this.myAgent.getLocalName() + ": timeout expired "
                    + "for receiving cfp help request");
            this.setDone(true);
            if(this.getToRecognizeLocal() != null)
            {
                //local recognition
                System.out.println(", local recognition");
                this.myAgent.addBehaviour(new RecognizeFaces(this.myAgent,this.getToRecognizeLocal(),
                        this.getFr(),this.getWriter(),false));
            }
            else
            {
                System.out.println();
            }
        }
    }

    @Override
    public boolean done() {
        return this.isDone();
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public long getDeadline() {
        return deadline;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public FaceRecognizer getFr() {
        return fr;
    }

    public void setFr(FaceRecognizer fr) {
        this.fr = fr;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }

    public int getPropose() {
        return propose;
    }

    public void setPropose(int propose) {
        this.propose = propose;
    }

    public ArrayList getToRecognizeLocal() {
        return toRecognizeLocal;
    }

    public void setToRecognizeLocal(ArrayList toRecognizeLocal) {
        this.toRecognizeLocal = toRecognizeLocal;
    }
}
