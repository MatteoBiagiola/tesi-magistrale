/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.CuttedFace;
import ontology.Recognize;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.face.FaceRecognizer;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author matteo
 */
public class RecognizeFaces extends OneShotBehaviour{

    private ArrayList cuttedFaces;
    private FaceRecognizer fr;
    private PrintWriter writer;
    private int[] predictlabel = {-1};
    private double[] confidence = {0.0};
    private boolean remote;
    private ACLMessage msg;
    private ArrayList toRecognizeLocal;
    
    public RecognizeFaces(Agent a,ArrayList cuttedFaces,
            FaceRecognizer fr,PrintWriter writer,boolean remote)
    {
        super(a);
        this.setCuttedFaces(cuttedFaces);
        this.setFr(fr);
        this.setWriter(writer);
        this.setRemote(remote);
    }
    
    public RecognizeFaces(Agent a,FaceRecognizer fr,PrintWriter writer,boolean remote,
            ACLMessage msg,ArrayList toRecognizeLocal)
    {
        super(a);
        this.setFr(fr);
        this.setWriter(writer);
        this.setRemote(remote);
        this.setMsg(msg);
        this.setToRecognizeLocal(toRecognizeLocal);
    }
    
    @Override
    public void action() 
    {
        if(!this.isRemote() && (this.getCuttedFaces() != null))
        {
            //recognize local faces and write results
            for(Object object: this.getCuttedFaces().toArray())
            {
                MatOfByte byteface = new MatOfByte();
                CuttedFace cutFace = (CuttedFace) object;
                byteface.fromArray(cutFace.getCutface());
                Mat face = Imgcodecs.imdecode(byteface, Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                Imgproc.resize(face,face,new Size(cutFace.getSize(),cutFace.getSize()));
                this.getFr().predict(face,this.getPredictlabel(),this.getConfidence());
                if(this.getWriter() != null)
                {
                    this.getWriter().println("Frame number: " + cutFace.getFrame_num() 
                            + " Face number: " + cutFace.getFace_num() 
                            + " Label: " + this.getPredictlabel()[0] 
                            + " Confidence: " + this.getConfidence()[0]);
                    this.getWriter().flush();
                }
            }
        }
        else
        {
            if(this.getToRecognizeLocal() != null)
            {
                //recognize local faces and write results
                for(Object object: this.getToRecognizeLocal().toArray())
                {
                    MatOfByte byteface = new MatOfByte();
                    CuttedFace cutFace = (CuttedFace) object;
                    byteface.fromArray(cutFace.getCutface());
                    Mat face = Imgcodecs.imdecode(byteface, Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                    Imgproc.resize(face,face,new Size(cutFace.getSize(),cutFace.getSize()));
                    this.getFr().predict(face,this.getPredictlabel(),this.getConfidence());
                    if(this.getWriter() != null && !(this.isRemote()))
                    {
                        this.getWriter().println("Frame number: " + cutFace.getFrame_num()
                                + " Face number: " + cutFace.getFace_num()
                                + " Label: " + this.getPredictlabel()[0]
                                + " Confidence: " + this.getConfidence()[0]);
                        this.getWriter().flush();
                    }
                }
            }
            try
            {
                //recognize remote faces and send results
                Recognize recognize = (Recognize) this.myAgent.getContentManager().extractContent(this.getMsg());
                ArrayList toSend = new ArrayList();
                ACLMessage reply = this.getMsg().createReply();
                for(Object object: recognize.getCuttedFaces().toArray())
                {
                    MatOfByte byteface = new MatOfByte();
                    CuttedFace cutFace = (CuttedFace) object;
                    byteface.fromArray(cutFace.getCutface());
                    Mat face = Imgcodecs.imdecode(byteface, Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                    Imgproc.resize(face,face,new Size(cutFace.getSize(),cutFace.getSize()));
                    this.getFr().predict(face,this.getPredictlabel(),this.getConfidence());
                    cutFace.setPredictlabel(this.getPredictlabel()[0]);
                    cutFace.setConfidence(this.getConfidence()[0]);
                    toSend.add(cutFace);
                }
                recognize.setCuttedFaces(toSend);
                reply.setPerformative(ACLMessage.INFORM);
                reply.setConversationId("remote");
                reply.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
                reply.setOntology(this.myAgent.getContentManager().getOntologyNames()[0]);
                this.myAgent.getContentManager().fillContent(reply,recognize);
                this.myAgent.send(reply);
                
            } catch (Codec.CodecException ex) {
                Logger.getLogger(RecognizeFaces.class.getName()).log(Level.SEVERE, null, ex);
            } catch (OntologyException ex) {
                Logger.getLogger(RecognizeFaces.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    } 

    public ArrayList getCuttedFaces() {
        return cuttedFaces;
    }

    public void setCuttedFaces(ArrayList cuttedFaces) {
        this.cuttedFaces = cuttedFaces;
    }

    public FaceRecognizer getFr() {
        return fr;
    }

    public void setFr(FaceRecognizer fr) {
        this.fr = fr;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }

    public int[] getPredictlabel() {
        return predictlabel;
    }

    public void setPredictlabel(int[] predictlabel) {
        this.predictlabel = predictlabel;
    }

    public double[] getConfidence() {
        return confidence;
    }

    public void setConfidence(double[] confidence) {
        this.confidence = confidence;
    }

    public boolean isRemote() {
        return remote;
    }

    public void setRemote(boolean remote) {
        this.remote = remote;
    }

    public ACLMessage getMsg() {
        return msg;
    }

    public void setMsg(ACLMessage msg) {
        this.msg = msg;
    }

    public ArrayList getToRecognizeLocal() {
        return toRecognizeLocal;
    }

    public void setToRecognizeLocal(ArrayList toRecognizeLocal) {
        this.toRecognizeLocal = toRecognizeLocal;
    }
}
