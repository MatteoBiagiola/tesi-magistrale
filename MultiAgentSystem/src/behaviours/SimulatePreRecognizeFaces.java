/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

/**
 *
 * @author matteo
 */
public class SimulatePreRecognizeFaces extends CyclicBehaviour{

    public SimulatePreRecognizeFaces(Agent a)
    {
        super(a);
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive();
        if(msg != null)
        {
            System.out.println(this.myAgent.getLocalName() + 
                    ": message received from detector, start recognition");
            //message received from detector; start recognition
            SimulateRecognizeFaces b = new SimulateRecognizeFaces(this.myAgent);
            this.myAgent.addBehaviour(b);
            System.out.println(this.myAgent.getLocalName() + 
                    ": incoming messages during recognition " + b.getV().size());
            //recognition finished; send message to detector
            ACLMessage reply = new ACLMessage(ACLMessage.PROPAGATE);
            reply.addReceiver(msg.getSender());
            this.myAgent.send(reply);
        }
        else
        {
            this.block();
        }
    }
    
}
