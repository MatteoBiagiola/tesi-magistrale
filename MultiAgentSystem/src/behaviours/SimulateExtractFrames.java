/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Slot;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

/**
 *
 * @author matteo
 */
public class SimulateExtractFrames extends CyclicBehaviour{

    private VideoCapture vd;
    private double timestamp;
    private long previousTime;
    private long currentTime;
    private boolean extract;
    
    public SimulateExtractFrames(Agent a,VideoCapture vd)
    {
        super(a);
        this.vd = vd;
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive();
        this.extract = false;
        if(msg != null && (msg.getPerformative() == ACLMessage.REQUEST))
        {
            System.out.println(this.myAgent.getLocalName() + 
                    ": frame request received, start extraction");
            try 
            {
                Slot slot = (Slot) this.myAgent.getContentManager().extractContent(msg);
                this.currentTime = slot.getStart();
                Mat frame =  new Mat();
                if(this.previousTime != 0)
                {
                    System.out.println(this.myAgent.getLocalName() + 
                            ": time elapsed since the last request " 
                            + (this.currentTime - this.previousTime) + " [ms]");
                    this.timestamp = this.vd.get(Videoio.CAP_PROP_POS_MSEC);
                    this.vd.set(Videoio.CAP_PROP_POS_MSEC,this.timestamp + 
                        (this.currentTime - this.previousTime));
                }
                this.extract = this.vd.read(frame);
                if(this.extract)
                {
                    this.previousTime = this.currentTime;
                    Mat gray = new Mat();
                    Imgproc.cvtColor(this.shrinkFrame(frame,320),gray,Imgproc.COLOR_BGR2GRAY);
                    MatOfByte byteimage = new MatOfByte();
                    Imgcodecs.imencode(".bmp",gray,byteimage);
                    Imgcodecs.imwrite(String.format("resources/output/frames/frame%05d.bmp",
                            (int) this.vd.get(Videoio.CAP_PROP_POS_FRAMES)),gray);
                    //send frame to detector
                    ACLMessage mess = new ACLMessage(ACLMessage.INFORM);
                    mess.addReceiver(msg.getSender());
                    this.myAgent.send(mess);
                    System.out.println(this.myAgent.getLocalName() + ": frame sent");
                }
            } catch (Codec.CodecException ex) {
                Logger.getLogger(SimulateExtractFrames.class.getName()).log(Level.SEVERE, null, ex);
            } catch (OntologyException ex) {
                Logger.getLogger(SimulateExtractFrames.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            this.block();
        }
    }
        
    private Mat shrinkFrame(Mat image, int detection_width)
    {
        Mat smallImage = new Mat();
        int scaledHeight = 0;
        Size rescale;
        float scale = image.cols()/(float) detection_width;
        if(image.cols() > detection_width)
        {
            scaledHeight = Math.round(image.rows()/scale);
            rescale = new Size(detection_width,scaledHeight);
            Imgproc.resize(image,smallImage,rescale);
        }
        else
        {
            smallImage = image;
        }
        return smallImage;
    }
    
}
