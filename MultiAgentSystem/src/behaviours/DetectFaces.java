/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.CuttedFace;
import ontology.Detect;
import ontology.Frame;
import ontology.Recognize;
import ontology.Slot;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class DetectFaces extends CyclicBehaviour{
    
    private CascadeClassifier faceDetector;
    private String classifier;
    private Mat grayEq = new Mat();
    private MatOfRect faceDetections = new MatOfRect();
    private int flags;
    private Size minFeaturesSize;
    private Size maxFeaturesSize;
    private float scaleFactor;
    private int minNeighbors;
    private int numfaces;
    private AID recognizer;
    
    public DetectFaces(Agent a, AID recognizer)
    {
        super(a);
        this.setNumfaces(0);
        this.setFlags(Objdetect.CASCADE_SCALE_IMAGE);
        this.setMinFeaturesSize(new Size(20,20));
        this.setMaxFeaturesSize(new Size());
        this.setScaleFactor((float) 1.2);
        this.setMinNeighbors(3);
        this.setClassifier("resources/input/lbpcascade_frontalface.xml");
        this.setFaceDetector(new CascadeClassifier(this.getClassifier()));
        this.setRecognizer(recognizer);
    }

    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchConversationId("local"));
        if(msg !=null && (msg.getPerformative() == ACLMessage.REQUEST))
        {
            try 
            {
                Detect detect = (Detect) this.myAgent.getContentManager().extractContent(msg);
                Frame frame = detect.getFrame();
                Slot slot = detect.getSlot();
                if(frame != null)
                {
                    MatOfByte byteframe = new MatOfByte();
                    byteframe.fromArray(frame.getFrame_image());
                    Mat image = Imgcodecs.imdecode(byteframe, Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                    Imgproc.equalizeHist(image,this.getGrayEq());
                    this.getFaceDetector().detectMultiScale(this.getGrayEq(),
                        this.getFaceDetections(),this.getScaleFactor(), 
                        this.getMinNeighbors(),this.getFlags(),this.getMinFeaturesSize(), 
                        this.getMaxFeaturesSize());
                    ArrayList cuttedFaces = new ArrayList();
                    Recognize recognize = new Recognize();
                    recognize.setSlot(slot);
                    this.setNumfaces(0);
                    for(Rect rect: this.getFaceDetections().toArray())
                    {
                        Rect r = new Rect(rect.x,rect.y,rect.width,rect.height);
                        Mat croppedFace = new Mat(image,r);
                        MatOfByte byteimage = new MatOfByte();
                        Imgcodecs.imencode(".pgm",croppedFace,byteimage);
                        CuttedFace cutFace = new CuttedFace();
                        cutFace.setSize(70);
                        cutFace.setCutface(byteimage.toArray());
                        cutFace.setFrame_num(frame.getFrame_number());
                        cutFace.setFace_num(this.getNumfaces());
                        cuttedFaces.add(cutFace);
                        String faceName = String.format("resources/output/faces/frame%05d",frame.getFrame_number()) +
                                String.format("-%02d.pgm",this.getNumfaces());
                        Imgcodecs.imwrite(faceName,croppedFace);
                        this.setNumfaces(this.getNumfaces() + 1);
                    }
                    recognize.setCuttedFaces(cuttedFaces);
                    System.out.println(this.myAgent.getLocalName() + ": extraction-detection " + 
                            (System.currentTimeMillis() - slot.getStart()) + " [ms]");
                    /*ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
                    message.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
                    message.setOntology(this.myAgent.getContentManager().getOntologyNames()[0]);
                    message.setConversationId("local");
                    message.addReceiver(this.getRecognizer());
                    this.myAgent.getContentManager().fillContent(message,recognize);
                    this.myAgent.send(message);*/
                }
            } catch (Codec.CodecException ex) {
                Logger.getLogger(DetectFaces.class.getName()).log(Level.SEVERE, null, ex);
            } catch (OntologyException ex) {
                Logger.getLogger(DetectFaces.class.getName()).log(Level.SEVERE, null, ex);
            }           
        }
        else if(msg != null && (msg.getPerformative() == ACLMessage.CANCEL))
        {
            ACLMessage cancel = new ACLMessage(ACLMessage.CANCEL);
            cancel.addReceiver(this.getRecognizer());
            cancel.setContent("stop");
            this.myAgent.send(cancel);
            this.myAgent.doDelete();
        }
        else
        {
            this.block();
        }
    }

    public Mat getGrayEq() {
        return grayEq;
    }

    public void setGrayEq(Mat grayEq) {
        this.grayEq = grayEq;
    }

    public MatOfRect getFaceDetections() {
        return faceDetections;
    }

    public void setFaceDetections(MatOfRect faceDetections) {
        this.faceDetections = faceDetections;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public Size getMinFeaturesSize() {
        return minFeaturesSize;
    }

    public void setMinFeaturesSize(Size minFeaturesSize) {
        this.minFeaturesSize = minFeaturesSize;
    }

    public Size getMaxFeaturesSize() {
        return maxFeaturesSize;
    }

    public void setMaxFeaturesSize(Size maxFeaturesSize) {
        this.maxFeaturesSize = maxFeaturesSize;
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    public void setScaleFactor(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public int getMinNeighbors() {
        return minNeighbors;
    }

    public void setMinNeighbors(int minNeighbors) {
        this.minNeighbors = minNeighbors;
    }

    public String getClassifier() {
        return classifier;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public CascadeClassifier getFaceDetector() {
        return faceDetector;
    }

    public void setFaceDetector(CascadeClassifier faceDetector) {
        this.faceDetector = faceDetector;
    }

    public int getNumfaces() {
        return numfaces;
    }

    public void setNumfaces(int numfaces) {
        this.numfaces = numfaces;
    }

    public AID getRecognizer() {
        return recognizer;
    }

    public void setRecognizer(AID recognizer) {
        this.recognizer = recognizer;
    }
}
