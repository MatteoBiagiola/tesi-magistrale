/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.DataStore;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.states.MsgReceiver;

/**
 *
 * @author matteo
 */
public class MessageReceiver extends MsgReceiver{
    
    public MessageReceiver(Agent a,MessageTemplate mt,long deadline,DataStore s,Object msgKey)
    {
        super(a,mt,deadline,s,msgKey);
        this.setDeadline(System.currentTimeMillis() + deadline);
    }
    
    @Override
    protected void handleMessage(ACLMessage msg)
    {
        if(msg != null)
        {
            System.out.println(this.myAgent.getLocalName() + ": message received");
        }
        else
        {
            System.out.println(this.myAgent.getLocalName() + ": timeout expired");
        }
    }
    
}
