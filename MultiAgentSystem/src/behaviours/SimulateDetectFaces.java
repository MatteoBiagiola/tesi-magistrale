/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.AID;
import jade.core.Agent;
import jade.core.LifeCycle;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.wrapper.AgentState;

/**
 *
 * @author matteo
 */
public class SimulateDetectFaces extends CyclicBehaviour{

    private AID recognizer;
    
    public SimulateDetectFaces(Agent a,AID recognizer)
    {
        super(a);
        this.recognizer = recognizer;
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive();
        if(msg != null && msg.getPerformative() == ACLMessage.INFORM)
        {
            System.out.println(this.myAgent.getLocalName() + 
                    ": frame received");
            //message comes from extractor that is the frame extracted by the video
            //agent performes the detection in this frame and sends results to recognizer
            ACLMessage mess = new ACLMessage(ACLMessage.REQUEST);
            mess.addReceiver(this.recognizer);
            this.myAgent.send(mess);
            System.out.println(this.myAgent.getLocalName() + 
                    ": recognition request sent");
        }
        else
        {
            this.block();
        }
        
    }
    
}
