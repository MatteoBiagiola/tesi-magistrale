/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;
import jade.wrapper.StaleProxyException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.CuttedFace;
import ontology.Recognize;
import ontology.Slot;
import org.opencv.core.Size;
import org.opencv.face.FaceRecognizer;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class PreRecognizeFaces extends CyclicBehaviour{
    
    private FaceRecognizer fr;
    private PrintWriter writer;
    private long time_elapsed;
    private long time_left;
    private long wcet_local;
    private long recognition_time = 400;
    private long negotiation;
    private long output;
    private int exceeded;
    private int propose;
    private DFAgentDescription ad;
    private String rightEyeClassificator;
    private String leftEyeClassificator;
    private Behaviour receiveRemoteRecognitionRequest;
    private Behaviour recognizeFaces;
    private Behaviour sendRemoteRecognitionRequest;
    
    public PreRecognizeFaces(Agent a,FaceRecognizer fr,PrintWriter writer,
            String rightEyeClassificator,String leftEyeClassificator) {
        super(a);
        this.setFr(fr);
        this.setWriter(writer);
        this.setWcet_local(0);
        this.setNegotiation(100);
        this.setOutput(10);
        this.setExceeded(0);
        this.setPropose(0);
        this.setRightEyeClassificator(rightEyeClassificator);
        this.setLeftEyeClassificator(leftEyeClassificator);
        
        DFAgentDescription ad = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Remote-recognition");
        ad.addServices(sd);
        
        this.setAd(ad);
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchConversationId("local"));
        if(msg != null && (msg.getPerformative() == ACLMessage.REQUEST))
        {
            try 
            {
                
                Recognize recognize = (Recognize) this.myAgent.getContentManager().extractContent(msg);
                Slot slot = (Slot) recognize.getSlot();
                ArrayList cuttedFaces = recognize.getCuttedFaces();
                this.setTime_elapsed(slot.getPeriod() - (System.currentTimeMillis() - slot.getStart()));
                this.setTime_left(this.getTime_elapsed() - this.getNegotiation() - this.getOutput());
                if(cuttedFaces != null)
                {
                    /*this.setWcet_local(this.getRecognition_time() * cuttedFaces.size());
                    if(this.getWcet_local() > this.getTime_left())
                    {
                        //agent send help request: agent calculates the number of faces to send 
                        //("toSend" and so those to local-recognize "toKeep")
                        this.setExceeded((int) Math.ceil((double) (this.getWcet_local() - this.getTime_left())/this.getRecognition_time()));
                        if(this.getExceeded() > cuttedFaces.size())
                        {
                            this.setExceeded(cuttedFaces.size());
                        }
                        ArrayList toSend = new ArrayList();
                        ArrayList toKeep = new ArrayList();
                        //send the first "exceeded"
                        for(int i = 0; i < this.getExceeded(); i++)
                        {
                            toSend.add(cuttedFaces.get(i));
                        }
                        //keep the difference
                        for(int i = this.getExceeded(); i < cuttedFaces.size(); i++)
                        {
                            toKeep.add(cuttedFaces.get(i));
                        }
                        System.out.println(this.myAgent.getLocalName() + ": send help request with " 
                        + this.getExceeded() + " faces to send and " + toKeep.size() + " faces to keep");
                        recognize.setCuttedFaces(toSend);
                        //search in DF remote recognizers
                        DFAgentDescription[] remoteRecognizers = DFService.search(this.myAgent,this.getAd());
                        ACLMessage remote = new ACLMessage(ACLMessage.CFP);
                        if(remoteRecognizers.length > 0)
                        {
                            for(int i = 0; i < remoteRecognizers.length; i++)
                            {
                                AID remoteAgent = remoteRecognizers[i].getName();
                                if(!remoteAgent.getLocalName().equals(this.myAgent.getLocalName()))
                                {
                                    remote.addReceiver(remoteAgent);
                                }
                            }
                            //set deadline (negotiation time)
                            remote.setReplyByDate(new Date(System.currentTimeMillis() + this.getNegotiation()));
                            remote.setConversationId("remote");
                            this.myAgent.addBehaviour(new SendRemoteRecognitionRequest(this.myAgent,remote,
                                    this.getFr(),this.getWriter(),this.getTime_left(),
                                    this.getRecognition_time(),toKeep,this.getExceeded(),
                                    this.getNegotiation(),System.currentTimeMillis(),recognize));
                        }
                    }
                    else
                    {
                        //agent listens to incoming cfp messages (deadline is negotiation time).
                        //Moreover agent calculates the number of faces ("propose") that he is willing to recognize
                        this.setPropose((int) Math.floor((double) (this.getTime_left() - this.getWcet_local())/this.getRecognition_time()));
                        if(this.getPropose() > 0)
                        {
                            //agent listens only if he can propose to recognize at least one face
                            System.out.println(this.myAgent.getLocalName() + ": local recognition with " 
                            + this.getPropose() + " faces to propose and " + cuttedFaces.size() + " faces to recognize");
                            this.myAgent.addBehaviour(new ReceiveRemoteRecognitionRequest(this.myAgent,this.getNegotiation(),
                                    System.currentTimeMillis(),this.getFr(),this.getWriter(),cuttedFaces,this.getPropose()));
                        }
                        else
                        {
                            System.out.println(this.myAgent.getLocalName() + ": local recognition with " 
                            + recognize.getCuttedFaces().size() + " faces to recognize");
                            this.myAgent.addBehaviour(new RecognizeFaces(this.myAgent,cuttedFaces,
                                    this.getFr(),this.getWriter(),false));
                        }
                    }*/
                }
                /*else
                {
                    //agent listens to incoming cfp messages (deadline is negotiation time + an offset since
                    //agent have not local face to recognize).
                    //Moreover agent calculates the number of faces ("propose") that he is willing to recognize
                    this.setPropose((int) Math.floor((double) (this.getTime_left() - 100)/this.getRecognition_time()));
                    if(this.getPropose() > 0)
                    {
                        //agent listens only if he can propose to recognize at least one face
                        System.out.println(this.myAgent.getLocalName() + ": no local recognition with " 
                        + this.getPropose() + " faces to propose and " + 0 + " faces to locally recognize");
                        this.myAgent.addBehaviour(new ReceiveRemoteRecognitionRequest(this.myAgent,this.getNegotiation() + 100,
                                System.currentTimeMillis(),this.getFr(),null,null,this.getPropose()));
                    }
                }*/
            } catch (Codec.CodecException ex) {
                Logger.getLogger(PreRecognizeFaces.class.getName()).log(Level.SEVERE, null, ex);
            } catch (OntologyException ex) {
                Logger.getLogger(PreRecognizeFaces.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(msg != null && (msg.getPerformative() == ACLMessage.CANCEL))
        {
            this.myAgent.doDelete();
            try {
                this.myAgent.getContainerController().kill();
            } catch (StaleProxyException ex) {
                Logger.getLogger(PreRecognizeFaces.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            this.block();
        }
    }

    public FaceRecognizer getFr() {
        return fr;
    }

    public void setFr(FaceRecognizer fr) {
        this.fr = fr;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }

    public long getWcet_local() {
        return wcet_local;
    }

    public void setWcet_local(long wcet_local) {
        this.wcet_local = wcet_local;
    }

    public long getRecognition_time() {
        return recognition_time;
    }

    public long getNegotiation() {
        return negotiation;
    }

    public void setNegotiation(long negotiation) {
        this.negotiation = negotiation;
    }

    public long getTime_elapsed() {
        return time_elapsed;
    }

    public void setTime_elapsed(long time_elapsed) {
        this.time_elapsed = time_elapsed;
    }

    public long getTime_left() {
        return time_left;
    }

    public void setTime_left(long time_left) {
        this.time_left = time_left;
    }

    public long getOutput() {
        return output;
    }

    public void setOutput(long output) {
        this.output = output;
    }

    public DFAgentDescription getAd() {
        return ad;
    }

    public void setAd(DFAgentDescription ad) {
        this.ad = ad;
    }

    public int getExceeded() {
        return exceeded;
    }

    public void setExceeded(int exceeded) {
        this.exceeded = exceeded;
    }

    public int getPropose() {
        return propose;
    }

    public void setPropose(int propose) {
        this.propose = propose;
    }

    public String getRightEyeClassificator() {
        return rightEyeClassificator;
    }

    public void setRightEyeClassificator(String rightEyeClassificator) {
        this.rightEyeClassificator = rightEyeClassificator;
    }

    public String getLeftEyeClassificator() {
        return leftEyeClassificator;
    }

    public void setLeftEyeClassificator(String leftEyeClassificator) {
        this.leftEyeClassificator = leftEyeClassificator;
    }

}
