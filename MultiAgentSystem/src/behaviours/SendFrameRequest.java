/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.Slot;

public class SendFrameRequest extends TickerBehaviour{

    private AID extractor;
    private AID recognizer;
    private long period_slot;

    public SendFrameRequest(Agent a, long period, AID extractor, AID recognizer)
    {
        super(a,period);
        this.setPeriod_slot(period);
        this.setExtractor(extractor);
        this.setRecognizer(recognizer);
    }

    @Override
    protected void onTick() {
        
        ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.addReceiver(this.getExtractor());
        msg.setConversationId("local");
        msg.setLanguage(this.myAgent.getContentManager().getLanguageNames()[0]);
        msg.setOntology(this.myAgent.getContentManager().getOntologyNames()[0]);
        
        Slot slot = new Slot();
        slot.setStart(System.currentTimeMillis()); 
        slot.setPeriod(this.getPeriod_slot());
        
        try {
            this.myAgent.getContentManager().fillContent(msg,slot);
            this.myAgent.send(msg);
        } catch (Codec.CodecException ex) {
            Logger.getLogger(SendFrameRequest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (OntologyException ex) {
            Logger.getLogger(SendFrameRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public AID getExtractor() {
        return extractor;
    }

    public void setExtractor(AID extractor) {
        this.extractor = extractor;
    }

    public long getPeriod_slot() {
        return period_slot;
    }

    public void setPeriod_slot(long period_slot) {
        this.period_slot = period_slot;
    }

    public AID getRecognizer() {
        return recognizer;
    }

    public void setRecognizer(AID recognizer) {
        this.recognizer = recognizer;
    }

}
