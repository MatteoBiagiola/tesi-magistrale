/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.leap.ArrayList;
import java.io.PrintWriter;
import org.opencv.face.FaceRecognizer;

/**
 *
 * @author matteo
 */
public class CustomContractNetResponder extends SimpleBehaviour{

    private boolean done;
    private ArrayList toRecognizeLocal;
    private FaceRecognizer fr;
    private PrintWriter writer;
    
    public CustomContractNetResponder(Agent a,ArrayList toRecognizeLocal,
            FaceRecognizer fr,PrintWriter writer)
    {
        super(a);
        this.setDone(false);
        this.setToRecognizeLocal(toRecognizeLocal);
        this.setFr(fr);
        this.setWriter(writer);
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchConversationId("remote"));
        if(msg != null && (msg.getPerformative() == ACLMessage.REJECT_PROPOSAL))
        {
            System.out.println(this.myAgent.getLocalName() + ": handle reject proposal");
            this.setDone(true);
            //local recognition
            this.myAgent.addBehaviour(new RecognizeFaces(this.myAgent,this.getToRecognizeLocal(),
                    this.getFr(),this.getWriter(),false));
        }
        else if(msg != null && (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL))
        {
            System.out.println(this.myAgent.getLocalName() + ": handle accept proposal");
            this.setDone(true);
            //local and remote recognition
            this.myAgent.addBehaviour(new RecognizeFaces(this.myAgent,this.getFr(),this.getWriter(),
                    true,msg,this.getToRecognizeLocal()));
        }
        else
        {
            this.block();
        }
    }

    @Override
    public boolean done() {
        return this.isDone();
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public ArrayList getToRecognizeLocal() {
        return toRecognizeLocal;
    }

    public void setToRecognizeLocal(ArrayList toRecognizeLocal) {
        this.toRecognizeLocal = toRecognizeLocal;
    }

    public FaceRecognizer getFr() {
        return fr;
    }

    public void setFr(FaceRecognizer fr) {
        this.fr = fr;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }
    
}
