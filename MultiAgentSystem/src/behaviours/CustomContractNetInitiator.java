/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.content.lang.Codec;
import jade.content.onto.OntologyException;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.PrintWriter;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import ontology.CuttedFace;
import ontology.Recognize;
import org.opencv.face.FaceRecognizer;

/**
 *
 * @author matteo
 */
public class CustomContractNetInitiator extends SimpleBehaviour{

    private boolean done;
    private long deadline;
    private long count;
    private long start;
    private FaceRecognizer fr;
    private PrintWriter writer;
    private Vector responses;
    
    public CustomContractNetInitiator(Agent a,long deadline,long start,PrintWriter writer)
    {
        super(a);
        this.setDone(false);
        this.setDeadline(deadline);
        this.setStart(start);
        this.setCount(0);
        this.setWriter(writer);
        this.setResponses(new Vector());
    }
    
    @Override
    public void action() {
        ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchConversationId("remote"));
        if(msg != null && (msg.getPerformative() == ACLMessage.FAILURE 
                || msg.getPerformative() == ACLMessage.INFORM))
        {
            System.out.println(this.myAgent.getLocalName() + ": failure or inform received");
            this.getResponses().add(msg);
        }
        else if(this.getDeadline() - this.getCount() >= 0)
        {
            this.setCount(System.currentTimeMillis() - this.getStart());
        }
        else
        {
            this.setDone(true);
            System.out.println(this.myAgent.getLocalName() + ": expired timeout"
                    + " for receiving results by remote recognizer");
            if(this.getResponses().size() != 0)
            {
                for(Object o: this.getResponses())
                {
                    ACLMessage content = (ACLMessage) o;
                    if(content.getPerformative() == ACLMessage.INFORM)
                    {
                        System.out.println(this.myAgent.getLocalName() + ": handle inform");
                        try 
                        {
                            Recognize recognize = (Recognize) this.myAgent.getContentManager().extractContent(content);
                            for(Object ob: recognize.getCuttedFaces().toArray())
                            {
                                CuttedFace cutFace = (CuttedFace) ob;
                                if(this.getWriter() != null)
                                {
                                    this.getWriter().println("* Frame number: " + cutFace.getFrame_num() 
                                            + " Face number: " + cutFace.getFace_num() 
                                            + " Label: " + cutFace.getPredictlabel()
                                            + " Confidence: " + cutFace.getConfidence());
                                    this.getWriter().flush();
                                }
                            }
                            
                        } catch (Codec.CodecException ex) {
                            Logger.getLogger(CustomContractNetInitiator.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (OntologyException ex) {
                            Logger.getLogger(CustomContractNetInitiator.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else if (content.getPerformative() == ACLMessage.FAILURE)
                    {
                        System.out.println(this.myAgent.getLocalName() + ": handle failure");
                    }
                        
                }
            }
        }
    }

    @Override
    public boolean done() {
        return this.isDone();
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public long getDeadline() {
        return deadline;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public FaceRecognizer getFr() {
        return fr;
    }

    public void setFr(FaceRecognizer fr) {
        this.fr = fr;
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }

    public Vector getResponses() {
        return responses;
    }

    public void setResponses(Vector responses) {
        this.responses = responses;
    }
    
}
