/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facedetection;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class FaceDetection {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        long start = System.nanoTime();
        CascadeClassifier faceDetector = new CascadeClassifier("resources/lbpcascade_frontalface.xml");
        Mat image = Imgcodecs.imread("resources/input/prova1.bmp");
        int DETECTION_WIDTH = 320;
        Mat smallImage = new Mat();
        int scaledHeight = 0;
        Size rescale;
        float scale = image.cols()/(float) DETECTION_WIDTH;
        if(image.cols() > DETECTION_WIDTH)
        {
            scaledHeight = Math.round(image.rows()/scale);
            rescale = new Size(DETECTION_WIDTH,scaledHeight);
            Imgproc.resize(image, smallImage, rescale);
        }
        else
        {
            smallImage = image;
        }
        Mat gray = new Mat();
        Mat grayEq = new Mat();
        Imgproc.cvtColor(smallImage, gray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.equalizeHist(gray, grayEq);
        MatOfRect faceDetections = new MatOfRect();
        int flags = Objdetect.CASCADE_SCALE_IMAGE;
        Size minFeaturesSize = new Size(20,20);
        Size maxFeaturesSize = new Size();
        float scaleFactor = (float) 1.2;
        int minNeighbors = 3;
        faceDetector.detectMultiScale(grayEq, faceDetections, scaleFactor, minNeighbors, flags, minFeaturesSize, maxFeaturesSize);
        System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));
        int i = 1;
        for(Rect rect: faceDetections.toArray())
        {
            Rect r = new Rect(rect.x,rect.y,rect.width,rect.height);
            Mat croppedFace = new Mat(grayEq,r);
            if(i < 10)
            {
                Imgcodecs.imwrite(String.format("resources/output/face0%s.bmp",i),croppedFace);
            }
            else Imgcodecs.imwrite(String.format("resources/output/face%s.bmp",i),croppedFace);
            i++;
        }
        long end = System.nanoTime();
        float timeElapsed = (float) ((end - start) / 10e8);
        System.out.println(String.format("Elapsed time: %s", timeElapsed));
    }  
}
