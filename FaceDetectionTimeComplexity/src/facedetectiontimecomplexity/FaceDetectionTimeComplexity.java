/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facedetectiontimecomplexity;

import java.io.File;
import java.io.IOException;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class FaceDetectionTimeComplexity {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    private int detection_width;
    private int scaledHeight;
    private Size rescale;
    private float scale;
    private Mat image;
    private Mat imageColor;
    private Mat imageColorRect = new Mat();
    private Mat grayEq = new Mat();
    private Mat croppedFace;
    private Mat smallImage = new Mat();
    private MatOfRect faceDetections = new MatOfRect();
    private int flags;
    private Size minFeaturesSize;
    private Size maxFeaturesSize;
    private float scaleFactor;
    private int minNeighbors;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String classifier = "resources/lbpcascade_frontalface.xml";
        if(args.length != 0) 
        {
            if(args[0].contains("haar"))
            {
                System.out.println("haarcascade classifier");
                classifier = "resources/haarcascade_frontalface_default.xml";
            }
        }
        else
        {
            System.out.println("lpb classifier");
        }
        StringBuffer sb = new StringBuffer();
        FaceDetectionTimeComplexity fdtc = new FaceDetectionTimeComplexity();
        CascadeClassifier faceDetector = new CascadeClassifier(classifier);
        File images = new File("resources/input");
        fdtc.setup();
        int numImages = fdtc.TestImages(images.listFiles(),0);
        long start = System.nanoTime();
        fdtc.readTestImages(images.listFiles(),faceDetector,sb);
        System.out.println("Average Detection Time: " + (System.nanoTime() - start)/(numImages*10e8) + " [s]");
    } 
    
    
    public void readTestImages(File[] imagesTest,CascadeClassifier fd,StringBuffer sb)
    {
        for(File imageTest: imagesTest)
        {
            if(imageTest.getPath().contains(".DS_Store") || imageTest.getPath().contains(".directory"))
            {
                imageTest.delete();
            }
            else if(imageTest.isDirectory())
            {
                String inputDirName = imageTest.getPath();
                String outputDirName = inputDirName.replace("input","output");
                File outputDirFile = new File(outputDirName);
                if(!outputDirFile.exists())
                {
                    outputDirFile.mkdir();
                }
                this.readTestImages(imageTest.listFiles(),fd,sb);
            }
            else   
            {
                this.setImage(this.readImage(imageTest.getPath(),sb));
                //Imgproc.resize(this.getImage(),this.getImage(),new Size(256,256));
                //Imgcodecs.imwrite(sb.toString(),this.getImage());
                this.detection(fd,sb);
            }
        }
    }
    
    public Mat readImage(String lineToRead, StringBuffer outputPath)
    {
        outputPath.delete(0,outputPath.length());
        outputPath.append(lineToRead.replace("input","output"));
        this.imageColor = Imgcodecs.imread(lineToRead);
        this.imageColorRect = Imgcodecs.imread(lineToRead);
        return Imgcodecs.imread(lineToRead,Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
    }
    
    public void setup()
    {
        this.setDetection_width(320);
        this.setFlags(Objdetect.CASCADE_SCALE_IMAGE);
        this.setMinFeaturesSize(new Size(20,20));
        this.setMaxFeaturesSize(new Size());
        this.setScaleFactor((float) 1.2);
        this.setMinNeighbors(4);
    }
    
    public void detection(CascadeClassifier fd,StringBuffer sb)
    {
        fd.detectMultiScale(this.getImage(),
                this.getFaceDetections(),this.getScaleFactor(), 
                this.getMinNeighbors(),this.getFlags(),this.getMinFeaturesSize(), 
                this.getMaxFeaturesSize());
        System.out.println("Detected " + this.getFaceDetections().toArray().length + " faces");
        int num = 0;
        for(Rect rect: this.getFaceDetections().toArray())
        {   
            Rect r = new Rect(rect.x,rect.y,rect.width,rect.height);
            Imgproc.rectangle(this.imageColorRect, rect.tl(), rect.br(), new Scalar(0, 255, 0, 255),3);
            this.setCroppedFace(new Mat(this.imageColor,r));
            Imgproc.resize(this.getCroppedFace(),this.getCroppedFace(),new Size(96,96));
            Imgcodecs.imwrite(sb.toString(),imageColorRect);
            Imgcodecs.imwrite("resources/output/face_" + num + ".jpg", this.getCroppedFace());
            num++;
        }
    }
    
    private Mat preProcessing()
    {
        this.setScale(this.getImage().cols()/(float) this.getDetection_width());
        if(this.getImage().cols() > this.getDetection_width())
        {
            this.setScaledHeight(Math.round(this.getImage().rows()/this.getScale()));
            this.setRescale(new Size(this.getDetection_width(),this.getScaledHeight()));
            Imgproc.resize(this.getImage(),this.getSmallImage(),this.getRescale());
        }
        else
        {
            this.setSmallImage(this.getImage());
        }
        Imgproc.equalizeHist(this.getSmallImage(),this.getGrayEq());
        return this.getGrayEq();
    }
    
    public int TestImages(File[] images,int num)
    {
        for(File image: images)
        {
            if(image.isDirectory())
            {
                num = this.TestImages(image.listFiles(),num);
            }
            else
            {
                num = num + 1;
            }
        }
        return num;
    }

    public int getDetection_width() {
        return detection_width;
    }

    public void setDetection_width(int detection_width) {
        this.detection_width = detection_width;
    }

    public int getScaledHeight() {
        return scaledHeight;
    }

    public void setScaledHeight(int scaledHeight) {
        this.scaledHeight = scaledHeight;
    }

    public Size getRescale() {
        return rescale;
    }

    public void setRescale(Size rescale) {
        this.rescale = rescale;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public Mat getImage() {
        return image;
    }

    public void setImage(Mat image) {
        this.image = image;
    }

    public Mat getGrayEq() {
        return grayEq;
    }

    public void setGrayEq(Mat grayEq) {
        this.grayEq = grayEq;
    }

    public Mat getCroppedFace() {
        return croppedFace;
    }

    public void setCroppedFace(Mat croppedFace) {
        this.croppedFace = croppedFace;
    }

    public MatOfRect getFaceDetections() {
        return faceDetections;
    }

    public void setFaceDetections(MatOfRect faceDetections) {
        this.faceDetections = faceDetections;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public Size getMinFeaturesSize() {
        return minFeaturesSize;
    }

    public void setMinFeaturesSize(Size minFeaturesSize) {
        this.minFeaturesSize = minFeaturesSize;
    }

    public Size getMaxFeaturesSize() {
        return maxFeaturesSize;
    }

    public void setMaxFeaturesSize(Size maxFeaturesSize) {
        this.maxFeaturesSize = maxFeaturesSize;
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    public void setScaleFactor(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public int getMinNeighbors() {
        return minNeighbors;
    }

    public void setMinNeighbors(int minNeighbors) {
        this.minNeighbors = minNeighbors;
    }

    public Mat getSmallImage() {
        return smallImage;
    }

    public void setSmallImage(Mat smallImage) {
        this.smallImage = smallImage;
    }
}
