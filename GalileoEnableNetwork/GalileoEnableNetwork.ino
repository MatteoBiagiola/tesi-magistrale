#include <Ethernet.h>
//Galileo1 mac 98:4F:EE:00:71:31 ip 192.68.0.20
//Galileo2 mac 98:4F:EE:00:73:21 ip 192.68.0.27

byte mac[] = {0x98, 0x4F, 0xEE, 0x00, 0x73, 0x1F};
byte staticIp[] = {192, 168, 0, 20};

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Attempting to configure Ethernet using DCHCP");
  if (Ethernet.begin(mac) == 0)
  {
    Serial.println("Failed to configure Ethernet using DHCP");
  }
  Serial.println("Attempting to configure Ethernet using Static IP");
  Ethernet.begin(mac, staticIp);
  Serial.print("Galileo IP address: ");
  Serial.println(Ethernet.localIP());
  system("telnetd -l /bin/sh");  // Start the telnet server
}

void loop() {
  // put your main code here, to run repeatedly: 
  
}
