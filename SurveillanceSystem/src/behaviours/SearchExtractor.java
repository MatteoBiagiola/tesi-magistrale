/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import org.opencv.objdetect.CascadeClassifier;

/**
 *
 * @author matteo
 */
public class SearchExtractor extends TickerBehaviour{
    
    public SearchExtractor(Agent a, long period) {
        super(a, period);
    }

    @Override
    protected void onTick()
    {
        System.out.println("OnTick");
        DFAgentDescription ad = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Extraction");
        ad.addServices(sd);
        try 
        {
            DFAgentDescription[] des = DFService.search(this.myAgent, ad);
            ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
            if(des.length>0)
            {
                System.out.println("Extractor found");
                msg.addReceiver(des[0].getName());
                msg.setProtocol(FIPANames.InteractionProtocol.ITERATED_FIPA_REQUEST);
                this.myAgent.addBehaviour(new SendFrameRequest(this.myAgent,msg));
                this.stop();
            }
            else
            {
                System.out.println(this.myAgent.getName() + ": nessun estrattore di frame trovato");
            }

        }catch(FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
