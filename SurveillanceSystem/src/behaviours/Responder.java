/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.SSResponderDispatcher;
import org.opencv.videoio.VideoCapture;

/**
 *
 * @author matteo
 */
public class Responder extends SSResponderDispatcher{

    private VideoCapture vd;
    
    public Responder(Agent a, MessageTemplate tpl) {
        super(a, tpl);
        this.setVd(new VideoCapture("resources/video1.mp4"));
    }

    @Override
    protected Behaviour createResponder(ACLMessage aclm) {
        return new ExtractFrames(this.myAgent,aclm,vd);
    }

    public VideoCapture getVd() {
        return vd;
    }

    public void setVd(VideoCapture vd) {
        this.vd = vd;
    }
    
}
