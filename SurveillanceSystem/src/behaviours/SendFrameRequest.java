/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.IteratedAchieveREInitiator;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.videoio.Videoio;

/**
 *
 * @author matteo
 */
public class SendFrameRequest extends IteratedAchieveREInitiator{
    
    private int framecount = 1;
    private ACLMessage initMessage;
    
    public SendFrameRequest(Agent a, ACLMessage msg) {
        super(a, msg);
        this.setInitMessage(msg);
    }
    
    protected void handleRefuse(ACLMessage refuse)
    {
        System.out.println("Video ended");
    }
    
    protected void handleInform(ACLMessage frameMsg,Vector nextRequests)
    {
        if(frameMsg != null)
        {
            MatOfByte byteframe = new MatOfByte();
            byteframe.fromArray(frameMsg.getByteSequenceContent());
            Mat image = Imgcodecs.imdecode(byteframe, Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
            CascadeClassifier faceDetector = new CascadeClassifier("resources/faceClassifiers/haarcascade_frontalface_default.xml");
            Mat faceEq = new Mat();
            MatOfRect detected = new MatOfRect();
            //List<Mat> faceDetected = new ArrayList();
            Imgproc.equalizeHist(image,faceEq);
            faceDetector.detectMultiScale(faceEq,detected,1.3,5,Objdetect.CASCADE_SCALE_IMAGE,
                new Size(20,20),new Size());
            int i = 1;
            for(Rect rect: detected.toArray())
            {
                Rect r = new Rect(rect.x,rect.y,rect.width,rect.height);
                Mat croppedFace = new Mat(image,r);
                Imgcodecs.imwrite("resources/faces/face-" + this.getFramecount() + "-" + i + ".bmp", croppedFace);
                i++;
                //faceDetected.add(croppedFace);
            }
            this.setFramecount(this.getFramecount() + 1);
            nextRequests.add(this.getInitMessage());
        }
        else
        {
            System.out.println("null");
            this.block();
        }
    }
    
    public static boolean isSessionTerminated(ACLMessage inform)
    {
        System.out.println("Termination flag");
        return true;
    }

    public int getFramecount() {
        return framecount;
    }

    public void setFramecount(int framecount) {
        this.framecount = framecount;
    }

    public ACLMessage getInitMessage() {
        return initMessage;
    }

    public void setInitMessage(ACLMessage initMessage) {
        this.initMessage = initMessage;
    }
}
