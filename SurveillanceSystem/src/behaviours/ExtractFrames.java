/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package behaviours;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.proto.SSIteratedAchieveREResponder;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

/**
 *
 * @author matteo
 */
public class ExtractFrames extends SSIteratedAchieveREResponder{
    
    private VideoCapture vd;
    
    public ExtractFrames(Agent a, ACLMessage request, VideoCapture vd) {
        super(a, request);
        this.setVd(vd);
    }
    
    protected ACLMessage handleRequest(ACLMessage request)
    {
        ACLMessage result = request.createReply();
        Mat frame =  new Mat();
        if(this.getVd().read(frame))
        {
            Mat gray = new Mat();
            Imgproc.cvtColor(this.shrinkFrame(frame,320),gray,Imgproc.COLOR_BGR2GRAY);
            MatOfByte byteimage = new MatOfByte();
            Imgcodecs.imencode(".bmp",gray,byteimage);
            Imgcodecs.imwrite("resources/frames/frames-" + String.format("%d.bmp",
                    (int) vd.get(Videoio.CAP_PROP_POS_FRAMES)),gray);
            result.setByteSequenceContent(byteimage.toArray());
            result.setPerformative(ACLMessage.INFORM);
        }
        else
        {
            System.out.println(this.getVd().get(Videoio.CAP_PROP_POS_FRAMES));
            result.setPerformative(ACLMessage.REFUSE);
        }
        return result;
    }
    
    public Mat shrinkFrame(Mat image, int detection_width)
    {
        Mat smallImage = new Mat();
        int scaledHeight = 0;
        Size rescale;
        float scale = image.cols()/(float) detection_width;
        if(image.cols() > detection_width)
        {
            scaledHeight = Math.round(image.rows()/scale);
            rescale = new Size(detection_width,scaledHeight);
            Imgproc.resize(image,smallImage,rescale);
        }
        else
        {
            smallImage = image;
        }
        return smallImage;
    }

    public VideoCapture getVd() {
        return vd;
    }

    public void setVd(VideoCapture vd) {
        this.vd = vd;
    }
}
