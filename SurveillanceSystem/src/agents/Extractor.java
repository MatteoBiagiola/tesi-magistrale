/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.ExtractFrames;
import behaviours.Responder;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import org.opencv.core.Core;

/**
 *
 * @author matteo
 */
public class Extractor extends Agent{
    static{
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    protected void setup()
    {
        DFAgentDescription df = new DFAgentDescription();
        df.setName(this.getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setName(this.getLocalName() + "-service");
        sd.setType("Extraction");
        this.addBehaviour(new Responder(this,MessageTemplate.MatchPerformative(ACLMessage.REQUEST)));
        /*this.addBehaviour(new OneShotBehaviour(this){
                @Override
                public void action()
                {
                        this.myAgent.addBehaviour(new Responder(this.myAgent,
                                MessageTemplate.MatchPerformative(ACLMessage.REQUEST)));
                }
        });*/
        df.addServices(sd);
        try 
        {
            DFService.register(this, df);
            System.out.println("Extraction service registered");
	}catch(FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
	}
    }
    
    protected void takeDown()
    {
        try 
        {
            DFService.deregister(this);
            System.out.println("Extraction service unregistered");
        } catch (FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
