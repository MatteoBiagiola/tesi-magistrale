/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import behaviours.SearchExtractor;
import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import org.opencv.core.Core;

/**
 *
 * @author matteo
 */
public class Detector extends Agent{
    static{
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    protected void setup()
    {
        DFAgentDescription df = new DFAgentDescription();
        df.setName(this.getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setName(this.getLocalName() + "-service");
        sd.setType("Detection");
        /*this.addBehaviour(new OneShotBehaviour(this){
                @Override
                public void action()
                {
                        this.myAgent.addBehaviour(new SearchExtractor(this.myAgent,1000));
                }
        });*/
        this.addBehaviour(new WakerBehaviour(this,10000){
                @Override
                public void onWake()
                {
                        this.myAgent.addBehaviour(new SearchExtractor(this.myAgent,1000));
                }
        });
        df.addServices(sd);
        try 
        {
            DFService.register(this, df);
            System.out.println("Detection service registered");
	}catch(FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
	}
    }
    
    protected void takeDown()
    {
        try 
        {
            DFService.deregister(this);
            System.out.println("Detection service unregistered");
        } catch (FIPAException e) 
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
