/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facerecognition;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.face.Face;
import org.opencv.face.FaceRecognizer;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class FaceRecognition {

    /**
     * @param args the command line arguments
     */
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    private int detected = 0;
    
    public static void main(String[] args) {
        // TODO code application logic here
        try
        {   
            File trainingEigen = new File("resources/trainingEigen.xml");
            File trainingFisher = new File("resources/trainingFisher.xml");
            File trainingLbph = new File("resources/trainingLbph.xml");
            File trainingFile = trainingEigen;
            String algo = "Eigen";
            int size = 96;
            FaceRecognizer fr = Face.createEigenFaceRecognizer();
            FacePreProcesser fp = new FacePreProcesser(96,96,new Size(),new Size(),
                3,(float) 1.1,Objdetect.CASCADE_SCALE_IMAGE);
            if(args.length != 0)
            {
                String recognizer = args[0];
                if(recognizer.contains("fisher"))
                {
                    algo = "Fisher";
                    trainingFile = trainingFisher;
                    fr = Face.createFisherFaceRecognizer();
                    System.out.println("Fisherface algorithm");
                }
                else if(recognizer.contains("lbph"))
                {
                    algo = "Lbph";
                    trainingFile = trainingLbph;
                    fr = Face.createLBPHFaceRecognizer();
                    System.out.println("LBPH algorithm");
                }
                else
                {
                    System.out.println("Eigenface algorithm");
                }
            }
            else
            {
                System.out.println("Eigenface algorithm");
            }
            long startTrain = System.nanoTime();
            FaceRecognition faceRec = new FaceRecognition();
            int numImagesTest = 0;
            File targetTrainingFile = new File("resources/training");
            File targetTestFile = new File("resources/faces");
            numImagesTest = faceRec.TestImages(targetTestFile.listFiles(),0);
            System.out.println("Number images test: " + numImagesTest);
            FileManager fileTrainingManager = new FileManager("resources/training.txt",
                    "resources/training.txt");
            FileManager fileTestManager = new FileManager("resources/results" +
                    algo + ".txt","resources/results" + algo + ".txt");
            fileTrainingManager.fillFile(targetTrainingFile.listFiles());
            fileTrainingManager.getWriter().close();
            if(trainingFile.exists())
            {
                System.out.println("Training file exist");
                System.out.print("Loading...");
                fr.load(trainingFile.getPath());
                System.out.println("done");
            }
            else
            {
                System.out.println("Training file doesn't exist");
                System.out.print("Creating training file...");
                String line;
                List<Mat> images = new ArrayList();
                Mat labels = new Mat(fileTrainingManager.getLinesWritten(),1,CvType.CV_32SC1);
                int buff[] = new int[fileTrainingManager.getLinesWritten()];
                labels.get(0,0,buff);
                int i = 0;
                while((line = fileTrainingManager.getReader().readLine()) != null)
                {
                    images.add(fileTrainingManager.readImage(line,buff,i,size));
                    i++;
                }
                labels.put(0,0,buff);
                fr.train(images,labels);
                images.removeAll(images);
                labels.release();
                fr.save(trainingFile.getPath());
                System.out.println("done");
            }
            System.out.println("Training time: " + (System.nanoTime() - startTrain)/10e8);
            //get test image files
            long startRec = System.nanoTime();
            File[] imagesTest = targetTestFile.listFiles();
            faceRec.readTestImages(imagesTest,fr,fileTestManager,size,fp);
            fileTestManager.getWriter().close();
            System.out.println("Average recognition time: " + 
                    ((System.nanoTime() - startRec)/10e8)/numImagesTest + " [s]");
            System.out.println("Eye-Detection accuracy: " + 
                    (faceRec.getDetected()*100)/numImagesTest + " %" + " on " + 
                    numImagesTest + " images");
        }
        catch(FileNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch(UnsupportedEncodingException ex)
        {
            ex.printStackTrace();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
        catch(CvException ex)
        {
            System.out.println("OutOfMemoryError");
            System.gc();
        }
    }
    
    public void readTestImages(File[] imagesTest,FaceRecognizer fr,
            FileManager fileTestManager, int size, FacePreProcesser fp)
    {
        for(File imageTest: imagesTest)
        {
            if(imageTest.getPath().contains(".DS_Store") ||
                    imageTest.getPath().contains(".directory"))
            {
                imageTest.delete();
            }
            else if(imageTest.isDirectory())
            {
                this.readTestImages(imageTest.listFiles(),fr,fileTestManager,size,fp);
            }
            else   
            {
                String pathInputName = imageTest.getPath();
                Mat testImage = Imgcodecs.imread(pathInputName,Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
                fp.setFace(testImage);
                this.noPreProcessing(testImage,fr,fileTestManager,size);
                //this.PreProcessing(fp,fr,fileTestManager,pathInputName);
            }
        }
    }
    
    public void noPreProcessing(Mat image,FaceRecognizer fr,FileManager fileTestManager,int size)
    {
        Imgproc.resize(image,image,new Size(size,size));
        int predictLabel[] = {-1};
        double confidence[] = {0.0};
        fr.predict(image,predictLabel,confidence);
        fileTestManager.writeResult(predictLabel[0],confidence[0]);
    }
    
    public int TestImages(File[] images,int num)
    {
        for(File image: images)
        {
            if(image.isDirectory())
            {
                num = this.TestImages(image.listFiles(),num);
            }
            else if(image.getPath().contains(".DS_Store") ||
                        image.getPath().contains(".directory"))
            {
                image.delete();
            }
            else
            {
                num = num + 1;
            }
        }
        return num;
    }
    
    public void PreProcessing(FacePreProcesser fp,FaceRecognizer fr,
            FileManager fileTestManager,String imageName)
    {
        //if eyes are closed it can be that they are not detected.
        //For the moment is not tried another eye classifier but this
        //could be a valid alternative.
        Imgproc.resize(fp.getFace(),fp.getFace(),new Size(256,256));
        fp.detectLeftEye("resources/eyeDetection/haarcascade_lefteye_2splits.xml");
        fp.detectRightEye("resources/eyeDetection/haarcascade_righteye_2splits.xml");
        fp.geometricalTransformation();
        fp.separateHistogramEqualization(fp.getGeoTransformed());
        fp.smoothing(fp.getEqualized());
        fp.ellipticalMask(fp.getFiltered());
        if(fp.getLeftEyePoint().x > 0 && fp.getRightEyePoint().x > 0)
        {
            this.setDetected(this.getDetected() + 1);
            int predictLabel[] = {-1};
            double confidence[] = {0.0};
            fr.predict(fp.getFiltered(),predictLabel,confidence);
            fileTestManager.writeResult(predictLabel[0],confidence[0]);
        }
        else
        {
            fileTestManager.writeResult(imageName);
        }
    }

    public int getDetected() {
        return detected;
    }

    public void setDetected(int detected) {
        this.detected = detected;
    }
}
