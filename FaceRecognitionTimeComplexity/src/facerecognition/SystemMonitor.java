/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facerecognition;

import com.sun.management.OperatingSystemMXBean;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryPoolMXBean;
import java.util.List;
/**
 *
 * @author matteo
 */
public class SystemMonitor implements Runnable{
    
    private static final long mb = 1024L * 1024L;
    private static final long percent = 100;
    private OperatingSystemMXBean osBean;
    private MemoryMXBean osMemoryBean;
    private List<MemoryPoolMXBean> memorypool;
    private Runtime runtime;
    private PrintWriter writer;

    public SystemMonitor() throws FileNotFoundException, UnsupportedEncodingException
    {
        this.setWriter(new PrintWriter("resources/sysmon.txt","UTF-8"));
        this.setOsMemoryBean(ManagementFactory.getMemoryMXBean());
        this.setOsBean(ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class));
        this.setRuntime(Runtime.getRuntime());
        this.setMemorypool(ManagementFactory.getMemoryPoolMXBeans());
    }
    
    public long getHeapUsed()
    {
        long memory = 0;
        for(MemoryPoolMXBean memorybean : this.getMemorypool())
        {
            memory = memory + memorybean.getUsage().getUsed();
        }
        return memory/mb;
    }
    
    public double getJvmCpuUsed() {
        return (this.getOsBean().getProcessCpuLoad()*percent);
    }

    public double getSystemCpuUsed() {
        return (this.getOsBean().getSystemCpuLoad()*percent);
    }
    
    public long getTotalRamSize()
    {
        return (this.getOsBean().getTotalPhysicalMemorySize())/mb;
    }
    
    public long getTotalSwapSize()
    {
        return (this.getOsBean().getTotalSwapSpaceSize())/mb;
    }
    
    public long getTotalHeapSize()
    {
        return (this.getOsMemoryBean().getHeapMemoryUsage().getCommitted())/mb;
    }
    
    public long getSwapMemoryUsed()
    {
        return (this.getOsBean().getTotalSwapSpaceSize() - this.getOsBean().getFreeSwapSpaceSize())/mb;
    }
    
    public long getRamUsed()
    {
        return (this.getOsBean().getTotalPhysicalMemorySize() -  this.getOsBean().getFreePhysicalMemorySize())/mb;
    }
    
    @Override
    public void run() {
        this.getWriter().println("Recent JVM cpu load: ".concat(String.format("%f",this.getJvmCpuUsed())).concat(" %"));
        this.getWriter().println("Ram used: ".concat(String.format("%d",this.getRamUsed()).concat(" [Mb]")));
        this.getWriter().println("Heap memory used: ".concat(String.format("%d",this.getHeapUsed()).concat(" [Mb]")));
        this.getWriter().println();
        /*this.getWriter().println("System cpu load: ".concat(String.format("%f",this.getSystemCpuUsage())));
        this.getWriter().println("Runtime total memory: ".concat(String.format("%d",this.getTotalMemory())));
        this.getWriter().println("Runtime free memory: ".concat(String.format("%d",this.getFreeMemory())));
        this.getWriter().println("Runtime memory used: ".concat(String.format("%d",this.getMemoryUsed())));
        this.getWriter().println("Runtime max memory: ".concat(String.format("%d",this.getMaxMemory())));
        this.getWriter().println("Bean heap memory init: ".concat(String.format("%d",this.getHeapInitMemory())));
        this.getWriter().println("Bean heap memory used: ".concat(String.format("%d",this.getHeapMemoryUsed())));
        this.getWriter().println("Bean heap memory committed: ".concat(String.format("%d",this.getHeapMemoryCommitted())));
        this.getWriter().println("Bean heap memory max: ".concat(String.format("%d",this.getHeapMemoryMax())));
        this.getWriter().println("Bean non heap memory init: ".concat(String.format("%d",this.getNonHeapInitMemory())));
        this.getWriter().println("Bean non heap memory used: ".concat(String.format("%d",this.getNonHeapMemoryUsed())));
        this.getWriter().println("Bean non heap memory committed: ".concat(String.format("%d",this.getNonHeapMemoryCommitted())));
        this.getWriter().println("Bean non heap memory max: ".concat(String.format("%d",this.getNonHeapMemoryMax())));*/
    }
    
    /*public long getTotalMemory()
    {
        return (this.getRuntime().totalMemory())/mb;
    }
    
    public long getFreeMemory()
    {
        return (this.getRuntime().freeMemory())/mb;
    }
    
    public long getMaxMemory()
    {
        return (this.getRuntime().maxMemory())/mb;
    }
    
    public long getMemoryUsed()
    {
        return (this.getRuntime().totalMemory() - this.getRuntime().freeMemory())/mb;
    }
    
    public long getHeapMemoryMax()
    {
        return (this.getOsMemoryBean().getHeapMemoryUsage().getMax())/mb;
    }
    
    public long getNonHeapMemoryMax()
    {
        return (this.getOsMemoryBean().getNonHeapMemoryUsage().getMax())/mb;
    }
    
    public long getHeapInitMemory()
    {
        return (this.getOsMemoryBean().getHeapMemoryUsage().getInit())/mb;
    }
    
    public long getNonHeapInitMemory()
    {
        return (this.getOsMemoryBean().getNonHeapMemoryUsage().getInit())/mb;
    }
    public long getHeapMemoryUsed()
    {
        return (this.getOsMemoryBean().getHeapMemoryUsage().getUsed())/mb;
    }
    
    public long getHeapMemoryCommitted()
    {
        return (this.getOsMemoryBean().getHeapMemoryUsage().getCommitted())/mb;
    }
    
    public long getNonHeapMemoryUsed()
    {
        return (this.getOsMemoryBean().getNonHeapMemoryUsage().getUsed())/mb;
    }
    
    public long getNonHeapMemoryCommitted()
    {
        return (this.getOsMemoryBean().getNonHeapMemoryUsage().getCommitted())/mb;
    }*/

    public OperatingSystemMXBean getOsBean() {
        return osBean;
    }

    public void setOsBean(OperatingSystemMXBean osBean) {
        this.osBean = osBean;
    }

    public MemoryMXBean getOsMemoryBean() {
        return osMemoryBean;
    }

    public void setOsMemoryBean(MemoryMXBean osMemoryBean) {
        this.osMemoryBean = osMemoryBean;
    }

    public Runtime getRuntime() {
        return runtime;
    }

    public void setRuntime(Runtime runtime) {
        this.runtime = runtime;
    }

    public PrintWriter getWriter() {
        return writer;
    }
    
    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }
    
    public List<MemoryPoolMXBean> getMemorypool() {
        return memorypool;
    }

    public void setMemorypool(List<MemoryPoolMXBean> memorypool) {
        this.memorypool = memorypool;
    }
}
