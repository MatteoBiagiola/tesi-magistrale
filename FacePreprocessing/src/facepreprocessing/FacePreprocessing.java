/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facepreprocessing;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class FacePreprocessing {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        long start = System.nanoTime();
        int desired_face_width = 256;
        int desired_face_height = 256;
        Mat warped = new Mat();
        
        /*EYE DETECTION*/
        Mat image = new Mat();
        //images are output of FaceDetection project
        image = Imgcodecs.imread("resources/input/deppGlasses.bmp");
        EyeDetection ed = new EyeDetection(image,new Size(),new Size(),
                2,(float) 1.1,Objdetect.CASCADE_SCALE_IMAGE);
        try
        {
            //the first reliable classifier
            ed.detectLeftEye("resources/haarcascade_mcs_lefteye.xml");
            ed.detectRightEye("resources/haarcascade_mcs_righteye.xml");
            //the second reliable classifier
            /*ed.detectLeftEye("resources/haarcascade_lefteye_2splits.xml");
            ed.detectRightEye("resources/haarcascade_righteye_2splits.xml");*/
            //the third reliable classifier
            /*ed.detectLeftEye("resources/haarcascade_eye.xml");
            ed.detectRightEye("resources/haarcascade_eye.xml");*/
            //glasses classifier
            /*ed.detectLeftEye("resources/haarcascade_eye_tree_eyeglasses.xml");
            ed.detectRightEye("resources/haarcascade_eye_tree_eyeglasses.xml");*/
        }
        catch(ArrayIndexOutOfBoundsException ex)
        {
            System.out.println("Is not possible to detect both eyes and so is not possible"
                    + " to make geometrical transformation of the image");
            System.out.println();
        }
        
        if(ed.getLeftEyePoint().x > 0 && ed.getRightEyePoint().x > 0)
        {
            /*GEOMETRICAL TRANSFORMATION*/
            //Get the center between the 2 eyes
            Point eyesCenter = new Point();
            eyesCenter.x = (ed.getLeftEyePoint().x + ed.getRightEyePoint().x) / 2;
            eyesCenter.y = (ed.getLeftEyePoint().y + ed.getRightEyePoint().y) / 2;
            //Get the angle between the 2 eyes
            double dy = ed.getLeftEyePoint().y - ed.getRightEyePoint().y;
            double dx = ed.getLeftEyePoint().x - ed.getRightEyePoint().x;
            double len = Math.sqrt(dx*dx + dy*dy);
            //convert radians to degrees
            double angle = Math.atan2(dy, dx) * 180.0/Math.PI;
            /*Hand measurement shown that the right eye center should
            ideally be roughly at (0.16, 0.14) of a scaled face image*/
            double desired_right_eye_x = 0.16;
            double desired_right_eye_y = 0.14;
            double desired_left_eye_x = 1 - 0.16;
            /*get the amount we need to scale the image to be the desidered
            fixed sized we want (it's arbitrary, in this case we choose ... see above)*/
            double desired_length = desired_left_eye_x - desired_right_eye_x;
            double scale = desired_length * desired_face_width/len;
            //get the transformation matrix for the desired angle and size
            Mat rot_mat = Imgproc.getRotationMatrix2D(eyesCenter,angle,scale);
            //shift the center of the eyes to be the desired center
            double ex = desired_face_width/2 - eyesCenter.x;
            double ey = (desired_face_height * desired_right_eye_y) - eyesCenter.y;
            double[] data1 = rot_mat.get(0,2);
            double[] data2 = rot_mat.get(1,2);
            data1[0] = data1[0] + ex;
            data2[0] = data2[0] + ey;
            rot_mat.put(0,2,data1);
            rot_mat.put(1,2,data2);
            /*transform the face image to the desired angle, size and position.
            Also clear the transformed image background to a default gray.*/
            Imgproc.warpAffine(image,warped,rot_mat,new Size(desired_face_width,desired_face_height));
            Imgcodecs.imwrite("resources/output/faceGeoTransform.bmp",warped);
        }
        else
        {
            System.out.println("The image will be resized to the desired height and width");
            Imgproc.resize(image,warped,new Size(desired_face_width,desired_face_height));
        }
        
        /*SEPARATE HISTOGRAM EQUALIZATION FOR LEFT AND RIGHT SIDES*/
        int width = warped.cols();
        int height = warped.rows();
        Mat grayWarped = new Mat();
        Mat wholeFace = new Mat();
        Imgproc.cvtColor(warped,grayWarped,Imgproc.COLOR_BGR2GRAY);
        //equalized histogram on the whole face
        Imgproc.equalizeHist(grayWarped,wholeFace);
        Imgcodecs.imwrite("resources/output/wholeface_equalized.bmp",wholeFace);
        int half_width = width/2;
        Rect leftImageSideRect = new Rect(0,0,half_width,height);
        Rect rightImageSideRect = new Rect(half_width,0,width-half_width,height);
        Mat leftImageSide = new Mat(grayWarped,leftImageSideRect);
        Mat rightImageSide = new Mat(grayWarped,rightImageSideRect);
        Imgcodecs.imwrite("resources/output/leftImageSide.bmp",leftImageSide);
        Imgcodecs.imwrite("resources/output/rightImageSide.bmp",rightImageSide);
        //equalized histogram on the left side of the image
        Imgproc.equalizeHist(leftImageSide,leftImageSide);
        //equalized histogram on the right side of the image
        Imgproc.equalizeHist(rightImageSide,rightImageSide);
        Imgcodecs.imwrite("resources/output/leftImageSide_equalized.bmp",leftImageSide);
        Imgcodecs.imwrite("resources/output/rightImageSide_equalized.bmp",rightImageSide);
        //merge of the three images into one: first method (the best)
        Mat faceEqualized = new Mat(height,width,wholeFace.type());
        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                double[] v = {0};
                if(x < width/4)
                {
                    //left 25%: just use the left face.
                    v = leftImageSide.get(y,x);
                }
                else if(x < width * 2/4)
                {
                    //mid-left 25%: blend the left face and the whole face.
                    double[] lv = leftImageSide.get(y,x);
                    double[] wv = wholeFace.get(y,x);
                    /*blend more of the whole face as it moves
                    further right along the face.*/
                    float f = (x - width * 1/4)/(width/4);
                    v[0] = Math.round((1.0 - f) * lv[0] + f * wv[0]);
                }
                else if(x < width * 3/4)
                {
                    //mid-right 25%: blend the right face and the whole face.
                    double[] rv = rightImageSide.get(y,x-half_width);
                    double[] wv = wholeFace.get(y,x);
                    /*blend more of the right-side face as it moves
                    further right along the face.*/
                    float f = (x - width * 2/4)/(width/4);
                    v[0] = Math.round((1.0 - f) * wv[0] + f * rv[0]);
                }
                else
                {
                    //right 25%: just use the right face.
                    v = rightImageSide.get(y,x-half_width);
                }
                faceEqualized.put(y,x,v);
            }
        }
        Imgcodecs.imwrite("resources/output/faceEqualized.bmp",faceEqualized);
        //merge of the three images into one: second method
        /*Mat roiLeft = wholeFace.submat(leftImageSideRect);
        Mat roiRight = wholeFace.submat(rightImageSideRect);
        Mat leftSideEqualized = new Mat();
        Mat rightSideEqualized = new Mat();
        //sum equalized left side of image with equalized left side of the whole image
        Core.addWeighted(leftImageSide,0.5,roiLeft,0.5,0.0,leftSideEqualized);
        //sum equalized right side of image with equalized right side of the whole image
        Core.addWeighted(rightImageSide,0.5,roiRight,0.5,0.0,rightSideEqualized);
        int rows = leftSideEqualized.rows();
        int cols = leftSideEqualized.cols();
        //merge of the two parts separately summed
        Mat faceEqualized = new Mat(leftSideEqualized.rows(),leftSideEqualized.cols()+rightSideEqualized.cols(),leftSideEqualized.type());
        leftSideEqualized.copyTo(faceEqualized.submat(new Rect(0,0,cols,rows)));
        rightSideEqualized.copyTo(faceEqualized.submat(new Rect(cols,0,cols,rows)));
        Imgcodecs.imwrite("resources/faceSeparateEqualization.bmp",faceEqualized);*/

        /*SMOOTHING*/
        Mat filtered = new Mat();
        Imgproc.bilateralFilter(faceEqualized,filtered,0,20.0,2.0);
        Imgcodecs.imwrite("resources/output/faceFiltered.bmp",filtered);

        /*ELLIPTICAL MASK*/
        /*draw a black-filled ellipse in the middle of the image.
        First we initialize the mask image to white (255).*/
        Mat mask = new Mat(filtered.size(),filtered.type(),new Scalar(255));
        Point faceCenter = new Point(Math.round(width*0.5),Math.round(height*0.4));
        Size size = new Size(Math.round(width*0.5),Math.round(height*0.8));
        Imgproc.ellipse(mask,faceCenter,size,0,0,360,new Scalar(0),Core.FILLED);
        Imgcodecs.imwrite("resources/output/mask.bmp",mask);
        /*apply the elliptical mask on the face to remove corners.
        Sets corners to gray, without touching the inner face.*/
        filtered.setTo(new Scalar(128),mask);
        Imgcodecs.imwrite("resources/output/facePreProcessed.bmp",filtered);

        /*CALCULATION TIME ELAPSED*/
        long end = System.nanoTime();
        double elapsedTime = (end - start) / 10e8;
        System.out.println(String.format("Time elapsed: %s",elapsedTime));
    }
    
}
