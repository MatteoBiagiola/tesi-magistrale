/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facepreprocessing;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.objdetect.CascadeClassifier;

/**
 *
 * @author matteo
 */
public class EyeDetection {
    
    private String classificator = new String();
    private float eye_sx;
    private float eye_sy;
    private float eye_sw;
    private float eye_sh;
    private int leftX;
    private int topY;
    private int widthX;
    private int heightY;
    private int rightX;
    private Size minFeaturesSize = new Size();
    private Size maxFeaturesSize = new Size();
    private int minNeighbors;
    private int flags;
    private float scaleFactor;
    private Point rightEyePoint = new Point();
    private Point leftEyePoint = new Point();
    private Mat image = new Mat();
    
    public EyeDetection(Mat image, Size minFeatures, Size maxFeatures, int minNeigh, float scale, int flags)
    {
        this.setEye_sh((float) 0.0);
        this.setEye_sw((float) 0.0);
        this.setEye_sx((float) 0.0);
        this.setEye_sy((float) 0.0);
        this.setHeightY(0);
        this.setLeftX(0);
        this.setRightX(0);
        this.setTopY(0);
        this.setWidthX(0);
        this.setMinFeaturesSize(minFeatures);
        this.setMaxFeaturesSize(maxFeatures);
        this.setMinNeighbors(minNeigh);
        this.setScaleFactor(scale);
        this.setFlags(flags);
        this.setImage(image);
    }
    
    
    public void detectRightEye(String classificator)
    {
        this.setClassificator(classificator);
        /*extract right-eye region from image represents face*/
        this.setConstantForFaceRegions();
        CascadeClassifier eyeDetector = new CascadeClassifier(this.getClassificator());
        Rect left = new Rect(this.getLeftX(),this.getTopY(),this.widthX,this.heightY);
        /*extract right-eye region (top left of image)*/
        Mat topLeftOfFace = new Mat(this.getImage(),left);
        this.setRightEyePoint(new Point(-1,-1));
        MatOfRect rightEyeDetections = new MatOfRect();
        eyeDetector.detectMultiScale(topLeftOfFace,rightEyeDetections,
                this.getScaleFactor(),this.getMinNeighbors(),this.getFlags(),
                this.getMinFeaturesSize(),this.getMaxFeaturesSize());
        Rect rightEyeMatRect = rightEyeDetections.toArray()[0];
        Rect rightEyeRect = new Rect(rightEyeMatRect.x,rightEyeMatRect.y,rightEyeMatRect.width,rightEyeMatRect.height);
        Mat rightEyeImage = new Mat(topLeftOfFace,rightEyeRect);
        //get the right eye center respect to the original image
        Point p = new Point();
        p.x = rightEyeRect.x + rightEyeRect.width/2 + this.getLeftX();
        p.y = rightEyeRect.y + rightEyeRect.height/2 + this.getTopY();
        this.setRightEyePoint(p);
        Imgcodecs.imwrite("resources/output/right_eye.bmp",rightEyeImage);
    }
    
    public void detectLeftEye(String classificator)
    {
        this.setClassificator(classificator);
        /*extract left-eye region from image represents face*/
        this.setConstantForFaceRegions();
        CascadeClassifier eyeDetector = new CascadeClassifier(this.getClassificator());
        Rect right = new Rect(this.getRightX(),this.getTopY(),this.widthX,this.heightY);
        /*extract right-eye region (top left of image)*/
        Mat topRightOfFace = new Mat(this.getImage(),right);
        this.setLeftEyePoint(new Point(-1,-1));
        MatOfRect leftEyeDetections = new MatOfRect();
        eyeDetector.detectMultiScale(topRightOfFace,leftEyeDetections,
                this.getScaleFactor(),this.getMinNeighbors(),this.getFlags(),
                this.getMinFeaturesSize(),this.getMaxFeaturesSize());
        Rect leftEyeMatRect = leftEyeDetections.toArray()[0];
        Rect leftEyeRect = new Rect(leftEyeMatRect.x,leftEyeMatRect.y,leftEyeMatRect.width,leftEyeMatRect.height);
        Mat leftEyeImage = new Mat(topRightOfFace,leftEyeRect);
        //get the right eye center respect to the original image
        Point p = new Point();
        p.x = leftEyeRect.x + leftEyeRect.width/2 + this.getRightX();
        p.y = leftEyeRect.y + leftEyeRect.height/2 + this.getTopY();
        this.setLeftEyePoint(p);
        Imgcodecs.imwrite("resources/output/left_eye.bmp",leftEyeImage);
    }

    private void setConstantForFaceRegions()
    {
        if(this.getClassificator().contains("mcs"))
        {
            this.setEye_sx((float) 0.10);
            this.setEye_sy((float) 0.19);
            this.setEye_sw((float) 0.40);
            this.setEye_sh((float) 0.36);
            this.setLeftX(Math.round(this.getImage().cols() * this.getEye_sx()));
            this.setTopY(Math.round(this.getImage().rows() * this.getEye_sy()));
            this.setWidthX(Math.round(this.getImage().cols() * this.getEye_sw()));
            this.setHeightY(Math.round(this.getImage().rows() * this.getEye_sh()));
            this.setRightX((int) Math.round(this.getImage().cols() * (1.0 - this.getEye_sx() - this.getEye_sw())));
        }
        else if (this.getClassificator().contains("splits"))
        {
            this.setEye_sx((float) 0.12);
            this.setEye_sy((float) 0.17);
            this.setEye_sw((float) 0.37);
            this.setEye_sh((float) 0.36);
            this.setLeftX(Math.round(this.getImage().cols() * this.getEye_sx()));
            this.setTopY(Math.round(this.getImage().rows() * this.getEye_sy()));
            this.setWidthX(Math.round(this.getImage().cols() * this.getEye_sw()));
            this.setHeightY(Math.round(this.getImage().rows() * this.getEye_sh()));
            this.setRightX((int) Math.round(this.getImage().cols() * (1.0 - this.getEye_sx() - this.getEye_sw())));
        }
        else if (this.getClassificator().endsWith("_eye.xml") || this.getClassificator().contains("tree"))
        {
            this.setEye_sx((float) 0.16);
            this.setEye_sy((float) 0.26);
            this.setEye_sw((float) 0.30);
            this.setEye_sh((float) 0.28);
            this.setLeftX(Math.round(this.getImage().cols() * this.getEye_sx()));
            this.setTopY(Math.round(this.getImage().rows() * this.getEye_sy()));
            this.setWidthX(Math.round(this.getImage().cols() * this.getEye_sw()));
            this.setHeightY(Math.round(this.getImage().rows() * this.getEye_sh()));
            this.setRightX((int) Math.round(this.getImage().cols() * (1.0 - this.getEye_sx() - this.getEye_sw())));
        }
    }
       
    public String getClassificator() {
        return classificator;
    }

    public void setClassificator(String classificator) {
        this.classificator = classificator;
    }

    public float getEye_sx() {
        return eye_sx;
    }

    public void setEye_sx(float eye_sx) {
        this.eye_sx = eye_sx;
    }

    public float getEye_sy() {
        return eye_sy;
    }

    public void setEye_sy(float eye_sy) {
        this.eye_sy = eye_sy;
    }

    public float getEye_sw() {
        return eye_sw;
    }

    public void setEye_sw(float eye_sw) {
        this.eye_sw = eye_sw;
    }

    public float getEye_sh() {
        return eye_sh;
    }

    public void setEye_sh(float eye_sh) {
        this.eye_sh = eye_sh;
    }

    public int getLeftX() {
        return leftX;
    }

    public void setLeftX(int leftX) {
        this.leftX = leftX;
    }

    public int getTopY() {
        return topY;
    }

    public void setTopY(int topY) {
        this.topY = topY;
    }

    public int getWidthX() {
        return widthX;
    }

    public void setWidthX(int widthX) {
        this.widthX = widthX;
    }

    public int getHeightY() {
        return heightY;
    }

    public void setHeightY(int heightY) {
        this.heightY = heightY;
    }

    public int getRightX() {
        return rightX;
    }

    public void setRightX(int rightX) {
        this.rightX = rightX;
    }

    public Size getMinFeaturesSize() {
        return minFeaturesSize;
    }

    public void setMinFeaturesSize(Size minFeaturesSize) {
        this.minFeaturesSize = minFeaturesSize;
    }

    public Size getMaxFeaturesSize() {
        return maxFeaturesSize;
    }

    public void setMaxFeaturesSize(Size maxFeaturesSize) {
        this.maxFeaturesSize = maxFeaturesSize;
    }

    public int getMinNeighbors() {
        return minNeighbors;
    }

    public void setMinNeighbors(int minNeighbors) {
        this.minNeighbors = minNeighbors;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    public void setScaleFactor(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public Point getRightEyePoint() {
        return rightEyePoint;
    }

    public void setRightEyePoint(Point rightEyePoint) {
        this.rightEyePoint = rightEyePoint;
    }

    public Point getLeftEyePoint() {
        return leftEyePoint;
    }

    public void setLeftEyePoint(Point leftEyePoint) {
        this.leftEyePoint = leftEyePoint;
    }

    public Mat getImage() {
        return image;
    }

    public void setImage(Mat image) {
        this.image = image;
    }
}
