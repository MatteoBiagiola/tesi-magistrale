/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prefacerecognition;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author matteo
 */
public class PreFaceRecognition {

    /**
     * @param args the command line arguments
     */
    static
    {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    
    private long start_processing;
    private long start;
    private int detected;
    
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, IOException 
    {
        // TODO code application logic here
        try
        {
            String classifier = "first";
            if(args.length != 0)
            {
                classifier = args[0];
            }
            PreFaceRecognition pfr = new PreFaceRecognition();
            File targetFile = new File("resources/input");
            FileManager fm = new FileManager("resources/images.txt","resources/images.txt");
            fm.fillFile(targetFile.listFiles());
            fm.getWriter().close();
            int numImages = pfr.TestImages(targetFile.listFiles(),0);
            String line = new String();
            StringBuffer sb = new StringBuffer();
            FacePreProcesser fp = new FacePreProcesser(96,96,new Size(),new Size(),
                3,(float) 1.1,Objdetect.CASCADE_SCALE_IMAGE);
            pfr.setDetected(0);
            pfr.setStart_processing(System.currentTimeMillis());
            Mat image = new Mat();
            while((line = fm.getReader().readLine()) != null)
            {
                System.out.println();
                System.out.println("********************");
                fp.setFace(fm.readImage(line, sb));
                Imgproc.resize(fp.getFace(),fp.getFace(),new Size(256,256));
                Imgcodecs.imwrite(sb.toString(),fp.getFace());
                //pfr.PreProcessing(sb,fp,classifier);
                System.out.println("********************");
                //System.gc();
            }
            fm.getReader().close();
            System.out.println();
            System.out.println("Total preprocessing time: " + (System.currentTimeMillis() - pfr.getStart_processing())/10e2 + " [s]");
            System.out.println("Average preprocessing time: " + (System.currentTimeMillis() - pfr.getStart_processing())/(numImages*10e2) + " [s]");
            System.out.println("Eye-Detection accuracy: " + (pfr.getDetected()*100)/numImages + " %" + " on " + numImages + " images");
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void PreProcessing(StringBuffer sb,FacePreProcesser fp,String classifier)
    {
        //if eyes are closed it can be that they are not detected.
        //For the moment is not tried another eye classifier but this
        //could be a valid alternative.
        this.setStart(System.currentTimeMillis());
        if(classifier.equals("first"))
        {
            fp.detectLeftEye("resources/eyeDetection/haarcascade_mcs_lefteye.xml");
            fp.detectRightEye("resources/eyeDetection/haarcascade_mcs_righteye.xml");
        }
        else if(classifier.equals("second"))
        {
            fp.detectLeftEye("resources/eyeDetection/haarcascade_lefteye_2splits.xml");
            fp.detectRightEye("resources/eyeDetection/haarcascade_righteye_2splits.xml");
        }
        else if(classifier.equals("third"))
        {
            fp.detectLeftEye("resources/eyeDetection/haarcascade_eye.xml");
            fp.detectRightEye("resources/eyeDetection/haarcascade_eye.xml");
        }
        fp.geometricalTransformation();
        System.out.println("Eye-detection time: " + (System.currentTimeMillis() - this.getStart()) + " [ms]");
        this.setStart(System.currentTimeMillis());
        fp.geometricalTransformation();
        System.out.println("Geometrical transformation time: " + (System.currentTimeMillis() - this.getStart()) + " [ms]");
        this.setStart(System.currentTimeMillis());
        fp.separateHistogramEqualization(fp.getGeoTransformed());
        System.out.println("Histogram equalization time: " + (System.currentTimeMillis() - this.getStart()) + " [ms]");
        this.setStart(System.currentTimeMillis());
        fp.smoothing(fp.getEqualized());
        System.out.println("Smoothing time: " + (System.currentTimeMillis() - this.getStart()) + " [ms]");
        this.setStart(System.currentTimeMillis());
        fp.ellipticalMask(fp.getFiltered());
        System.out.println("Elliptical mask time: " + (System.currentTimeMillis() - this.getStart()) + " [ms]");
        if(fp.getLeftEyePoint().x > 0 && fp.getRightEyePoint().x > 0)
        {
            this.setDetected(this.getDetected() + 1);
            Imgcodecs.imwrite(sb.toString(),fp.getFiltered());
        }
    }
    
    public int TestImages(File[] images,int num)
    {
        for(File image: images)
        {
            if(image.isDirectory())
            {
                num = this.TestImages(image.listFiles(),num);
            }
            else
            {
                num = num + 1;
            }
        }
        return num;
    }

    public long getStart_processing() {
        return start_processing;
    }

    public void setStart_processing(long start_processing) {
        this.start_processing = start_processing;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public int getDetected() {
        return detected;
    }

    public void setDetected(int detected) {
        this.detected = detected;
    }
    
    
}
