/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prefacerecognition;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

/**
 *
 * @author matteo
 */
public class FacePreProcesser {
    
    private float eye_sx;
    private float eye_sy;
    private float eye_sw;
    private float eye_sh;
    private int leftX;
    private int topY;
    private int widthX;
    private int heightY;
    private int rightX;
    private String classificator = new String();
    private Size minFeaturesSize = new Size();
    private Size maxFeaturesSize = new Size();
    private int minNeighbors;
    private int flags;
    private float scaleFactor;
    private int desired_face_width;
    private int desired_face_height;
    private Mat face = new Mat();
    private Point rightEyePoint = new Point();
    private Point leftEyePoint = new Point();
    private Mat geoTransformed = new Mat();
    private Mat equalized = new Mat();
    private Mat filtered = new Mat();
    private Mat masked = new Mat();
    
    public FacePreProcesser(int height, int width,
            Size minFeatures, Size maxFeatures, int minNeigh, float scale, int flags)
    {
        this.setDesired_face_height(height);
        this.setDesired_face_width(width);
        this.setMinFeaturesSize(minFeatures);
        this.setMaxFeaturesSize(maxFeatures);
        this.setMinNeighbors(minNeigh);
        this.setScaleFactor(scale);
        this.setFlags(flags);
    }
    
    public void detectRightEye(String classificator)
    {
        this.setClassificator(classificator);
        /*extract right-eye region from image represents face*/
        this.setConstantForFaceRegions();
        CascadeClassifier eyeDetector = new CascadeClassifier(this.getClassificator());
        Rect left = new Rect(this.getLeftX(),this.getTopY(), this.getWidthX(), this.getHeightY());
        /*extract right-eye region (top left of image)*/
        Mat topLeftOfFace = new Mat(this.getFace(),left);
        this.setRightEyePoint(new Point(-1,-1));
        MatOfRect rightEyeDetections = new MatOfRect();
        eyeDetector.detectMultiScale(topLeftOfFace,rightEyeDetections,
                this.getScaleFactor(),this.getMinNeighbors(),this.getFlags(),
                this.getMinFeaturesSize(),this.getMaxFeaturesSize());
        if(rightEyeDetections.toArray().length != 0)
        {
            System.out.println("Detected " + rightEyeDetections.toArray().length + " right eye");
            Rect rightEyeMatRect = rightEyeDetections.toArray()[0];
            Rect rightEyeRect = new Rect(rightEyeMatRect.x,rightEyeMatRect.y,rightEyeMatRect.width,rightEyeMatRect.height);
            //Mat rightEyeImage = new Mat(topLeftOfFace,rightEyeRect);
            //get the right eye center respect to the original image
            Point p = new Point();
            p.x = rightEyeRect.x + rightEyeRect.width/2 + this.getLeftX();
            p.y = rightEyeRect.y + rightEyeRect.height/2 + this.getTopY();
            this.setRightEyePoint(p);
        }
        else
        {
            System.out.println("Detected no right eye");
        }
    }
    
    public void detectLeftEye(String classificator)
    {
        this.setClassificator(classificator);
        /*extract left-eye region from image represents face*/
        this.setConstantForFaceRegions();
        CascadeClassifier eyeDetector = new CascadeClassifier(this.getClassificator());
        Rect right = new Rect(this.getRightX(),this.getTopY(), this.getWidthX(), this.getHeightY());
        /*extract right-eye region (top left of image)*/
        Mat topRightOfFace = new Mat(this.getFace(),right);
        this.setLeftEyePoint(new Point(-1,-1));
        MatOfRect leftEyeDetections = new MatOfRect();
        eyeDetector.detectMultiScale(topRightOfFace,leftEyeDetections,
                this.getScaleFactor(),this.getMinNeighbors(),this.getFlags(),
                this.getMinFeaturesSize(),this.getMaxFeaturesSize());
        if(leftEyeDetections.toArray().length != 0)
        {
            System.out.println("Detected " + leftEyeDetections.toArray().length + " left eye");
            Rect leftEyeMatRect = leftEyeDetections.toArray()[0];
            Rect leftEyeRect = new Rect(leftEyeMatRect.x,leftEyeMatRect.y,leftEyeMatRect.width,leftEyeMatRect.height);
            //Mat leftEyeImage = new Mat(topRightOfFace,leftEyeRect);
            //get the right eye center respect to the original image
            Point p = new Point();
            p.x = leftEyeRect.x + leftEyeRect.width/2 + this.getRightX();
            p.y = leftEyeRect.y + leftEyeRect.height/2 + this.getTopY();
            this.setLeftEyePoint(p);
        }
        else
        {
            System.out.println("Detected no left eye");
        }
    }

    private void setConstantForFaceRegions()
    {
        if(this.getClassificator().contains("mcs"))
        {
            this.setEye_sx((float) 0.10);
            this.setEye_sy((float) 0.19);
            this.setEye_sw((float) 0.40);
            this.setEye_sh((float) 0.36);
            this.setLeftX(Math.round(this.getFace().cols() * this.getEye_sx()));
            this.setTopY(Math.round(this.getFace().rows() * this.getEye_sy()));
            this.setWidthX(Math.round(this.getFace().cols() * this.getEye_sw()));
            this.setHeightY(Math.round(this.getFace().rows() * this.getEye_sh()));
            this.setRightX((int) Math.round(this.getFace().cols() * (1.0 - this.getEye_sx() - this.getEye_sw())));
        }
        else if (this.getClassificator().contains("splits"))
        {
            this.setEye_sx((float) 0.12);
            this.setEye_sy((float) 0.17);
            this.setEye_sw((float) 0.37);
            this.setEye_sh((float) 0.36);
            this.setLeftX(Math.round(this.getFace().cols() * this.getEye_sx()));
            this.setTopY(Math.round(this.getFace().rows() * this.getEye_sy()));
            this.setWidthX(Math.round(this.getFace().cols() * this.getEye_sw()));
            this.setHeightY(Math.round(this.getFace().rows() * this.getEye_sh()));
            this.setRightX((int) Math.round(this.getFace().cols() * (1.0 - this.getEye_sx() - this.getEye_sw())));
        }
        else if (this.getClassificator().endsWith("_eye.xml"))
        {
            this.setEye_sx((float) 0.16);
            this.setEye_sy((float) 0.26);
            this.setEye_sw((float) 0.30);
            this.setEye_sh((float) 0.28);
            this.setLeftX(Math.round(this.getFace().cols() * this.getEye_sx()));
            this.setTopY(Math.round(this.getFace().rows() * this.getEye_sy()));
            this.setWidthX(Math.round(this.getFace().cols() * this.getEye_sw()));
            this.setHeightY(Math.round(this.getFace().rows() * this.getEye_sh()));
            this.setRightX((int) Math.round(this.getFace().cols() * (1.0 - this.getEye_sx() - this.getEye_sw())));
        }
    }
    
    public void geometricalTransformation()
    {
        Mat warped = new Mat();
        if(this.getLeftEyePoint().x > 0 && this.getRightEyePoint().x > 0)
        {
            //Get the center between the 2 eyes
            Point eyesCenter = new Point();
            eyesCenter.x = (this.getLeftEyePoint().x + this.getRightEyePoint().x) / 2;
            eyesCenter.y = (this.getLeftEyePoint().y + this.getRightEyePoint().y) / 2;
            //Get the angle between the 2 eyes
            double dy = this.getLeftEyePoint().y - this.getRightEyePoint().y;
            double dx = this.getLeftEyePoint().x - this.getRightEyePoint().x;
            double len = Math.sqrt(dx*dx + dy*dy);
            //convert radians to degrees
            double angle = Math.atan2(dy, dx) * 180.0/Math.PI;
            /*Hand measurement shown that the right eye center should
            ideally be roughly at (0.16, 0.14) of a scaled face image*/
            /*get the amount we need to scale the image to be the desidered
            fixed sized we want (it's arbitrary, in this case we choose 70x70).
            See the following two instructions in the constructor of the class.
            this.setDesired_face_height(70);
            this.setDesired_face_width(70);*/
            float desired_right_eye_x = (float) 0.16;
            float desired_right_eye_y = (float) 0.14;
            float desired_left_eye_x = (float) (1 - 0.16);
            double desired_length = desired_left_eye_x - desired_right_eye_x;
            double scale = desired_length * this.getDesired_face_width()/len;
            //get the transformation matrix for the desired angle and size
            Mat rot_mat = Imgproc.getRotationMatrix2D(eyesCenter,angle,scale);
            //shift the center of the eyes to be the desired center
            double ex = this.getDesired_face_width()/2 - eyesCenter.x;
            double ey = (this.getDesired_face_height() * desired_right_eye_y) - eyesCenter.y;
            double[] data1 = rot_mat.get(0,2);
            double[] data2 = rot_mat.get(1,2);
            data1[0] = data1[0] + ex;
            data2[0] = data2[0] + ey;
            rot_mat.put(0,2,data1);
            rot_mat.put(1,2,data2);
            /*transform the face image to the desired angle, size and position.
            Also clear the transformed image background to a default gray.*/
            Imgproc.warpAffine(this.getFace(),warped,rot_mat,new Size(this.getDesired_face_width(),this.getDesired_face_height()));
        }
        else
        {
            Imgproc.resize(this.getFace(),warped,new Size(this.getDesired_face_width(),this.getDesired_face_height()));
        }
        this.setGeoTransformed(warped);
    }
    
    public void separateHistogramEqualization(Mat image)
    {
        Mat wholeFace = new Mat();
        //equalized histogram on the whole face
        Imgproc.equalizeHist(image,wholeFace);
        if(this.getLeftEyePoint().x > 0 && this.getRightEyePoint().x > 0)
        {
            int half_width = this.getDesired_face_width()/2;
            Rect leftImageSideRect = new Rect(0,0,half_width,this.getDesired_face_height());
            Rect rightImageSideRect = new Rect(half_width,0,this.getDesired_face_width()-half_width,this.getDesired_face_height());
            Mat leftImageSide = new Mat(image,leftImageSideRect);
            Mat rightImageSide = new Mat(image,rightImageSideRect);
            //equalized histogram on the left side of the image
            Imgproc.equalizeHist(leftImageSide,leftImageSide);
            //equalized histogram on the right side of the image
            Imgproc.equalizeHist(rightImageSide,rightImageSide);
            //merge of the three images into one: first method (the best)
            Mat faceEqualized = new Mat(this.getDesired_face_height(),this.getDesired_face_width(),wholeFace.type());
            this.mergeImage(faceEqualized, leftImageSide, rightImageSide, wholeFace);
        }
        else
        {
            this.setEqualized(wholeFace);
        }
        
    }
    
    private void mergeImage(Mat imageRes, Mat imageLeft, Mat imageRight, Mat wholeImage)
    {
        for(int y = 0; y < this.getDesired_face_height(); y++)
        {
            for(int x = 0; x < this.getDesired_face_width(); x++)
            {
                double[] v = {0};
                if(x < this.getDesired_face_width()/4)
                {
                    //left 25%: just use the left face.
                    v = imageLeft.get(y,x);
                }
                else if(x < this.getDesired_face_width() * 2/4)
                {
                    //mid-left 25%: blend the left face and the whole face.
                    double[] lv = imageLeft.get(y,x);
                    double[] wv = wholeImage.get(y,x);
                    /*blend more of the whole face as it moves
                    further right along the face.*/
                    float f = (x - this.getDesired_face_width() * 1/4)/(this.getDesired_face_width()/4);
                    v[0] = Math.round((1.0 - f) * lv[0] + f * wv[0]);
                }
                else if(x < this.getDesired_face_width() * 3/4)
                {
                    //mid-right 25%: blend the right face and the whole face.
                    double[] rv = imageRight.get(y,x-(this.getDesired_face_width()/2));
                    double[] wv = wholeImage.get(y,x);
                    /*blend more of the right-side face as it moves
                    further right along the face.*/
                    float f = (x - this.getDesired_face_width() * 2/4)/(this.getDesired_face_width()/4);
                    v[0] = Math.round((1.0 - f) * wv[0] + f * rv[0]);
                }
                else
                {
                    //right 25%: just use the right face.
                    v = imageRight.get(y,x-(this.getDesired_face_width()/2));
                }
                imageRes.put(y,x,v);
            }
        }
        this.setEqualized(imageRes);
    }
    
    public void smoothing(Mat image)
    {
        Mat filtered = new Mat();
        Imgproc.bilateralFilter(image,filtered,0,20.0,2.0);
        this.setFiltered(filtered);
    }
    
    public void ellipticalMask(Mat image)
    {
        if(this.getLeftEyePoint().x > 0 && this.getRightEyePoint().x > 0)
        {
            /*draw a black-filled ellipse in the middle of the image.
            First we initialize the mask image to white (255).*/
            Mat mask = new Mat(image.size(),image.type(),new Scalar(255));
            Point faceCenter = new Point(Math.round(this.getDesired_face_width()*0.5),Math.round(this.getDesired_face_height()*0.4));
            Size size = new Size(Math.round(this.getDesired_face_width()*0.5),Math.round(this.getDesired_face_height()*0.8));
            Imgproc.ellipse(mask,faceCenter,size,0,0,360,new Scalar(0),Core.FILLED);
            /*apply the elliptical mask on the face to remove corners.
            Sets corners to gray, without touching the inner face.*/
            image.setTo(new Scalar(128),mask);
        }
        this.setMasked(image);
    }
    
    public float getEye_sx() {
        return eye_sx;
    }

    public void setEye_sx(float eye_sx) {
        this.eye_sx = eye_sx;
    }

    public float getEye_sy() {
        return eye_sy;
    }

    public void setEye_sy(float eye_sy) {
        this.eye_sy = eye_sy;
    }

    public float getEye_sw() {
        return eye_sw;
    }

    public void setEye_sw(float eye_sw) {
        this.eye_sw = eye_sw;
    }

    public float getEye_sh() {
        return eye_sh;
    }

    public void setEye_sh(float eye_sh) {
        this.eye_sh = eye_sh;
    }

    public int getLeftX() {
        return leftX;
    }

    public void setLeftX(int leftX) {
        this.leftX = leftX;
    }

    public int getTopY() {
        return topY;
    }

    public void setTopY(int topY) {
        this.topY = topY;
    }

    public int getWidthX() {
        return widthX;
    }

    public void setWidthX(int widthX) {
        this.widthX = widthX;
    }

    public int getHeightY() {
        return heightY;
    }

    public void setHeightY(int heightY) {
        this.heightY = heightY;
    }

    public int getRightX() {
        return rightX;
    }

    public void setRightX(int rightX) {
        this.rightX = rightX;
    }

    public String getClassificator() {
        return classificator;
    }

    public void setClassificator(String classificator) {
        this.classificator = classificator;
    }

    public Size getMinFeaturesSize() {
        return minFeaturesSize;
    }

    public void setMinFeaturesSize(Size minFeaturesSize) {
        this.minFeaturesSize = minFeaturesSize;
    }

    public Size getMaxFeaturesSize() {
        return maxFeaturesSize;
    }

    public void setMaxFeaturesSize(Size maxFeaturesSize) {
        this.maxFeaturesSize = maxFeaturesSize;
    }

    public int getMinNeighbors() {
        return minNeighbors;
    }

    public void setMinNeighbors(int minNeighbors) {
        this.minNeighbors = minNeighbors;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    public void setScaleFactor(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public int getDesired_face_width() {
        return desired_face_width;
    }

    public void setDesired_face_width(int desired_face_width) {
        this.desired_face_width = desired_face_width;
    }

    public int getDesired_face_height() {
        return desired_face_height;
    }

    public void setDesired_face_height(int desired_face_height) {
        this.desired_face_height = desired_face_height;
    }

    public Mat getFace() {
        return face;
    }

    public void setFace(Mat face) {
        this.face = face;
    }

    public Point getRightEyePoint() {
        return rightEyePoint;
    }

    public void setRightEyePoint(Point rightEyePoint) {
        this.rightEyePoint = rightEyePoint;
    }

    public Point getLeftEyePoint() {
        return leftEyePoint;
    }

    public void setLeftEyePoint(Point leftEyePoint) {
        this.leftEyePoint = leftEyePoint;
    }

    public Mat getGeoTransformed() {
        return geoTransformed;
    }

    public void setGeoTransformed(Mat geoTransformed) {
        this.geoTransformed = geoTransformed;
    }

    public Mat getEqualized() {
        return equalized;
    }

    public void setEqualized(Mat equalized) {
        this.equalized = equalized;
    }

    public Mat getFiltered() {
        return filtered;
    }

    public void setFiltered(Mat filtered) {
        this.filtered = filtered;
    }

    public Mat getMasked() {
        return masked;
    }

    public void setMasked(Mat masked) {
        this.masked = masked;
    }

}