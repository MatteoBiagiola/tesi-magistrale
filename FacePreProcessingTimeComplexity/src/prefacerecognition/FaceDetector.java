/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prefacerecognition;

import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

/**
 *
 * @author matteo
 */
public class FaceDetector {
    
    private List<Mat> detected = new ArrayList();
    private Mat image = new Mat();
    private String classificator;
    private int detection_width;
    private Size minFeaturesSize;
    private Size maxFeaturesSize;
    private int flags;
    private float scaleFactor;
    private int minNeighbors;
    
    public FaceDetector(Mat image,int detection_width,
            Size minFeaturesSize,Size maxFeaturesSize,
            int flags,float scaleFactor,int minNeighbors)
    {
        this.setImage(image);
        this.setDetection_width(detection_width);
        this.setMinFeaturesSize(minFeaturesSize);
        this.setMaxFeaturesSize(maxFeaturesSize);
        this.setFlags(flags);
        this.setScaleFactor(scaleFactor);
        this.setMinNeighbors(minNeighbors);
    }

    public Mat resize()
    {
        Mat smallImage = new Mat();
        int scaledHeight = 0;
        Size rescale;
        float scale = this.getImage().cols()/(float) this.getDetection_width();
        if(this.getImage().cols() > this.getDetection_width())
        {
            scaledHeight = Math.round(this.getImage().rows()/scale);
            rescale = new Size(this.getDetection_width(),scaledHeight);
            Imgproc.resize(this.getImage(), smallImage, rescale);
        }
        else
        {
            smallImage = this.getImage();
        }
        return smallImage;
    }
    
    public void faceDetections(Mat image, String classificator)
    {
        this.setClassificator(classificator);
        CascadeClassifier faceDetector = new CascadeClassifier(this.getClassificator());
        Mat gray = new Mat();
        Mat grayEq = new Mat();
        MatOfRect detected = new MatOfRect();
        List<Mat> faceDetected = new ArrayList();
        Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.equalizeHist(gray, grayEq);
        faceDetector.detectMultiScale(grayEq,detected,this.getScaleFactor(),
            this.getMinNeighbors(),this.getFlags(),
            this.getMinFeaturesSize(),this.getMaxFeaturesSize());
        for(Rect rect: detected.toArray())
        {
            Rect r = new Rect(rect.x,rect.y,rect.width,rect.height);
            Mat croppedFace = new Mat(gray,r);
            faceDetected.add(croppedFace);
        }
        this.setDetected(faceDetected);
    }
    
    public List<Mat> getDetected() {
        return detected;
    }

    public void setDetected(List<Mat> imageDetected) {
        this.detected = imageDetected;
    }

    public int getDetection_width() {
        return detection_width;
    }

    public void setDetection_width(int detection_width) {
        this.detection_width = detection_width;
    }

    public Size getMinFeaturesSize() {
        return minFeaturesSize;
    }

    public void setMinFeaturesSize(Size minFeaturesSize) {
        this.minFeaturesSize = minFeaturesSize;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    public void setScaleFactor(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public int getMinNeighbors() {
        return minNeighbors;
    }

    public void setMinNeighbors(int minNeighbors) {
        this.minNeighbors = minNeighbors;
    }

    public Mat getImage() {
        return image;
    }

    public void setImage(Mat image) {
        this.image = image;
    }

    public Size getMaxFeaturesSize() {
        return maxFeaturesSize;
    }

    public void setMaxFeaturesSize(Size maxFeaturesSize) {
        this.maxFeaturesSize = maxFeaturesSize;
    }

    public String getClassificator() {
        return classificator;
    }

    public void setClassificator(String classificator) {
        this.classificator = classificator;
    }
}