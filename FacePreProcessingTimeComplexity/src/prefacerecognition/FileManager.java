/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prefacerecognition;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

/**
 *
 * @author matteo
 */
public class FileManager {
    private PrintWriter writer;
    private BufferedReader reader;
    
    public FileManager(String fileToWrite, String fileToRead) throws FileNotFoundException, UnsupportedEncodingException
    {
        this.setWriter(new PrintWriter(fileToWrite,"UTF-8"));
        this.setReader(new BufferedReader (new FileReader(fileToRead)));
    }

    public void fillFile(File[] files) throws IOException
    {
        for(File file: files)
        {
            if(file.getAbsolutePath().contains("DS_Store"))
            {
                file.delete();
            }
            else if(file.getAbsolutePath().contains(".directory"))
            {
                file.delete();
            }
            else if(file.isDirectory())
            {
                String inputDirName = file.getPath();
                String outputDirName = inputDirName.replace("input","output");
                File outputDirFile = new File(outputDirName);
                if(!outputDirFile.exists())
                {
                    outputDirFile.mkdir();
                }
                this.fillFile(file.listFiles());
            }
            else
            {
                this.getWriter().println(file.getPath());
            }    
        }
    }
    
    public Mat readImage(String lineToRead, StringBuffer outputPath) throws IOException
    {
        outputPath.delete(0,outputPath.length());
        outputPath.append(lineToRead.replace("input","output"));
        return Imgcodecs.imread(lineToRead,Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
    }

    public PrintWriter getWriter() {
        return writer;
    }

    public void setWriter(PrintWriter writer) {
        this.writer = writer;
    }

    public BufferedReader getReader() {
        return reader;
    }

    public void setReader(BufferedReader reader) {
        this.reader = reader;
    }
    
}
